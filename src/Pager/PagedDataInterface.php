<?php

namespace BBCWorldWide\JsonApi\Pager;

interface PagedDataInterface
{
    /**
     * @return array|\Iterator
     */
    public function getData();

    /**
     * @return int
     */
    public function getCount();

    /**
     * @return int
     */
    public function getPageSize();

    /**
     * @return int
     */
    public function getCurrentPage();

    /**
     * @return int
     */
    public function getLastPage();
}
