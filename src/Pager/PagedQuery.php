<?php

namespace BBCWorldWide\JsonApi\Pager;

use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\CountWalker;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * Paged Query
 *
 * @codeCoverageIgnore Query object cannot be mocked
 */
class PagedQuery implements PagedDataInterface
{
    const DEFAULT_PAGE_SIZE = 50;
    const MAX_PAGE_SIZE     = 1000;

    /**
     * @var Paginator
     */
    protected $paginator;

    /**
     * @var int
     */
    protected $count;

    /**
     * @var int
     */
    protected $pageSize;

    /**
     * @var int
     */
    protected $currentPage;

    /**
     * @var int
     */
    protected $lastPage;

    /**
     * PagedQuery constructor.
     *
     * @param Query $query
     * @param int   $currentPage
     * @param int   $pageSize
     */
    public function __construct(Query $query, $currentPage, $pageSize)
    {
        if ($pageSize < 1 || $pageSize > self::MAX_PAGE_SIZE) {
            throw new \InvalidArgumentException('Page size out of range');
        }
        if (!is_numeric($pageSize)) {
            throw new \InvalidArgumentException('Page size must be numeric');
        }
        if (!is_numeric($currentPage)) {
            throw new \InvalidArgumentException('Page number must be numeric');
        }

        // Avoid DISTINCT where possible to improve performance.
        if ($query->getHint(CountWalker::HINT_DISTINCT)) {
            $this->paginator = new Paginator($query);
        } else {
            $query->setHint(CountWalker::HINT_DISTINCT, false);
            $this->paginator = new Paginator($query, false);
            $this->paginator->setUseOutputWalkers(false);
        }

        $this->count       = $this->paginator->count();
        $this->currentPage = $currentPage;
        $this->pageSize    = $pageSize;
        $this->lastPage    = max(ceil($this->count / $this->pageSize), 1);

        $query->setFirstResult($pageSize * ($currentPage - 1))
              ->setMaxResults($pageSize);

        if ($currentPage < 1 || $currentPage > $this->lastPage) {
            throw new \InvalidArgumentException('Page number out of range', 400);
        }
    }

    /**
     * @return array|\Iterator
     */
    public function getData()
    {
        return $this->paginator->getQuery()->execute();
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->pageSize;
    }

    /**
     * @return int
     */
    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    /**
     * @return int
     */
    public function getLastPage()
    {
        return $this->lastPage;
    }
}
