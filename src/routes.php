<?php
use BBCWorldWide\JsonApi\Action\Relationship;
use BBCWorldWide\JsonApi\Action\Resource;
use Slim\App;

// Routes
return function (App $app) {
    $app->group('/v1', function () use ($app) {
        $app->get('/swagger.json', BBCWorldWide\JsonApi\Action\SwaggerAction::class);

        $app->get('/{type}', Resource\IndexAction::class);
        $app->post('/{type}', Resource\CreateAction::class);
        $app->get('/{type}/{id}', Resource\GetAction::class);
        $app->put('/{type}/{id}', Resource\UpdateAction::class);
        $app->patch('/{type}/{id}', Resource\UpdateAction::class);
        $app->delete('/{type}/{id}', Resource\DeleteAction::class);
        $app->get('/{type}/{id}/{relationship}', Resource\GetRelatedAction::class);
        $app->get('/{type}/{id}/relationships/{relationship}', Relationship\GetAction::class);
        $app->post('/{type}/{id}/relationships/{relationship}', Relationship\CreateAction::class);
        $app->put('/{type}/{id}/relationships/{relationship}', Relationship\UpdateAction::class);
        $app->patch('/{type}/{id}/relationships/{relationship}', Relationship\UpdateAction::class);
        $app->delete('/{type}/{id}/relationships/{relationship}', Relationship\DeleteAction::class);
    });
};
