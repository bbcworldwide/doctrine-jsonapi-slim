<?php

namespace BBCWorldWide\JsonApi\Extensions\Doctrine;

use Monolog\Logger;

class SqlLogger implements \Doctrine\DBAL\Logging\SQLLogger
{
    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var string Log level
     */
    protected $level;

    /**
     * @var string
     */
    protected $sql;

    /**
     * @var array
     */
    protected $params;

    /**
     * @var array
     */
    protected $types;

    /**
     * @var int
     */
    protected $startTime;

    /**
     * Public constructor.
     *
     * @param Logger $logger Logger instance
     * @param string $level  Log level
     */
    public function __construct(Logger $logger, string $level)
    {
        $this->logger = $logger;
        $this->level  = $level;
    }

    /**
     * {@inheritdoc}
     */
    public function startQuery($sql, array $params = null, array $types = null)
    {
        $this->sql       = $sql;
        $this->params    = $params;
        $this->types     = $types;
        $this->startTime = microtime(true);
    }

    /**
     * {@inheritdoc}
     */
    public function stopQuery()
    {
        $this->logger->log($this->level, 'SQL Query', [
            'type'     => 'sql-query',
            'sql'      => $this->sql,
            'params'   => $this->params,
            'types'    => $this->types,
            'duration' => (microtime(true) - $this->startTime) * 1000,
        ]);
    }
}
