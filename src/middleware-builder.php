<?php
// Application middleware

return function (\Slim\App $app) {
    $container = $app->getContainer();

    $app->add(new \BBCWorldWide\JsonApi\ResponseCache\ResponseCacheMiddleware(
        $container->get('cachePool'),
        $container->get('cacheTagMapper'),
        $container->get('cacheExpiryChecker')
    ));
    $app->add(new \BBCWorldWide\JsonApi\JsonApi\JsonApiMiddleware($container->get('parametersParser')));
    $app->add(new \BBCWorldwide\Logging\Middleware\RequestLogger($container->get('logger')));
};
