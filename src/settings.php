<?php

$env   = $_SERVER['APP_ENVIRONMENT'] ?? 'dev';
$isDev = $env !== 'prod';

// Database settings
$databaseSettings = include __DIR__ . '/settings-database.php';

// Logging settings
$loggerSettings = [
    'name'     => $_SERVER['APP_NAME'] ?? 'BBCWW - JSON API of DOOM',
    'logLevel' => Monolog\Logger::INFO,
];

return [
    'cacheFolder' => $_SERVER['APP_CACHE_FOLDER'] ?? '/tmp/cache',
    'settings'    => [
        'env'                               => $env,
        'dev'                               => $isDev,
        'baseUri'                           => $_SERVER['APP_BASE_URI'] ?? 'http://127.0.0.1/v1',

        // Database
        'db'                                => $databaseSettings,

        // Slim Settings
        'determineRouteBeforeAppMiddleware' => false,
        'displayErrorDetails'               => $isDev,

        // Monolog settings
        'logger'                            => $loggerSettings,

        // Response cache
        'cacheResponses'                    => $_SERVER['APP_CACHE_RESPONSES'] ?? true,
        'cacheDriver'                       => $_SERVER['APP_CACHE_DRIVER'] ?? 'array',

        'redis' => [
            'host' => $_SERVER['APP_REDIS_HOST'] ?? 'redis',
            'port' => $_SERVER['APP_REDIS_PORT'] ?? '6379',
        ],
    ],
];
