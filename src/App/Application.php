<?php

namespace BBCWorldWide\JsonApi\App;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface as Logger;
use Slim\App as SlimApp;

/**
 * Main application class.
 *
 * @author BBC Worldwide
 */
class Application
{
    /**
     * @var SlimApp
     */
    private $slimApp;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @param SlimApp $slimApp
     * @param Logger  $logger
     */
    public function __construct(SlimApp $slimApp, Logger $logger)
    {
        $this->slimApp = $slimApp;
        $this->logger  = $logger;
    }

    /**
     * Gives access to the DI container.
     *
     * @return ContainerInterface
     */
    public function getContainer(): ContainerInterface
    {
        return $this->slimApp->getContainer();
    }

    /**
     * Gives access to the Slim application.
     *
     * @return SlimApp
     */
    public function getSlimApp(): SlimApp
    {
        return $this->slimApp;
    }

    /**
     * Run the app and ensure uncaught exception logging.
     *
     * @throws \Exception
     */
    public function run(): void
    {
        try {
            $this->slimApp->run();
        } catch (\Exception $ex) {
            $this->logger->error($ex->getMessage(), ['exception' => $ex]);
            throw $ex;
        }
    }

    /**
     * Processes a request and returns a response.
     *
     * The response object passed on the signature must be a concrete implementation to avoid making assumptions of
     * whose PSR-7 implementation to use.
     *
     * @param ServerRequestInterface $request  The request to process
     * @param ResponseInterface      $response A concrete Response implementation to fill
     *
     * @return ResponseInterface
     *
     * @throws \Exception
     */
    public function process(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        try {
            return $this->slimApp->process($request, $response);
        } catch (\Exception $ex) {
            $this->logger->error($ex->getMessage(), ['exception' => $ex]);
            throw $ex;
        }
    }
}
