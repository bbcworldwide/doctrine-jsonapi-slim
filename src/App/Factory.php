<?php

namespace BBCWorldWide\JsonApi\App;

use Psr\Log\NullLogger;
use Slim\App as SlimApp;

/**
 * This factory will start our Application with all dependencies (both default and app-specific), middlewares, config
 * already factored into it.
 *
 * @author BBC Worldwide
 */
class Factory
{
    /**
     * Build Application from an app-specific implementation of SetupInterface.
     *
     * @param SetupInterface $setup
     * @param array $entityPaths
     * @param array $schemaLocation
     *
     * @return Application
     */
    public static function getInstance(SlimApp $slim, array $entityPaths, array $schema): Application
    {
        $container = $slim->getContainer();

        // Get Doctrine entity locations from app setup
        $container['entityPaths'] = $entityPaths;

        // Pass in the JSONAPI schema location
        $container['schemaLocation'] = $schema;

        // Show errors in dev mode
        $settings = $container->get('settings');
        if ($settings['settings']['dev'] ?? false) {
            error_reporting(-1);
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
        }

        // Set up container by using the default container builder, and the app supplied builder
        $dependencies = require __DIR__ . '/../container.php';

        foreach ($dependencies as $name => $dependency) {
            $slim->getContainer()[$name] = $dependency;
        }

        // Set up middlewares as a combination of the default ones with the ones provided by the app
        $middlewareBuilder = require __DIR__ . '/../middleware-builder.php';
        $middlewareBuilder($slim);

        // Finally, build slim routes
        $routeBuilder = require __DIR__ . '/../routes.php';
        $routeBuilder($slim);

        // Last dependency, logger
        $logger = $slim->getContainer()->has('logger') ? $slim->getContainer()->get('logger') : new NullLogger();

        // Now, instantiate our own Application
        return new Application($slim, $logger);
    }
}
