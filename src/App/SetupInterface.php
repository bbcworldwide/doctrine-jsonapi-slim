<?php

namespace BBCWorldWide\JsonApi\App;

/**
 * This interface defines the necessary user-provided set up required to instantiate the application itself.
 *
 * This is how we get to user defined, app-specific set up - extra middlewares, config, container dependencies...
 *
 * @package BBCWorldWide\JsonApi\App
 */
interface SetupInterface
{
    /**
     * Return an array of middlewares to add to the stack. They must be callables which return instances to middlewares.
     *
     * @return callable[]
     */
    public function getMiddlewareCallables(): array;

    /**
     * Returns any extra config the app may want to define as an array.
     *
     * @return array
     */
    public function getSettings(): array;

    /**
     * Returns a list of extra dependency (or overrides) the app may want to add to the container.
     *
     * @return callable[]
     */
    public function getContainerDependencies(): array;

    /**
     * Returns a JSON API schema array - see examples on provided schema, or in fixtures in functional tests.
     *
     * @return array
     */
    public function getJsonApiSchema(): array;

    /**
     * Return an array of paths (relative to the class implementing this interface) where doctrine entities can be
     * found.
     *
     * @return array
     */
    public function getEntityPaths(): array;

    /**
     * Return an array of callables that take a Psr\Container\ContainerInterface parameter which returns an
     * implementation of the interface Doctrine\Common\EventSubscriber.
     *
     * @return array
     */
    public function getEventSubscribers(): array;
}
