<?php
use BBCWorldWide\JsonApi\JsonApi\Schema\AttributeDefinition as Attr;

// This is an example; this file must return the full schema array.
$sampleSchema = [
    'entityType' => [
        'entityClass'   => 'EntityClassName',
        'attributes'    => [
            'crid'      => ['filterable' => true],
            'name'      => [],
            'position'  => ['type' => Attr::TYPE_INT, 'sortable' => true, 'filterable' => true],
            'createdAt' => ['type' => Attr::TYPE_DATETIME, 'sortable' => true, 'readOnly' => true],
            'updatedAt' => ['type' => Attr::TYPE_DATETIME, 'sortable' => true, 'readOnly' => true],
        ],
        'relationships' => [
            'parent'     => ['types' => ['categories']],
            'children'   => ['multiple' => true, 'types' => ['categories']],
            'programmes' => ['multiple' => true, 'types' => ['programmes'], 'includable' => false],
            'discounts'  => ['multiple' => true, 'types' => ['discounts']],
        ],
        'defaultSort'   => 'position',
    ],
];

return [];
