<?php
// DIC configuration

use BBCWorldwide\Logging\Formatter\JsonFormatter;
use BBCWorldwide\Logging\Processor\CorrelationProcessor;
use BBCWorldWide\JsonApi\Extensions\Doctrine\SqlLogger;
use BBCWorldWide\JsonApi\JsonApi\Encoder\EntityEncoder;
use BBCWorldWide\JsonApi\JsonApi\Encoder\PagedDataEncoder;
use BBCWorldWide\JsonApi\JsonApi\EntityMapper;
use BBCWorldWide\JsonApi\JsonApi\Parameters\ParametersParser;
use BBCWorldWide\JsonApi\JsonApi\Schema\Schema;
use BBCWorldWide\JsonApi\JsonApi\Store;
use BBCWorldWide\JsonApi\JsonApi\Validator\DocumentValidator;
use BBCWorldWide\JsonApi\ResponseCache\ResponseCacheSubscriber;
use Cache\Adapter\PHPArray\ArrayCachePool;
use Cache\Adapter\Redis\RedisCachePool;
use Cache\Adapter\Void\VoidCachePool;
use Doctrine\Common\Cache\ApcuCache;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\EventManager;
use Doctrine\DBAL\Connections\MasterSlaveConnection;
use Doctrine\DBAL\DriverManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\UnderscoreNamingStrategy;
use Doctrine\ORM\Tools\Setup as DoctrineSetup;
use Gedmo\Timestampable\TimestampableListener;
use Psr\Container\ContainerInterface;
use Psr\Log\LogLevel;

$container = [];

$container['schema'] = function (ContainerInterface $c) {
    return new Schema($c->get('schemaLocation'));
};

// Doctrine eventManager
$container['eventManager'] = function (ContainerInterface $c) {
    $eventManager = new EventManager();
    $eventManager->addEventSubscriber(new TimestampableListener());
    $eventManager->addEventSubscriber(new ResponseCacheSubscriber($c->get('cachePool'), $c->get('cacheTagMapper')));

    return $eventManager;
};

$container['cachePool'] = function (ContainerInterface $c) {
    $settings = $c->get('settings');

    if (!$settings['cacheResponses']) {
        return new VoidCachePool();
    }

    // @codeCoverageIgnoreStart
    switch ($settings['cacheDriver']) {
        case 'redis':
            $redis = new \Redis();
            $redis->pconnect($settings['redis']['host'], $settings['redis']['port']);

            return new RedisCachePool($redis);

        case 'array':
            return new ArrayCachePool();

        default:
            return new VoidCachePool();
    }
    // @codeCoverageIgnoreEnd
};

$container['cacheTagMapper'] = function (ContainerInterface $c) {
    return new \BBCWorldWide\JsonApi\ResponseCache\TagMapper($c->get('schema'));
};

$container['cacheExpiryChecker'] = function (ContainerInterface $c) {
    return new \BBCWorldWide\JsonApi\ResponseCache\ExpiryChecker($c->get('entityManager'));
};

// Doctrine entityManager
$container['entityManager'] = function (ContainerInterface $c) {
    $settings = $c->get('settings');
    $paths    = $c->get('entityPaths');
    $proxyDir = sprintf('%s/proxies', $c->get('cacheFolder'));

    $eventManager = $c->get('eventManager');
    $isProd       = $settings['env'] === 'prod';
    $cacheDriver = !$isProd ? new ArrayCache() : new ApcuCache();

    // Spawn config, and configure proxy locations and naming strategy
    $config = DoctrineSetup::createAnnotationMetadataConfiguration($paths, !$isProd, $proxyDir, $cacheDriver);
    $config->setNamingStrategy(new UnderscoreNamingStrategy());
    $config->addCustomStringFunction('MATCH_AGAINST', BBCWorldWide\JsonApi\Extensions\Doctrine\MatchAgainst::class);
    $config->setSQLLogger(new SqlLogger($c->get('logger'), LogLevel::DEBUG));
    $config->setAutoGenerateProxyClasses(!$isProd);

    // Master/slave connections
    // @codeCoverageIgnoreStart
    $connectionSettings = $settings['db']['master'];
    if (array_key_exists('slaves', $settings['db']) === true) {
        $connectionSettings = [
            'wrapperClass' => MasterSlaveConnection::class,
            'driver'       => $settings['db']['master']['driver'],
            'master'       => $settings['db']['master'],
            'slaves'       => $settings['db']['slaves'],
        ];
    }
    // @codeCoverageIgnoreEnd

    $connection = DriverManager::getConnection($connectionSettings, $config, $eventManager);

    return EntityManager::create($connection, $config, $eventManager);
};

// Correlation processor
$container['correlationProcessor'] = function (ContainerInterface $c) {
    return new CorrelationProcessor($c->get('request'));
};

// Logging configuration
$container['logger'] = function (ContainerInterface $c) {
    $settings = $c->get('settings')['logger'];
    $logger   = new Monolog\Logger($settings['name'] ?? 'app name not configured');
    $logger->pushProcessor($c->get('correlationProcessor'));

    $handler = new Monolog\Handler\StreamHandler('/tmp/stdout', $settings['logLevel']);
    $handler->setFormatter(new JsonFormatter());

    $logger->pushHandler($handler);

    return $logger;
};

$container['entityEncoder'] = function (ContainerInterface $c) {
    return new EntityEncoder($c->get('schema'), $c->get('store'));
};

$container['pagedDataEncoder'] = function (ContainerInterface $c) {
    return new PagedDataEncoder($c->get('schema'), $c->get('store'));
};

$container['entityMapper'] = function (ContainerInterface $c) {
    return new EntityMapper($c->get('schema'), $c->get('store'));
};

$container['documentValidator'] = function (ContainerInterface $c) {
    return new DocumentValidator($c->get('schema'));
};

$container['parametersParser'] = function (ContainerInterface $c) {
    return new ParametersParser($c->get('schema'));
};

$container['store'] = function (ContainerInterface $c) {
    return new Store($c->get('entityManager'), $c->get('schema'));
};

$container['buildInfo'] = function () {
    $buildInfoPath = __DIR__ . '/../build-info.json';
    if (file_exists($buildInfoPath)) {
        return json_decode(file_get_contents($buildInfoPath), true);
    }

    return ['tag' => 'dev', 'time' => null];
};

$container['notFoundHandler'] = function (ContainerInterface $c) {
    return new BBCWorldWide\JsonApi\Error\NotFoundHandler($c->get('buildInfo'));
};

$container['errorHandler'] = function (ContainerInterface $c) {
    $settings = $c->get('settings');

    return new BBCWorldWide\JsonApi\Error\ErrorHandler(
        $settings['displayErrorDetails'],
        $settings['env'],
        $c->get('logger'),
        $c->get('buildInfo')
    );
};

$container['phpErrorHandler'] = function (ContainerInterface $c) {
    return $c->get('errorHandler');
};

return $container;
