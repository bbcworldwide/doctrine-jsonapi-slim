<?php

namespace BBCWorldWide\JsonApi\JsonApi;

use BBCWorldWide\JsonApi\JsonApi\Parameters\ParametersParser;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Http\Response;

/**
 * Class JsonApiMiddleware
 *
 * @codeCoverageIgnore Functional test only
 */
class JsonApiMiddleware
{
    /**
     * @var ParametersParser
     */
    private $parametersParser;

    /**
     * JsonApiMiddleware constructor.
     *
     * @param ParametersParser $parametersParser
     */
    public function __construct(ParametersParser $parametersParser)
    {
        $this->parametersParser = $parametersParser;
    }

    public function __invoke(ServerRequestInterface $request, Response $response, $next)
    {
        $uri  = $request->getUri();
        $path = $uri->getPath();

        if (strpos($path, '/v1/') === 0 && $path !== '/v1/swagger.json') {
            $response = $response->withHeader('Content-Type', 'application/vnd.api+json');

            $parameters = $this->parametersParser->getInstance($request);
            $request    = $request->withAttribute('parameters', $parameters);
        }

        return $next($request, $response);
    }
}
