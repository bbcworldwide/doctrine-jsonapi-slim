<?php

namespace BBCWorldWide\JsonApi\JsonApi\Schema;

use Doctrine\Common\Inflector\Inflector;

class AttributeDefinition
{
    const TYPE_STRING   = 'string';
    const TYPE_INT      = 'int';
    const TYPE_FLOAT    = 'float';
    const TYPE_BOOL     = 'bool';
    const TYPE_DATETIME = 'datetime';

    protected $name;
    protected $alias;
    protected $sortable   = false;
    protected $filterable = false;
    protected $type       = self::TYPE_STRING;
    protected $readOnly   = false;
    protected $required   = false;

    /**
     * AttributeDefinition constructor.
     *
     * @param string     $name
     * @param array|null $config
     */
    public function __construct(string $name, array $config = null)
    {
        $this->name       = $name;
        $this->alias      = $config['alias'] ?? ucfirst(str_replace('_', ' ', Inflector::tableize($name)));
        $this->sortable   = $config['sortable'] ?? $this->sortable;
        $this->filterable = $config['filterable'] ?? $this->filterable;
        $this->type       = $config['type'] ?? $this->type;
        $this->readOnly   = $config['readOnly'] ?? $this->readOnly;
        $this->required   = $config['required'] ?? $this->required;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return AttributeDefinition
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param mixed $alias
     *
     * @return AttributeDefinition
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isSortable()
    {
        return $this->sortable;
    }

    /**
     * @param boolean $sortable
     *
     * @return AttributeDefinition
     */
    public function setSortable($sortable)
    {
        $this->sortable = $sortable;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isFilterable()
    {
        return $this->filterable;
    }

    /**
     * @param boolean $filterable
     *
     * @return AttributeDefinition
     */
    public function setFilterable($filterable)
    {
        $this->filterable = $filterable;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return AttributeDefinition
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isReadOnly()
    {
        return $this->readOnly;
    }

    /**
     * @param boolean $readOnly
     *
     * @return AttributeDefinition
     */
    public function setReadOnly($readOnly)
    {
        $this->readOnly = $readOnly;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isRequired()
    {
        return $this->required;
    }

    /**
     * @param boolean $required
     *
     * @return AttributeDefinition
     */
    public function setRequired($required)
    {
        $this->required = $required;

        return $this;
    }
}
