<?php

namespace BBCWorldWide\JsonApi\JsonApi\Schema;

use Doctrine\Common\Inflector\Inflector;

class TypeDefinition
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $entityClass;

    /**
     * @var string
     */
    protected $alias;

    /**
     * @var AttributeDefinition[]
     */
    protected $attributes = [];

    /**
     * @var RelationshipDefinition[]
     */
    protected $relationships = [];

    /**
     * @var string[]
     */
    protected $subtypes = [];

    /**
     * @var string
     */
    protected $subtypeOf;

    /**
     * @var string[]
     */
    protected $customFilters = [];

    /**
     * @var string
     */
    protected $defaultSort;

    /**
     * @var bool
     */
    protected $cacheResults = false;

    /**
     * Schema constructor.
     *
     * @param string     $name
     * @param array|null $config
     *
     * @throws \InvalidArgumentException
     */
    public function __construct($name, array $config = null)
    {
        $this->name = $name;

        if (isset($config['entityClass'])) {
            $this->entityClass = $config['entityClass'];
        } else {
            throw new \InvalidArgumentException('Attribute config is missing required key \'entityClass\'');
        }

        $this->alias         = $config['alias'] ?? $this->alias;
        $this->subtypes      = $config['subtypes'] ?? $this->subtypes;
        $this->subtypeOf     = $config['subtypeOf'] ?? $this->subtypeOf;
        $this->customFilters = $config['customFilters'] ?? $this->customFilters;
        $this->defaultSort   = $config['defaultSort'] ?? $this->defaultSort;
        $this->cacheResults  = $config['cacheResults'] ?? $this->cacheResults;
        $this->alias         = $config['alias'] ?? $this->alias;

        foreach ($config['attributes'] ?? [] as $attribute => $attributeConfig) {
            $definition = new AttributeDefinition($attribute, $attributeConfig);
            $this->addAttribute($attribute, $definition);
        }

        foreach ($config['relationships'] ?? [] as $relationship => $relationshipConfig) {
            $definition = new RelationshipDefinition($relationship, $relationshipConfig);
            $this->addRelationship($relationship, $definition);
        }
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getEntityClass()
    {
        return $this->entityClass;
    }

    /**
     * @param mixed $entityClass
     *
     * @return AttributeDefinition
     */
    public function setEntityClass($entityClass)
    {
        $this->entityClass = $entityClass;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAlias()
    {
        if (!$this->alias) {
            $classParts  = explode('\\', $this->entityClass);
            $alias       = array_pop($classParts);
            $alias       = ucfirst(str_replace('_', ' ', Inflector::tableize($alias)));
            $this->alias = $alias;
        }

        return $this->alias;
    }

    /**
     * @param mixed $alias
     *
     * @return AttributeDefinition
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * @param string              $attribute
     * @param AttributeDefinition $definition
     *
     * @return TypeDefinition
     */
    public function addAttribute(string $attribute, AttributeDefinition $definition): self
    {
        $this->attributes[$attribute] = $definition;

        return $this;
    }

    /**
     * @return AttributeDefinition[]
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param string $attribute
     *
     * @return AttributeDefinition
     */
    public function getAttribute(string $attribute): AttributeDefinition
    {
        if (!isset($this->attributes[$attribute])) {
            throw new \InvalidArgumentException("Attribute '$attribute' is not defined'");
        }

        return $this->attributes[$attribute];
    }

    /**
     * @param string                 $relationship
     * @param RelationshipDefinition $definition
     *
     * @return TypeDefinition
     */
    public function addRelationship(string $relationship, RelationshipDefinition $definition): self
    {
        $this->relationships[$relationship] = $definition;

        return $this;
    }

    /**
     * @return RelationshipDefinition[]
     */
    public function getRelationships()
    {
        return $this->relationships;
    }

    /**
     * @param string $relationship
     *
     * @return RelationshipDefinition
     */
    public function getRelationship(string $relationship): RelationshipDefinition
    {
        if (!isset($this->relationships[$relationship])) {
            throw new \InvalidArgumentException("Relationship '$relationship' is not defined'");
        }

        return $this->relationships[$relationship];
    }

    /**
     * @return string[]
     */
    public function getSubtypes()
    {
        return $this->subtypes;
    }

    /**
     * @param array $subtypes
     *
     * @return TypeDefinition
     */
    public function setSubtypes($subtypes)
    {
        $this->subtypes = $subtypes;

        return $this;
    }

    /**
     * @return string
     */
    public function getSubtypeOf()
    {
        return $this->subtypeOf;
    }

    /**
     * @param string $subtypeOf
     *
     * @return TypeDefinition
     */
    public function setSubtypeOf($subtypeOf)
    {
        $this->subtypeOf = $subtypeOf;

        return $this;
    }

    public function getBaseType()
    {
        if ($this->subtypeOf) {
            return $this->subtypeOf;
        }

        return $this->name;
    }

    /**
     * @return string[]
     */
    public function getCustomFilters()
    {
        return $this->customFilters;
    }

    /**
     * @param array $subtypes
     *
     * @return TypeDefinition
     */
    public function setCustomFilters($customFilters)
    {
        $this->customFilters = $customFilters;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getConcreteTypes()
    {
        return !empty($this->subtypes) ? $this->subtypes : [$this->name];
    }

    /**
     * @return string[]
     */
    public function getFields()
    {
        return array_merge(array_keys($this->attributes), array_keys($this->relationships));
    }

    public function getIncludableRelationships()
    {
        $names = [];
        foreach ($this->getRelationships() as $name => $definition) {
            if ($definition->isIncludable()) {
                $names[] = $name;
            }
        }

        return $names;
    }

    public function getSortableAttributes()
    {
        $names = [];
        foreach ($this->getAttributes() as $name => $definition) {
            if ($definition->isSortable()) {
                $names[] = $name;
            }
        }

        return $names;
    }

    public function getFilterableAttributes()
    {
        $names = [];
        foreach ($this->getAttributes() as $name => $definition) {
            if ($definition->isFilterable()) {
                $names[] = $name;
            }
        }
        foreach ($this->getCustomFilters() as $name) {
            $names[] = $name;
        }

        return $names;
    }

    /**
     * @return string
     */
    public function getDefaultSort()
    {
        return $this->defaultSort;
    }

    /**
     * @param string $defaultSort
     *
     * @return TypeDefinition
     */
    public function setDefaultSort($defaultSort)
    {
        $this->defaultSort = $defaultSort;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getCacheResults()
    {
        return $this->cacheResults;
    }

    /**
     * @param boolean $cacheResults
     *
     * @return TypeDefinition
     */
    public function setCacheResults($cacheResults)
    {
        $this->cacheResults = $cacheResults;

        return $this;
    }
}
