<?php

namespace BBCWorldWide\JsonApi\JsonApi\Schema;

use BBCWorldWide\JsonApi\JsonApi\Data\Error\Error;
use BBCWorldWide\JsonApi\JsonApi\EntityInterface;
use BBCWorldWide\JsonApi\JsonApi\Exception\JsonApiException;

class Schema
{
    /**
     * @var TypeDefinition[]
     */
    protected $types = [];

    /**
     * Schema constructor.
     *
     * @param array|null $config
     */
    public function __construct(array $config = [])
    {
        foreach ($config as $type => $typeConfig) {
            // Set 'subtypes' key using 'subtypeOf' key on other types.
            if (!isset($typeConfig['subtypes'])) {
                $typeConfig['subtypes'] = [];
                $otherTypes             = $config;
                foreach ($otherTypes as $otherType => $otherOptions) {
                    if (isset($otherOptions['subtypeOf']) && $otherOptions['subtypeOf'] === $type) {
                        $typeConfig['subtypes'][] = $otherType;
                    }
                }
            }

            $definition = new TypeDefinition($type, $typeConfig);
            $this->addType($type, $definition);
        }
    }

    /**
     * @param string         $type
     * @param TypeDefinition $definition
     *
     * @return Schema
     */
    public function addType(string $type, TypeDefinition $definition): self
    {
        $this->types[$type] = $definition;

        return $this;
    }

    /**
     * @return TypeDefinition[]
     */
    public function getTypes()
    {
        return $this->types;
    }

    /**
     * @param string $type
     *
     * @return bool
     */
    public function hasType(string $type): bool
    {
        return isset($this->types[$type]);
    }

    /**
     * @param string $type
     *
     * @return TypeDefinition
     *
     * @throws JsonApiException
     */
    public function getType(string $type): TypeDefinition
    {
        if (!isset($this->types[$type])) {
            $error = new Error('not-found', 'Page not found', "'$type' is not a valid resource type", 404);
            throw new JsonApiException($error);
        }

        return $this->types[$type];
    }

    /**
     * @param string $entityClass
     *
     * @return TypeDefinition
     */
    public function getTypeByClassName(string $entityClass)
    {
        foreach ($this->types as $type => $definition) {
            if ($definition->getEntityClass() == $entityClass) {
                return $definition;
            }
        }
    }

    /**
     * @param EntityInterface $resource
     *
     * @return TypeDefinition|null
     */
    public function getTypeForResource(EntityInterface $resource)
    {
        $type = $this->getTypeByClassName(get_class($resource));

        // Try parent classes - allows use of Doctrine proxies.
        return $type ? $type : $this->getTypeByClassName(get_parent_class($resource));
    }
}
