<?php

namespace BBCWorldWide\JsonApi\JsonApi\Schema;

use BBCWorldWide\JsonApi\Util\Inflector;

class RelationshipDefinition
{
    /**
     * @var string Machine name of relationship
     */
    protected $name;
    /**
     * @var string Hyphenated name of relationship as used in URLs / paths.
     */
    protected $pathName;
    /**
     * @var string Human readable name of relationship for error messages etc.
     */
    protected $alias;
    /**
     * @var string Singular name of relationship. Generated from inflector by default.
     */
    protected $singular;
    protected $readOnly         = false;
    protected $includable       = true;
    protected $includeByDefault = false;
    /**
     * @var bool Include reference to resource in relationship data property even if it's not included.
     */
    protected $referenced = false;
    protected $required   = false;
    /**
     * @var bool Is a 'to-many' relationship
     */
    protected $multiple = false;
    /**
     * @var string Resource type(s) that this relationship can reference.
     */
    protected $types = [];

    /**
     * RelationshipDefinition constructor.
     *
     * @param string     $name
     * @param array|null $config
     */
    public function __construct(string $name, array $config = null)
    {
        $this->name             = $name;
        $this->alias            = $config['alias'] ?? $this->alias;
        $this->multiple         = $config['multiple'] ?? $this->multiple;
        $this->singular         = $config['singular'] ?? $this->singular;
        $this->readOnly         = $config['readOnly'] ?? $this->readOnly;
        $this->includable       = $config['includable'] ?? $this->includable;
        $this->includeByDefault = $config['includeByDefault'] ?? $this->includeByDefault;
        $this->required         = $config['required'] ?? $this->required;
        $this->types            = $config['types'] ?? $this->types;

        // To-one relationships include reference by default. They are usually foreign keys, which won't
        // require an additional query.
        $this->referenced = $config['referenced'] ?? !$this->multiple;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return RelationshipDefinition
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getPathName()
    {
        if (!$this->pathName) {
            $this->pathName = Inflector::hyphenate($this->name);
        }

        return $this->pathName;
    }

    /**
     * @return mixed
     */
    public function getAlias()
    {
        if (!$this->alias) {
            $this->alias = Inflector::words($this->name);
        }

        return $this->alias;
    }

    /**
     * @param string $alias
     *
     * @return RelationshipDefinition
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * @return string
     */
    public function getSingular()
    {
        if (!$this->singular) {
            $this->singular = Inflector::singularize($this->name);
        }

        return $this->singular;
    }

    /**
     * @param string $singular
     *
     * @return RelationshipDefinition
     */
    public function setSingular($singular)
    {
        $this->singular = $singular;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isReadOnly()
    {
        return $this->readOnly;
    }

    /**
     * @param boolean $readOnly
     *
     * @return RelationshipDefinition
     */
    public function setReadOnly($readOnly)
    {
        $this->readOnly = $readOnly;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isIncludable()
    {
        return $this->includable;
    }

    /**
     * @param boolean $includable
     *
     * @return RelationshipDefinition
     */
    public function setIncludable($includable)
    {
        $this->includable = $includable;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isIncludeByDefault()
    {
        return $this->includeByDefault;
    }

    /**
     * @param boolean $includeByDefault
     *
     * @return RelationshipDefinition
     */
    public function setIncludeByDefault($includeByDefault)
    {
        $this->includeByDefault = $includeByDefault;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isReferenced()
    {
        return $this->referenced;
    }

    /**
     * @param boolean $referenced
     *
     * @return RelationshipDefinition
     */
    public function setReferenced($referenced)
    {
        $this->referenced = $referenced;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isRequired()
    {
        return $this->required;
    }

    /**
     * @param boolean $required
     *
     * @return RelationshipDefinition
     */
    public function setRequired($required)
    {
        $this->required = $required;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isMultiple()
    {
        return $this->multiple;
    }

    /**
     * @param boolean $multiple
     *
     * @return RelationshipDefinition
     */
    public function setMultiple($multiple)
    {
        $this->multiple = $multiple;

        return $this;
    }

    /**
     * @return array
     */
    public function getTypes()
    {
        return $this->types;
    }

    /**
     * @param array $types
     *
     * @return RelationshipDefinition
     */
    public function setTypes($types)
    {
        $this->types = $types;

        return $this;
    }
}
