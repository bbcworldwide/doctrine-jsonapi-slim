<?php

namespace BBCWorldWide\JsonApi\JsonApi\Encoder;

use BBCWorldWide\JsonApi\JsonApi\Data\Document;
use BBCWorldWide\JsonApi\JsonApi\Data\Relationship;
use BBCWorldWide\JsonApi\JsonApi\Data\Resource as Resrc;
use BBCWorldWide\JsonApi\JsonApi\Data\ResourceIdentifier;
use BBCWorldWide\JsonApi\JsonApi\Data\ResourceIdentifierCollection;
use BBCWorldWide\JsonApi\JsonApi\EntityInterface;
use BBCWorldWide\JsonApi\JsonApi\Parameters\ParameterSet;
use BBCWorldWide\JsonApi\JsonApi\Schema\AttributeDefinition;
use BBCWorldWide\JsonApi\JsonApi\Schema\Schema;
use BBCWorldWide\JsonApi\JsonApi\Schema\TypeDefinition;
use BBCWorldWide\JsonApi\JsonApi\Store;
use BBCWorldWide\JsonApi\Util\Methodiser;
use Doctrine\Common\Collections\Collection;

/**
 * Entity Encoder
 *
 * Encodes Entity objects into JSON API objects
 */
class EntityEncoder
{
    /**
     * @var EntityInterface
     */
    protected $entity;

    /**
     * @var Schema
     */
    protected $schema;

    /**
     * @var Store
     */
    protected $store;

    /**
     * @var ParameterSet
     */
    protected $parameters;

    /**
     * @var TypeDefinition|null
     */
    protected $typeDefinition;

    /**
     * Serializer constructor.
     *
     * @param Schema $schema
     * @param Store  $store
     */
    public function __construct(
        Schema $schema,
        Store $store
    ) {
        $this->schema = $schema;
        $this->store  = $store;
    }

    /**
     * Encodes Entity into a Resource object
     *
     * @param EntityInterface $entity
     * @param ParameterSet    $parameters
     *
     * @return Resrc|null
     */
    public function encodeResource(EntityInterface $entity = null, ParameterSet $parameters)
    {
        if (!$entity) {
            return null;
        }

        $this->entity         = $entity;
        $this->parameters     = $parameters;
        $this->typeDefinition = $this->schema->getTypeForResource($entity);

        $resource = new Resrc($this->typeDefinition->getName(), $this->entity->getId());
        $this->addAttributes($resource);
        $this->addRelationships($resource);

        return $resource;
    }

    /**
     * Encodes Entity into a Document object containing Resource data
     *
     * @param EntityInterface|null $entity
     * @param ParameterSet         $parameters
     *
     * @return Document
     */
    public function encodeResourceDocument(EntityInterface $entity = null, ParameterSet $parameters)
    {
        $document = new Document();
        if ($entity) {
            $resource = $this->encodeResource($entity, $parameters);
            $document->setData($resource)
                     ->setIncludedResources($resource->getIncludedResources());
        }

        return $document;
    }

    /**
     * Encodes Entity into a Resource identifier object
     *
     * @param EntityInterface|null $entity
     *
     * @return ResourceIdentifier|null
     */
    public function encodeIdentifier(EntityInterface $entity = null)
    {
        if (!$entity) {
            return null;
        }
        $this->typeDefinition = $this->schema->getTypeForResource($entity);

        if ($this->typeDefinition !== null) {
            return new ResourceIdentifier($this->typeDefinition->getName(), $entity->getId());
        }
    }

    /**
     * Encodes Entity into a Document object containing a Resource identifier
     *
     * @param EntityInterface|null $entity
     *
     * @return Document
     */
    public function encodeIdentifierDocument(EntityInterface $entity = null)
    {
        $document = new Document();
        if ($entity) {
            $identifier = $this->encodeIdentifier($entity);
            $document->setData($identifier);
        }

        return $document;
    }

    /**
     * @param Resrc $resource
     */
    protected function addAttributes(Resrc $resource)
    {
        $type = $resource->getType();

        foreach ($this->typeDefinition->getAttributes() as $attribute => $definition) {
            // Skip excluded attributes in sparse fieldsets.
            $fields = $this->parameters->getFieldsForType($type);
            if (!empty($fields && !in_array($attribute, $fields))) {
                continue;
            }

            $dataType = $definition->getType();
            $method   = Methodiser::getter($attribute);
            $value    = $this->entity->$method();

            // Only cast/transform value if it isn't null to avoid issues
            if ($value !== null) {
                switch ($dataType) {
                    case AttributeDefinition::TYPE_INT:
                        $value = (int) $value;
                        break;

                    case AttributeDefinition::TYPE_FLOAT:
                        $value = (float) $value;
                        break;

                    case AttributeDefinition::TYPE_BOOL:
                        $value = (bool) $value;
                        break;

                    case AttributeDefinition::TYPE_DATETIME:
                        /** @var \DateTime $value */
                        $value = $value->format(\DateTime::ISO8601);
                }
            }

            $resource->setAttribute($attribute, $value);
        }
    }

    /**
     * @param Resrc $resource
     */
    protected function addRelationships(Resrc $resource)
    {
        $type         = $resource->getType();
        $id           = $resource->getId();
        $encoder      = new EntityEncoder($this->schema, $this->store);
        $fields       = $this->parameters->getFieldsForType($type);
        $includes     = $this->parameters->getIncludes();
        $includeBases = array_map(function ($value) {
            return explode('.', $value)[0];
        }, $includes);

        foreach ($this->typeDefinition->getRelationships() as $relationshipName => $definition) {
            // Skip excluded relationships in sparse fieldsets.
            if (!empty($fields) &&
                !in_array($relationshipName, $fields) &&
                !in_array($relationshipName, $includeBases)
            ) {
                continue;
            }

            $relationship = new Relationship();

            $included = $definition->isIncludeByDefault() ||
                ($definition->isIncludable() && in_array($relationshipName, $includeBases, true));

            $method = Methodiser::getter($relationshipName);

            if ($included) {
                $subParameters = $this->parameters->withIncludes($this->getNestedIncludes($relationshipName));

                if (method_exists($this->entity, $method)) {
                    $results = $this->entity->$method();
                } else {
                    // Use query builder to get relations from store:
                    $results = $this->store
                        ->getRelationQueryBuilder($type, $id, $relationshipName)
                        ->getQuery()
                        ->execute();
                }

                if ($definition->isMultiple()) {
                    $refs = new ResourceIdentifierCollection();

                    foreach ($results as $entity) {
                        // Where we include a position index with results, each item will be an array containing the
                        // actual entity at 0.
                        if (is_array($entity) || $entity instanceof Collection) {
                            $relatedResource = $encoder->encodeResource($entity[0], $subParameters);
                            $identifier      = $relatedResource->getIdentifier();
                            if ($entity['position'] ?? false) {
                                $identifier->addMeta('position', $entity['position']);
                            }
                        } else {
                            $relatedResource = $encoder->encodeResource($entity, $subParameters);
                            $identifier      = $relatedResource->getIdentifier();
                        }
                        $resource->addIncludedResource($relatedResource);
                        $refs->attach($identifier);
                    }

                    $relationship->setData($refs);
                } else {
                    $relatedResource = $encoder->encodeResource($results ?? null, $subParameters);
                    if ($relatedResource) {
                        $resource->addIncludedResource($relatedResource);
                        $relationship->setData($relatedResource->getIdentifier());
                    } else {
                        $relationship->setData(null);
                    }
                }
            } elseif ($definition->isReferenced()) {
                /** @var EntityInterface $related */
                $related = $this->entity->$method();
                $relationship->setData($encoder->encodeIdentifier($related));
            }

            // Build links
            $basePath         = $resource->getPath();
            $relationshipPath = $definition->getPathName();
            $relationship
                ->addLink('self', $basePath . '/relationships/' . $relationshipPath)
                ->addLink('related', $basePath . '/' . $relationshipPath);

            $resource->addRelationship($relationshipName, $relationship);
        }
    }

    /**
     * Get included fields prefixed with a relationship name
     *
     * @param string $relationshipName Relationship name
     *
     * @return array Unprefixed matching includes
     */
    protected function getNestedIncludes(string $relationshipName)
    {
        $prefix       = $relationshipName . '.';
        $prefixLength = strlen($prefix);
        $includes     = [];
        foreach ($this->parameters->getIncludes() as $include) {
            if (strpos($include, $prefix) === 0) {
                $includes[] = substr($include, $prefixLength);
            }
        }

        return $includes;
    }
}
