<?php

namespace BBCWorldWide\JsonApi\JsonApi\Encoder;

use BBCWorldWide\JsonApi\JsonApi\Data\Document;
use BBCWorldWide\JsonApi\JsonApi\Data\DocumentDataInterface;
use BBCWorldWide\JsonApi\JsonApi\Data\ResourceCollection;
use BBCWorldWide\JsonApi\JsonApi\Data\ResourceIdentifierCollection;
use BBCWorldWide\JsonApi\JsonApi\EntityInterface;
use BBCWorldWide\JsonApi\JsonApi\Parameters\ParameterSet;
use BBCWorldWide\JsonApi\JsonApi\Schema\Schema;
use BBCWorldWide\JsonApi\JsonApi\Store;
use BBCWorldWide\JsonApi\Pager\PagedDataInterface;

/**
 * Paged Data Encoder
 *
 * Encodes paginated query results into JSON API objects
 */
class PagedDataEncoder
{
    /**
     * @var Schema
     */
    protected $schema;

    /**
     * @var Store
     */
    protected $store;

    /**
     * Serializer constructor.
     *
     * @param Schema $schema
     * @param Store  $store
     */
    public function __construct(Schema $schema, Store $store)
    {
        $this->schema = $schema;
        $this->store  = $store;
    }

    /**
     * Encode Paged data as a Document of Resource objects
     *
     * @param PagedDataInterface $query
     * @param ParameterSet       $parameters
     *
     * @return Document
     */
    public function encodeResourcesDocument(PagedDataInterface $query, ParameterSet $parameters, $path)
    {
        $collection    = new ResourceCollection();
        $entityEncoder = new EntityEncoder($this->schema, $this->store);

        /** @var EntityInterface $entity */
        foreach ($query->getData() as $entity) {
            if (is_array($entity)) {
                $resource = $entityEncoder->encodeResource($entity[0], $parameters);
                $resource->addMeta('position', $entity['position']);
            } else {
                $resource = $entityEncoder->encodeResource($entity, $parameters);
            }
            $collection->attach($resource);
        }

        $document = $this->buildDocument($query, $parameters, $path, $collection);
        $document->setIncludedResources($collection->getIncludedResources());

        return $document;
    }

    /**
     * Encode Paged data as a Document of Resource Identifier objects
     *
     * @param PagedDataInterface $query
     * @param ParameterSet       $parameters
     * @param                    $path
     *
     * @return Document
     */
    public function encodeIdentifiersDocument(PagedDataInterface $query, ParameterSet $parameters, $path)
    {
        $collection    = new ResourceIdentifierCollection();
        $entityEncoder = new EntityEncoder($this->schema, $this->store);

        /** @var EntityInterface|array $entity */
        foreach ($query->getData() as $entity) {
            if (is_array($entity)) {
                $identifier = $entityEncoder->encodeIdentifier($entity[0]);
                $identifier->addMeta('position', $entity['position']);
            } else {
                $identifier = $entityEncoder->encodeIdentifier($entity);
            }

            if ($identifier !== null) {
                $collection->attach($identifier);
            }
        }

        return $this->buildDocument($query, $parameters, $path, $collection);
    }

    /**
     * Wraps data in a Document object with pagination metadata
     *
     * @param PagedDataInterface    $query
     * @param ParameterSet          $parameters
     * @param DocumentDataInterface $data
     *
     * @return Document
     */
    protected function buildDocument(PagedDataInterface $query, ParameterSet $parameters, $path, $data)
    {
        $document = new Document();
        $document->setData($data)
                 ->setMeta([
                     'results'    => $query->getCount(),
                     'pageNumber' => $query->getCurrentPage(),
                     'pageSize'   => $query->getPageSize(),
                     'totalPages' => ceil($query->getCount() / $query->getPageSize()),
                 ]);

        $document->addLink('self', $path . '?' . $parameters->withPageNumber($query->getCurrentPage()))
                 ->addLink('first', $path . '?' . $parameters->withPageNumber(1))
                 ->addLink('last', $path . '?' . $parameters->withPageNumber($query->getLastPage()));

        if ($query->getCurrentPage() > 1) {
            $document->addLink('prev', $path . '?' . $parameters->withPageNumber($query->getCurrentPage() - 1));
        }
        if ($query->getCurrentPage() < $query->getLastPage()) {
            $document->addLink('next', $path . '?' . $parameters->withPageNumber($query->getCurrentPage() + 1));
        }

        return $document;
    }
}
