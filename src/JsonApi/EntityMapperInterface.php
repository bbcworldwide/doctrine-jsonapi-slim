<?php

namespace BBCWorldWide\JsonApi\JsonApi;

/**
 * Entity mapper interface.
 */
interface EntityMapperInterface
{
    /**
     * Update an entity's properties and relationships from a Json:API Resource Object.
     *
     * Only the properties/relationships present in the object are updated, allowing partial updates.
     *
     * @param EntityInterface $entity
     * @param object          $resourceObject
     */
    public function mapResourceObject(EntityInterface $entity, $resourceObject): void;

    /**
     * Update a named relationship on an entity with references defined in a Json:API Relationship Object.
     *
     * This removes any existing references that are not present in the object.
     *
     * @param EntityInterface $entity
     * @param string          $relationshipName
     * @param \stdClass       $relationshipObject
     */
    public function mapRelationship(
        EntityInterface $entity,
        string $relationshipName,
        \stdClass $relationshipObject
    ): void;

    /**
     * Add reference(s) defined in a Json:API Relationship Object to a named relationship on an entity.
     *
     * This leaves any existing references in place.
     *
     * @param EntityInterface $entity
     * @param string          $relationshipName
     * @param \stdClass       $relationshipObject
     */
    public function addToRelationship(
        EntityInterface $entity,
        string $relationshipName,
        \stdClass $relationshipObject
    ): void;

    /**
     * Remove reference(s) defined in a Json:API Relationship Object from a named relationship on an entity.
     *
     * $relationshipObject can only be null if the relationship is single.
     *
     * @param EntityInterface $entity
     * @param string          $relationshipName
     * @param \stdClass       $relationshipObject
     */
    public function deleteFromRelationship(
        EntityInterface $entity,
        string $relationshipName,
        \stdClass $relationshipObject = null
    ): void;
}
