<?php

namespace BBCWorldWide\JsonApi\JsonApi;

use BBCWorldWide\JsonApi\JsonApi\Schema\Schema;
use BBCWorldWide\JsonApi\JsonApi\Schema\AttributeDefinition;
use BBCWorldWide\JsonApi\Pager\PagedQuery;
use BBCWorldWide\JsonApi\Util\Inflector;

/**
 * Swagger schema converter.
 */
class Swagger
{
    /**
     * @var Schema
     */
    protected $schema;

    /**
     * Swagger constructor.
     *
     * @param Schema $schema
     */
    public function __construct(Schema $schema)
    {
        $this->schema = $schema;
    }

    /**
     * Generate data for Swagger JSON schema.
     *
     * @param        $host
     * @param string $basePath
     * @param array  $schemes
     *
     * @return array
     */
    public function generate($host, $basePath = '', $schemes = ['https']): array
    {
        // Base service definition.
        $output = [
            'swagger'  => '2.0',
            'info'     => [],
            'host'     => $host,
            'basePath' => $basePath,
            'tags'     => [],
            'schemes'  => $schemes,
        ];

        // Define error schema:
        $output['definitions']['Errors'] = [
            'type'       => 'object',
            'properties' => [
                'errors' => [
                    'type'  => 'array',
                    'items' => [
                        'type'       => 'object',
                        'properties' => [
                            'status'  => ['type' => 'string'],
                            'code'    => ['type' => 'string'],
                            'title'   => ['type' => 'string'],
                            'detail'  => ['type' => 'string'],
                            'pointer' => ['type' => 'string'],
                        ],
                    ],
                ],
            ],
        ];

        // Set up re-usable elements:

        $mimeType        = ['application/vnd.api+json'];
        $mimeTypeMapping = [
            'consumes' => $mimeType,
            'produces' => $mimeType,
        ];

        $paginationMeta  = [
            'type'       => 'object',
            'properties' => [
                'results'    => ['type' => 'integer'],
                'pageNumber' => ['type' => 'integer'],
                'pageSize'   => ['type' => 'integer'],
                'totalPages' => ['type' => 'integer'],
            ],
        ];
        $paginationLinks = [
            'type'       => 'object',
            'properties' => [
                'self'  => ['type' => 'string'],
                'first' => ['type' => 'string'],
                'last'  => ['type' => 'string'],
                'prev'  => ['type' => 'string'],
                'next'  => ['type' => 'string'],
            ],
        ];
        $pageParams      = [
            [
                'in'          => 'query',
                'name'        => 'page[number]',
                'description' => 'Page number (1 based index)',
                'type'        => 'integer',
                'default'     => 1,
            ],
            [
                'in'          => 'query',
                'name'        => 'page[size]',
                'description' => 'Number of results per page',
                'type'        => 'integer',
                'default'     => PagedQuery::DEFAULT_PAGE_SIZE,
            ],
        ];

        // Define fields for all types for use in params.
        $fieldParameters = [];
        foreach ($this->schema->getTypes() as $t => $d) {
            $fieldParameters[] = [
                'in'          => 'query',
                'name'        => 'fields[' . $t . ']',
                'description' => 'Limit returned fields for ' . $d->getAlias() . ' resources',
                'type'        => 'array',
                'items'       => ['type' => 'string', 'enum' => $d->getFields()],
            ];
        }

        // Map our attribute type constants to Swagger types.
        $typeMap = [
            AttributeDefinition::TYPE_BOOL     => 'boolean',
            AttributeDefinition::TYPE_STRING   => 'string',
            AttributeDefinition::TYPE_INT      => 'integer',
            AttributeDefinition::TYPE_FLOAT    => 'float',
            AttributeDefinition::TYPE_DATETIME => 'dateTime',
        ];

        foreach ($this->schema->getTypes() as $type => $definition) {
            $alias = $definition->getAlias();
            // Format alias for use in definition and operation IDs.
            $niceType = Inflector::classify($alias);

            // Define common parameters for this type:

            $bodyParam = [
                'in'          => 'body',
                'name'        => 'body',
                'description' => $alias . ' object',
                'required'    => true,
                'schema'      => [
                    'type'       => 'object',
                    'properties' => [
                        'data' => ['$ref' => '#/definitions/' . $niceType],
                    ],
                ],
            ];

            $idParam = [
                'in'          => 'path',
                'name'        => 'id',
                'description' => $alias . ' ID',
                'required'    => true,
                'type'        => 'string',
            ];

            $sortables = "\n * " . implode("\n * ", $definition->getSortableAttributes()) . "\n\n";

            $jsonApiParams = array_merge([
                [
                    'in'          => 'query',
                    'name'        => 'sort',
                    'description' => 'Sort by fields:' . $sortables . 'Prefix with \'-\' for descending',
                    'type'        => 'array',
                    'items'       => [
                        'type' => 'string',
                    ],
                ],
                [
                    'in'          => 'query',
                    'name'        => 'include',
                    'description' => 'Include relationship(s)',
                    'type'        => 'array',
                    'items'       => [
                        'type' => 'string',
                        'enum' => $definition->getIncludableRelationships(),
                    ],
                ],
            ], $fieldParameters);

            foreach ($definition->getFilterableAttributes() as $attribute) {
                $jsonApiParams[] = [
                    'in'          => 'query',
                    'name'        => 'filter[' . $attribute . ']',
                    'description' => 'Filter by ' . $attribute . ' attribute',
                    'type'        => 'string',
                ];
            }

            // Index endpoint.
            $output['paths']['/' . $type]['get'] = [
                'summary'     => 'List ' . $alias . ' objects',
                'operationId' => 'find' . $niceType,
                'produces'    => $mimeType,
                'parameters'  => array_merge($pageParams, $jsonApiParams),
                'responses'   => [
                    200 => [
                        'description' => 'Successful operation',
                        'schema'      => [
                            'type'       => 'object',
                            'properties' => [
                                'metadata' => $paginationMeta,
                                'links'    => $paginationLinks,
                                'data'     => [
                                    'type'  => 'array',
                                    'items' => ['$ref' => '#/definitions/' . $niceType],
                                ],
                            ],
                        ],
                    ],
                ],
            ];

            // Get Resource endpoint.
            $output['paths']['/' . $type . '/{id}']['get'] = [
                'summary'     => 'List ' . $alias . ' objects',
                'operationId' => 'get' . $niceType,
                'produces'    => $mimeType,
                'parameters'  => [$idParam],
                'responses'   => [
                    200 => [
                        'description' => 'Successful operation',
                        'schema'      => [
                            'type'       => 'object',
                            'properties' => [
                                'data' => ['$ref' => '#/definitions/' . $niceType],
                            ],
                        ],
                    ],
                    404 => [
                        'description' => 'Not found',
                        'schema'      => ['$ref' => '#/definitions/Errors'],
                    ],
                ],
            ];

            // Define additional endpoints for concrete resource types.
            if (empty($definition->getSubtypes())) {
                // Create Resource (post) endpoint.
                $output['paths']['/' . $type]['post'] = [
                        'summary'     => 'Add a new ' . $alias . ' to the catalogue',
                        'operationId' => 'add' . $niceType,
                        'parameters'  => [$bodyParam],
                        'responses'   => [
                            204 => ['description' => 'Successful operation with supplied ID'],
                            200 => [
                                'description' => 'Successful operation with generated ID',
                                'schema'      => [
                                    'type'       => 'object',
                                    'properties' => [
                                        'data' => ['$ref' => '#/definitions/' . $niceType],
                                    ],
                                ],
                            ],
                            409 => [
                                'description' => 'Conflict',
                                'schema'      => ['$ref' => '#/definitions/Errors'],
                            ],
                        ],
                    ] + $mimeTypeMapping;

                // Update Resource (patch) endpoint.
                $output['paths']['/' . $type . '/{id}']['patch'] = [
                        'summary'     => 'Update an existing ' . $alias,
                        'operationId' => 'update' . $niceType,
                        'parameters'  => [$idParam, $bodyParam],
                        'responses'   => [
                            200 => [
                                'description' => 'Successful operation',
                                'schema'      => [
                                    'type'       => 'object',
                                    'properties' => [
                                        'data' => ['$ref' => '#/definitions/' . $niceType],
                                    ],
                                ],
                            ],
                            404 => [
                                'description' => 'Not found',
                                'schema'      => ['$ref' => '#/definitions/Errors'],
                            ],
                            400 => [
                                'description' => 'Bad request',
                                'schema'      => ['$ref' => '#/definitions/Errors'],
                            ],
                        ],
                    ] + $mimeTypeMapping;

                // Replace Resource (put) endpoint.
                $output['paths']['/' . $type . '/{id}']['put'] = [
                        'summary'     => 'Replace an existing ' . $alias,
                        'operationId' => 'replace' . $niceType,
                        'parameters'  => [$idParam, $bodyParam],
                        'responses'   => [
                            204 => ['description' => 'Successful operation'],
                            404 => [
                                'description' => 'Not found',
                                'schema'      => ['$ref' => '#/definitions/Errors'],
                            ],
                            400 => [
                                'description' => 'Bad request',
                                'schema'      => ['$ref' => '#/definitions/Errors'],
                            ],
                        ],
                    ] + $mimeTypeMapping + $bodyParam;

                // Delete Resource (delete) endpoint.
                $output['paths']['/' . $type . '/{id}']['delete'] = [
                    'summary'     => 'Delete an existing ' . $alias,
                    'operationId' => 'delete' . $niceType,
                    'parameters'  => [$idParam],
                    'responses'   => [
                        204 => ['description' => 'Successful operation'],
                        404 => [
                            'description' => 'Not found',
                            'schema'      => ['$ref' => '#/definitions/Errors'],
                        ],
                    ],
                ];
            }

            $relationships = [];
            foreach ($definition->getRelationships() as $relationship => $relationshipDefinition) {
                $relAlias             = $relationshipDefinition->getAlias();
                $niceRelationshipType = Inflector::classify($relAlias);
                $defType              = $niceType . '_' . $niceRelationshipType;

                // Add definition for relationship object.
                $ref = [
                    'type'       => 'object',
                    'properties' => [
                        'id'   => ['type' => 'string', 'required' => true],
                        'type' => [
                            'type'     => 'string',
                            'required' => true,
                            'enum'     => $relationshipDefinition->getTypes(),
                        ],
                    ],
                    'required'   => $relationshipDefinition->isRequired(),
                ];
                if ($relationshipDefinition->isMultiple()) {
                    $ref = ['type' => 'array', 'items' => $ref];
                }

                $output['definitions'][$defType] = [
                    'type'       => 'object',
                    'properties' => [
                        'data' => $ref,
                    ],
                ];

                // Store reference to add to Resource definition later.
                $relationships[$relationship] = [
                    '$ref' => '#/definitions/' . $defType,
                ];

                // Define 'related' and 'relationship' endpoints for concrete resource types.
                if (empty($definition->getSubtypes())) {
                    // Build path component for relationship.
                    $relPath = Inflector::hyphenate($relationship);

                    // Get base type of related resources.
                    $relatedTypes    = $relationshipDefinition->getTypes();
                    $relatedType     = $this->schema->getType($relatedTypes[0])->getBaseType();
                    $niceRelatedType = Inflector::classify($this->schema->getType($relatedType)->getAlias());

                    // Define schema for toOne/toMany relation using base type.
                    $relatedSchema = ['$ref' => '#/definitions/' . $niceRelatedType];
                    if ($relationshipDefinition->isMultiple()) {
                        $relatedSchema = ['type' => 'array', 'items' => $relatedSchema];
                    }

                    $relBodyParam = [
                        'in'          => 'body',
                        'name'        => 'body',
                        'description' => $relAlias . ' relationship',
                        'required'    => true,
                        'schema'      => [
                            '$ref' => '#/definitions/' . $defType,
                        ],
                    ];

                    // Related resource(s) endpoint for relationship.
                    $output['paths']['/' . $type . '/{id}/' . $relPath]['get'] = [
                        'summary'     => 'Get related ' . $relAlias . ' for ' . $alias,
                        'operationId' => 'get' . $niceType . 'Related' . $niceRelationshipType,
                        'produces'    => $mimeType,
                        'parameters'  => array_merge([$idParam], $pageParams),
                        'responses'   => [
                            200 => [
                                'description' => 'Successful operation',
                                'schema'      => [
                                    'type'       => 'object',
                                    'properties' => [
                                        'metadata' => $paginationMeta,
                                        'links'    => $paginationLinks,
                                        'data'     => $relatedSchema,
                                    ],
                                ],
                            ],
                            404 => [
                                'description' => 'Not found',
                                'schema'      => ['$ref' => '#/definitions/Errors'],
                            ],
                        ],
                    ];

                    // Get relationship endpoint.
                    $output['paths']['/' . $type . '/{id}/relationships/' . $relPath]['get'] = [
                        'summary'     => 'Get ' . $relAlias . ' relationships for ' . $alias,
                        'operationId' => 'get' . $niceType . $niceRelationshipType,
                        'produces'    => $mimeType,
                        'parameters'  => array_merge([$idParam], $pageParams),
                        'responses'   => [
                            200 => [
                                'description' => 'Successful operation',
                                'schema'      => [
                                    'type'       => 'object',
                                    'properties' => [
                                        'metadata' => $paginationMeta,
                                        'links'    => $paginationLinks,
                                        'data'     => $ref,
                                    ],
                                ],
                            ],
                            404 => [
                                'description' => 'Not found',
                                'schema'      => ['$ref' => '#/definitions/Errors'],
                            ],
                        ],
                    ];

                    // Define additional endpoints for writable relationships.
                    if (!$relationshipDefinition->isReadOnly()) {
                        // Add Relationship (post) endpoint.
                        $output['paths']['/' . $type . '/{id}/relationships/' . $relPath]['post'] = [
                            'summary'     => 'Add ' . $relAlias . ' relationship for ' . $alias,
                            'operationId' => 'add' . $niceType . $niceRelationshipType,
                            'produces'    => $mimeType,
                            'parameters'  => [$idParam, $relBodyParam],
                            'responses'   => [
                                204 => ['description' => 'Successful operation'],
                                404 => [
                                    'description' => 'Not found',
                                    'schema'      => ['$ref' => '#/definitions/Errors'],
                                ],
                                400 => [
                                    'description' => 'Bad request',
                                    'schema'      => ['$ref' => '#/definitions/Errors'],
                                ],
                            ],
                        ];

                        // Update Relationship (patch) endpoint.
                        $output['paths']['/' . $type . '/{id}/relationships/' . $relPath]['patch'] = [
                            'summary'     => 'Update ' . $relAlias . ' relationships for ' . $alias,
                            'operationId' => 'update' . $niceType . $niceRelationshipType,
                            'produces'    => $mimeType,
                            'parameters'  => [$idParam, $relBodyParam],
                            'responses'   => [
                                204 => ['description' => 'Successful operation'],
                                404 => [
                                    'description' => 'Not found',
                                    'schema'      => ['$ref' => '#/definitions/Errors'],
                                ],
                                400 => [
                                    'description' => 'Bad request',
                                    'schema'      => ['$ref' => '#/definitions/Errors'],
                                ],
                            ],
                        ];

                        // Replace Relationship (put) endpoint.
                        $output['paths']['/' . $type . '/{id}/relationships/' . $relPath]['put'] = [
                            'summary'     => 'Replace ' . $relAlias . ' relationships for ' . $alias,
                            'operationId' => 'replace' . $niceType . $niceRelationshipType,
                            'produces'    => $mimeType,
                            'parameters'  => [$idParam, $relBodyParam],
                            'responses'   => [
                                204 => ['description' => 'Successful operation'],
                                404 => [
                                    'description' => 'Not found',
                                    'schema'      => ['$ref' => '#/definitions/Errors'],
                                ],
                                400 => [
                                    'description' => 'Bad request',
                                    'schema'      => ['$ref' => '#/definitions/Errors'],
                                ],
                            ],
                        ];

                        // Delete Relationship endpoint.
                        $output['paths']['/' . $type . '/{id}/relationships/' . $relPath]['delete'] = [
                            'summary'     => 'Delete ' . $relAlias . ' relationships for ' . $alias,
                            'operationId' => 'delete' . $niceType . $niceRelationshipType,
                            'produces'    => $mimeType,
                            'parameters'  => [$idParam, $relBodyParam],
                            'responses'   => [
                                204 => ['description' => 'Successful operation'],
                                404 => [
                                    'description' => 'Not found',
                                    'schema'      => ['$ref' => '#/definitions/Errors'],
                                ],
                                400 => [
                                    'description' => 'Bad request',
                                    'schema'      => ['$ref' => '#/definitions/Errors'],
                                ],
                            ],
                        ];
                    }
                }
            }

            // Add definition for Resource type:

            $subTypes = $definition->getSubtypes();
            $types    = !empty($subTypes) ? $subTypes : [$type];

            $attributes = [];
            foreach ($definition->getAttributes() as $attribute => $attributeDefinition) {
                $attributes[$attribute] = [
                    'type'     => $typeMap[$attributeDefinition->getType()],
                    'required' => $attributeDefinition->isRequired(),
                ];
            }

            $output['definitions'][$niceType] = [
                'type'       => 'object',
                'properties' => [
                    'id'            => [
                        'type' => 'string',
                    ],
                    'type'          => [
                        'type'     => 'string',
                        'required' => true,
                        'enum'     => $types,
                    ],
                    'attributes'    => [
                        'type'       => 'object',
                        'properties' => $attributes,
                    ],
                    'relationships' => [
                        'type'       => 'object',
                        'properties' => $relationships,
                    ],
                ],
            ];
        }

        return $output;
    }
}
