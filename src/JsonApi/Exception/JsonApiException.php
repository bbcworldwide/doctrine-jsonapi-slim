<?php

namespace BBCWorldWide\JsonApi\JsonApi\Exception;

use BBCWorldWide\JsonApi\JsonApi\Data\Error\Error;
use BBCWorldWide\JsonApi\JsonApi\Data\ErrorCollection;

class JsonApiException extends \RuntimeException
{
    /**
     * @var ErrorCollection
     */
    protected $errors;

    /**
     * JsonApiException constructor.
     *
     * @param ErrorCollection|Error $errors
     * @param \Exception            $previous
     */
    public function __construct($errors, $previous = null)
    {
        if ($errors instanceof Error) {
            $this->errors = new ErrorCollection();
            $this->errors->attach($errors);
        } elseif ($errors instanceof ErrorCollection) {
            if (!$errors->count()) {
                throw new \InvalidArgumentException('ErrorCollection cannot be empty');
            }
            $this->errors = $errors;
        } else {
            throw new \InvalidArgumentException('JsonApiException requires an Error or ErrorCollection');
        }

        $code     = 500;
        $messages = [];
        /** @var Error $error */
        foreach ($this->errors as $error) {
            if ($errorCode = $error->getStatus()) {
                $code = min($code, $errorCode);
            }
            $messages[] = $error->getTitle();
        }

        parent::__construct('JsonApi Error', $code);
    }

    /**
     * @return ErrorCollection
     */
    public function getErrors()
    {
        return $this->errors;
    }
}
