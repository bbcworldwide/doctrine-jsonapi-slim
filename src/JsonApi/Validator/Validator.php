<?php

namespace BBCWorldWide\JsonApi\JsonApi\Validator;

use BBCWorldWide\JsonApi\JsonApi\Data\ErrorCollection;
use BBCWorldWide\JsonApi\JsonApi\Schema\Schema;

class Validator
{
    /**
     * @var Schema
     */
    protected $schema;

    /**
     * @var ErrorCollection
     */
    protected $errors;

    /**
     * DocumentValidator constructor.
     *
     * @param Schema $schema
     * @param        $typeClassMap
     */
    public function __construct(Schema $schema)
    {
        $this->schema = $schema;
        $this->errors = new ErrorCollection();
    }

    public function reset()
    {
        $this->errors = new ErrorCollection();
    }

    public function isValid()
    {
        return !$this->errors->count();
    }

    public function getErrors()
    {
        return $this->errors;
    }
}
