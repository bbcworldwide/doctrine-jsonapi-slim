<?php

namespace BBCWorldWide\JsonApi\JsonApi\Validator;

use BBCWorldWide\JsonApi\JsonApi\Data\Error\AttributeError;
use BBCWorldWide\JsonApi\JsonApi\Data\Error\DataError;
use BBCWorldWide\JsonApi\JsonApi\Data\Error\RelationshipError;
use BBCWorldWide\JsonApi\JsonApi\Exception\JsonApiException;
use BBCWorldWide\JsonApi\JsonApi\Schema\AttributeDefinition;
use BBCWorldWide\JsonApi\JsonApi\Schema\RelationshipDefinition;

class DocumentValidator extends Validator
{
    public function ensureObject($object, $pointer = '')
    {
        if (!is_object($object)) {
            $this->errors->attach(new DataError($pointer, 'Invalid document'));
        }

        return $this->isValid();
    }

    public function ensureData($object, $pointer = '')
    {
        if (!property_exists($object, 'data')) {
            $this->errors->attach(new DataError($pointer . '/data', 'Data property is missing'));
        }

        return $this->isValid();
    }

    public function validateResourceObject($resourceObject, $allowedTypes, $partial = false)
    {
        $this->reset();

        if (!$this->ensureObject($resourceObject) || !$this->ensureData($resourceObject)) {
            return false;
        }

        $resourceType = $resourceObject->data->type ?? '';

        if (!$resourceType) {
            $this->errors->attach(new DataError('/data/type', 'Resource type is missing'));

            return false;
        }

        if (!in_array($resourceType, $allowedTypes)) {
            $this->errors->attach(new DataError(
                '/data/type',
                "Type '{$resourceObject->data->type}' is not supported on this end-point"
            ));

            return false;
        }

        $resourceSchema = $this->schema->getType($resourceType);

        foreach ($resourceSchema->getAttributes() as $attribute => $definition) {
            if (isset($resourceObject->data->attributes->$attribute) || !$partial) {
                $value = $resourceObject->data->attributes->$attribute ?? null;
                $this->validateAttribute($value, $definition);
            }
        }

        foreach ($resourceSchema->getRelationships() as $relationship => $definition) {
            if (isset($resourceObject->data->relationships->$relationship) || !$partial) {
                $default            = $definition->isMultiple() ? [] : null;
                $relationshipObject = $resourceObject->data->relationships->$relationship ?? $default;
                $pointer            = '/data/relationships/' . $relationship;
                $this->validateRelationshipObject($relationshipObject, $definition, $pointer);
            }
        }

        return $this->isValid();
    }

    public function validateAttribute($value, AttributeDefinition $definition)
    {
        $name  = $definition->getName();
        $alias = $definition->getAlias();

        if ($value === null || $value === '') {
            if ($definition->isRequired()) {
                $this->errors->attach(new AttributeError($name, "$alias is required"));

                return false;
            }

            return true;
        }

        // Map defined type to values returned by getType().
        $allowedTypes = [
            AttributeDefinition::TYPE_DATETIME => ['string'],
            AttributeDefinition::TYPE_BOOL     => ['boolean'],
            AttributeDefinition::TYPE_INT      => ['integer', 'string'],
            AttributeDefinition::TYPE_FLOAT    => ['integer', 'double', 'string'],
            AttributeDefinition::TYPE_STRING   => ['string'],
        ];

        $type = $definition->getType();

        if (!in_array(gettype($value), $allowedTypes[$type])) {
            $this->errors->attach(new AttributeError($name, "$alias must be a $type"));
        }
        if ($type === AttributeDefinition::TYPE_DATETIME && !$this->assertISO8601Date($value)) {
            $this->errors->attach(new AttributeError($name, "$alias must be a valid ISO8601 date"));
        }
        if ($type === AttributeDefinition::TYPE_INT && !is_int($value) && !ctype_digit($value)) {
            $this->errors->attach(new AttributeError($name, "$alias must be an integer"));
        }
        if ($type === AttributeDefinition::TYPE_FLOAT && !is_numeric($value)) {
            $this->errors->attach(new AttributeError($name, "$alias must be numeric"));
        }

        // Todo: other validation - max/min length, pattern matching etc.

        return $this->isValid();
    }

    public function validateRelationshipObject($relationshipObject, RelationshipDefinition $definition, $pointer = '')
    {
        $name  = $definition->getName();
        $alias = $definition->getAlias();

        if (empty($relationshipObject->data)) {
            if ($definition->isRequired()) {
                $this->errors->attach(new RelationshipError($name, "$alias is required"));

                return false;
            }

            return true;
        }

        if ($definition->isMultiple()) {
            // Check is array.
            if (!is_array($relationshipObject->data)) {
                $this->errors->attach(new RelationshipError($name, "$alias data should be an array"));
            } else {
                foreach ($relationshipObject->data as $identifier) {
                    if ($identifier) {
                        $this->validateResourceIdentifierObject($identifier, $definition, $pointer);
                    }
                }
            }
        } elseif ($relationshipObject->data) {
            $this->validateResourceIdentifierObject($relationshipObject->data, $definition, $pointer);
        }

        // Todo: prevent self-references?

        return $this->isValid();
    }

    public function validateResourceIdentifierObject(
        $resourceIdentifierObject,
        RelationshipDefinition $definition,
        $pointer = ''
    ) {
        $resourceType = $resourceIdentifierObject->type ?? '';
        $resourceId   = $resourceIdentifierObject->id ?? '';

        $typePointer = $pointer . '/data/type';
        $idPointer   = $pointer . '/data/id';

        if ($resourceIdentifierObject != null) {
            if (!$resourceType) {
                $this->errors->attach(new DataError($typePointer, 'Resource type is missing'));
            }
            if (!$resourceId) {
                $this->errors->attach(new DataError($idPointer, 'Resource ID is missing'));
            }

            if (!$this->isValid()) {
                return false;
            }
        }

        $baseType = null;
        try {
            $baseType = $this->schema->getType($resourceType)->getBaseType();
        } catch (JsonApiException $e) {
            $this->errors->attach(new DataError($typePointer, "Type '$resourceType' is not on the schema"));
        }

        if ($baseType !== null &&
            !in_array($resourceType, $definition->getTypes()) &&
            !in_array($baseType, $definition->getTypes())
        ) {
            $this->errors->attach(
                new DataError($typePointer, "Type '$resourceType' is not supported on this relationship")
            );
        }

        return $this->isValid();
    }

    /**
     * @param $dateStr
     *
     * @return bool
     *
     * @link https://gist.github.com/jeremiahlee/2885845
     */
    protected function assertISO8601Date($dateStr)
    {
        $pattern = '/^([\+-]?\d{4}(?!\d{2}\b))((-?)((0[1-9]|1[0-2])(\3([12]\d|0[1-9]|3[01]))?|W([0-4]\d|5[0-2])'
            . '(-?[1-7])?|(00[1-9]|0[1-9]\d|[12]\d{2}|3([0-5]\d|6[1-6])))([T\s]((([01]\d|2[0-3])((:?)[0-5]\d)?|24\:?00)'
            . '([\.,]\d+(?!:))?)?(\17[0-5]\d([\.,]\d+)?)?([zZ]|([\+-])([01]\d|2[0-3]):?([0-5]\d)?)?)?)?$/';
        if (preg_match($pattern, $dateStr) > 0) {
            return true;
        } else {
            return false;
        }
    }
}
