<?php

namespace BBCWorldWide\JsonApi\JsonApi;

use BBCWorldWide\JsonApi\JsonApi\Parameters\ParameterSet;
use BBCWorldWide\JsonApi\Pager\PagedDataInterface;

/**
 * Entity store interface.
 */
interface StoreInterface
{
    /**
     * Get a single entity by resource type and ID.
     *
     * @param string $type Resource type
     * @param mixed  $id   Entity ID
     *
     * @return EntityInterface
     */
    public function findOne($type, $id);

    /**
     * Find entities by resource type - sorted, filtered and paged according to parameters.
     *
     * @param string       $type       Resource type
     * @param ParameterSet $parameters Json:API query parameters
     *
     * @return PagedDataInterface      Paged set of entities
     */
    public function find($type, ParameterSet $parameters);

    /**
     * Find multiple entities through a named relationship - sorted, filtered and paged according to parameters.
     *
     * @param string       $type         Resource type of referencing entity
     * @param mixed        $id           Entity ID
     * @param string       $relationName Name of relationship as defined in schema
     * @param ParameterSet $parameters   Json:API query parameters
     *
     * @return PagedDataInterface       Paged set of entities
     */
    public function findByRelation($type, $id, $relationName, ParameterSet $parameters);

    /**
     * Find a single entity through a named relationship.
     *
     * @param string $type
     * @param mixed  $id
     * @param string $relationName
     *
     * @return EntityInterface
     */
    public function findOneByRelation($type, $id, $relationName);

    /**
     * Save an entity.
     *
     * @param EntityInterface $entity
     */
    public function save(EntityInterface $entity);

    /**
     * Delete an entity.
     *
     * @param EntityInterface $entity
     */
    public function delete(EntityInterface $entity);
}
