<?php

namespace BBCWorldWide\JsonApi\JsonApi\Data;

use BBCWorldWide\JsonApi\JsonApi\Data\Traits\Meta;

/**
 * JSON API Resource Identifier object
 */
class ResourceIdentifier implements \JsonSerializable, RelationshipDataInterface
{
    use Meta;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var mixed
     */
    protected $id;

    /**
     * Resource constructor.
     *
     * @param string $type
     * @param mixed  $id
     */
    public function __construct($type, $id)
    {
        $this->type = $type;
        $this->id   = $id;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get ID
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function jsonSerialize()
    {
        return array_filter([
            'meta' => $this->meta,
            'type' => $this->type,
            'id'   => $this->id,
        ]);
    }
}
