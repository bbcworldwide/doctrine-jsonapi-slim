<?php

namespace BBCWorldWide\JsonApi\JsonApi\Data;

use BBCWorldWide\JsonApi\JsonApi\Data\Error\Error;

/**
 * Error Collection
 */
class ErrorCollection extends \SplObjectStorage implements \JsonSerializable
{
    /**
     * @param Error $resourceIdentifier
     */
    public function getHash($error)
    {
        return json_encode($error);
    }

    public function jsonSerialize()
    {
        $items = [];
        foreach ($this as $item) {
            $items[] = $item;
        }

        return $items;
    }
}
