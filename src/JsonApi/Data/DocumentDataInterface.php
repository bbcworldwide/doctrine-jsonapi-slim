<?php

namespace BBCWorldWide\JsonApi\JsonApi\Data;

/**
 * Document data interface
 *
 * Identifies objects which can provide a root data element in a JSON API document
 */
interface DocumentDataInterface
{
}
