<?php

namespace BBCWorldWide\JsonApi\JsonApi\Data;

use BBCWorldWide\JsonApi\JsonApi\Data\Error\Error;
use BBCWorldWide\JsonApi\JsonApi\Data\Traits\IncludedResources;
use BBCWorldWide\JsonApi\JsonApi\Data\Traits\Links;
use BBCWorldWide\JsonApi\JsonApi\Data\Traits\Meta;

/**
 * JSON API Document object
 */
class Document implements \JsonSerializable
{
    use IncludedResources;
    use Links;
    use Meta;

    /**
     * @var DocumentDataInterface
     */
    protected $data;

    /**
     * @var array
     */
    protected $includedResources;

    /**
     * @var ErrorCollection
     */
    protected $errors;

    /**
     * Document constructor.
     */
    public function __construct()
    {
        $this->includedResources = new ResourceCollection();
        $this->errors            = new ErrorCollection();
    }

    /**
     * Get data
     *
     * @return DocumentDataInterface
     *
     * @codeCoverageIgnore
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set data
     *
     * @param DocumentDataInterface|null $data
     *
     * @return self
     */
    public function setData(DocumentDataInterface $data = null): self
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get errors
     *
     * @return ErrorCollection
     *
     * @codeCoverageIgnore
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Set errors
     *
     * @param ErrorCollection $errors
     *
     * @return self
     */
    public function setErrors(ErrorCollection $errors): self
    {
        $this->errors = $errors;

        return $this;
    }

    /**
     * Add error
     *
     * @param Error $error
     *
     * @return self
     */
    public function addError(Error $error): self
    {
        $this->errors->attach($error);

        return $this;
    }

    public function jsonSerialize()
    {
        // Data should be included even if null
        return [
                'data' => $this->data,
            ] + array_filter([
                'included' => $this->includedResources->jsonSerialize(),
                'links'    => $this->links,
                'meta'     => $this->meta,
                'errors'   => $this->errors->jsonSerialize(),
            ]);
    }

    public function __toString()
    {
        return json_encode($this);
    }
}
