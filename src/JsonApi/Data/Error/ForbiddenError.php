<?php

namespace BBCWorldWide\JsonApi\JsonApi\Data\Error;

class ForbiddenError extends Error
{
    public function __construct($detail = '')
    {
        parent::__construct('forbidden', 'Forbidden', $detail, 403);
    }
}
