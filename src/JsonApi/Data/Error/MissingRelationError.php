<?php

namespace BBCWorldWide\JsonApi\JsonApi\Data\Error;

class MissingRelationError extends Error
{
    public function __construct($detail = '', $code = 400)
    {
        parent::__construct('missing-relation', 'Missing relation', $detail, $code);
    }
}
