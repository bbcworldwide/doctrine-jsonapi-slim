<?php

namespace BBCWorldWide\JsonApi\JsonApi\Data\Error;

/**
 * JSON API Error object
 */
class Error implements \JsonSerializable
{
    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $detail;

    /**
     * @var int
     */
    protected $status;

    /**
     * @var string
     */
    protected $pointer;

    /**
     * @var string
     */
    protected $parameter;

    /**
     * Error constructor.
     *
     * @param $code
     * @param $title
     * @param $detail
     * @param $status
     */
    public function __construct($code, $title, $detail, $status = 400)
    {
        $this->code   = $code;
        $this->title  = $title;
        $this->detail = $detail;
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     *
     * @return Error
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return Error
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getDetail()
    {
        return $this->detail;
    }

    /**
     * @param string $detail
     *
     * @return Error
     */
    public function setDetail($detail)
    {
        $this->detail = $detail;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return Error
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getPointer()
    {
        return $this->pointer;
    }

    /**
     * @param string $pointer
     *
     * @return Error
     */
    public function setPointer($pointer)
    {
        $this->pointer = $pointer;

        return $this;
    }

    /**
     * @return string
     */
    public function getParameter()
    {
        return $this->parameter;
    }

    /**
     * @param string $parameter
     *
     * @return Error
     */
    public function setParameter($parameter)
    {
        $this->parameter = $parameter;

        return $this;
    }

    public function jsonSerialize()
    {
        $output = array_filter([
            'code'   => $this->code,
            'title'  => $this->title,
            'detail' => $this->detail,
            'status' => $this->status,
        ]);

        if ($this->pointer) {
            $output['source']['pointer'] = $this->pointer;
        }
        if ($this->parameter) {
            $output['source']['parameter'] = $this->parameter;
        }

        return $output;
    }
}
