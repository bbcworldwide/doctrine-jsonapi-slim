<?php

namespace BBCWorldWide\JsonApi\JsonApi\Data\Error;

class ConflictError extends Error
{
    public function __construct($detail = '')
    {
        parent::__construct('conflict', 'Conflict', $detail, 409);
    }
}
