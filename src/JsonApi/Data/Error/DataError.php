<?php

namespace BBCWorldWide\JsonApi\JsonApi\Data\Error;

class DataError extends Error
{
    public function __construct($pointer, $detail = '', $code = 400)
    {
        parent::__construct('data-error', 'Data error', $detail, 400);
        $this->setPointer($pointer);
    }
}
