<?php

namespace BBCWorldWide\JsonApi\JsonApi\Data\Error;

class RelationshipError extends Error
{
    public function __construct($relationshipName, $detail = '', $code = 400)
    {
        parent::__construct('relationship-error', 'Relationship error', $detail, $code);
        $this->setPointer('/data/relationships/' . $relationshipName);
    }
}
