<?php

namespace BBCWorldWide\JsonApi\JsonApi\Data\Error;

class ParameterError extends Error
{
    public function __construct($parameterName, $detail = '', $code = 400)
    {
        parent::__construct('parameter-error', 'Parameter error', $detail, $code);
        $this->setParameter($parameterName);
    }
}
