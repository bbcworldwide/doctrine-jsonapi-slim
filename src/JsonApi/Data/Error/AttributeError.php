<?php

namespace BBCWorldWide\JsonApi\JsonApi\Data\Error;

class AttributeError extends Error
{
    public function __construct($attributeName, $detail = '', $code = 400)
    {
        parent::__construct('attribute-error', 'Attribute error', $detail, 400);
        $this->setPointer('/data/attributes/' . $attributeName);
    }
}
