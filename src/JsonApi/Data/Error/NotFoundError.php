<?php

namespace BBCWorldWide\JsonApi\JsonApi\Data\Error;

class NotFoundError extends Error
{
    public function __construct($detail = '', $code = 404)
    {
        parent::__construct('not-found', 'Page not found', $detail, $code);
    }
}
