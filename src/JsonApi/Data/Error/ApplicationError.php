<?php

namespace BBCWorldWide\JsonApi\JsonApi\Data\Error;

class ApplicationError extends Error
{
    /**
     * @var \Throwable
     */
    private $throwable;

    /**
     * @var bool
     */
    private $showDetail;

    public function __construct(\Throwable $throwable, $showDetail = false)
    {
        $this->throwable  = $throwable;
        $this->showDetail = $showDetail;
        $detail           = ($this->throwable && $this->showDetail)
            ? $throwable->getMessage()
            : '';
        parent::__construct('application-error', 'Application error', $detail, 500);
    }

    public function jsonSerialize()
    {
        $json = parent::jsonSerialize();
        if ($this->throwable && $this->showDetail) {
            $json['meta']['error'] = [
                'type'    => get_class($this->throwable),
                'code'    => $this->throwable->getCode(),
                'message' => $this->throwable->getMessage(),
                'file'    => $this->throwable->getFile(),
                'line'    => $this->throwable->getLine(),
                'trace'   => explode("\n", $this->throwable->getTraceAsString()),
            ];
        }

        return $json;
    }
}
