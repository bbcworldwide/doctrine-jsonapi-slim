<?php

namespace BBCWorldWide\JsonApi\JsonApi\Data;

use BBCWorldWide\JsonApi\JsonApi\Data\Traits\Links;
use BBCWorldWide\JsonApi\JsonApi\Data\Traits\Meta;

/**
 * JSON API Relationship object
 */
class Relationship implements \JsonSerializable
{
    use Links;
    use Meta;

    /**
     * @var RelationshipDataInterface
     */
    protected $data;

    /**
     * @var bool Data has been set
     */
    protected $isDataSet = false;

    /**
     * @return DocumentDataInterface|null
     *
     * @codeCoverageIgnore
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param RelationshipDataInterface|null $data
     *
     * @return Relationship
     */
    public function setData(RelationshipDataInterface $data = null)
    {
        $this->data = $data;
        // Allow us to identify where data is explicitly set to null vs not set.
        $this->isDataSet = true;

        return $this;
    }

    public function jsonSerialize()
    {
        $output = array_filter([
            'meta'  => $this->meta,
            'links' => $this->links,
        ]);
        if ($this->isDataSet) {
            $output['data'] = $this->data;
        }

        return $output;
    }
}
