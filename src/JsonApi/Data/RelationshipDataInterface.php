<?php

namespace BBCWorldWide\JsonApi\JsonApi\Data;

/**
 * Relationship data interface
 *
 * Identifies objects which can provide the data element for a relationship
 */
interface RelationshipDataInterface extends DocumentDataInterface
{
}
