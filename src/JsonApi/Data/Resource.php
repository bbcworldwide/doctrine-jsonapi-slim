<?php

namespace BBCWorldWide\JsonApi\JsonApi\Data;

use BBCWorldWide\JsonApi\JsonApi\Data\Traits\IncludedResources;
use BBCWorldWide\JsonApi\JsonApi\Data\Traits\Meta;

/**
 * JSON API Resource object
 */
class Resource implements \JsonSerializable, DocumentDataInterface
{
    use IncludedResources;
    use Meta;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var mixed
     */
    protected $id;

    /**
     * @var array
     */
    protected $attributes = [];

    /**
     * @var array
     */
    protected $relationships = [];

    /**
     * Resource constructor.
     *
     * @param string $type
     * @param mixed  $id
     */
    public function __construct($type, $id)
    {
        $this->type = $type;
        $this->id   = $id;

        $this->includedResources = new ResourceCollection();
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return array
     *
     * @codeCoverageIgnore
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param string $name
     * @param mixed  $value
     *
     * @return $this
     */
    public function setAttribute($name, $value)
    {
        $this->attributes[$name] = $value;

        return $this;
    }

    /**
     * @return array
     *
     * @codeCoverageIgnore
     */
    public function getRelationships()
    {
        return $this->relationships;
    }

    /**
     * @param $name
     * @param $data
     *
     * @return $this
     */
    public function addRelationship($name, $data)
    {
        $this->relationships[$name] = $data;

        return $this;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return '/v1/' . $this->type . '/' . $this->id;
    }

    /**
     * @return ResourceIdentifier
     */
    public function getIdentifier()
    {
        return new ResourceIdentifier($this->type, $this->id);
    }

    public function jsonSerialize()
    {
        return array_filter([
            'meta'          => $this->meta,
            'type'          => $this->type,
            'id'            => $this->id,
            'attributes'    => $this->attributes,
            'relationships' => $this->relationships,
            'links'         => ['self' => $this->getPath()],
        ]);
    }
}
