<?php

namespace BBCWorldWide\JsonApi\JsonApi\Data;

/**
 * Resource Collection
 */
class ResourceCollection extends \SplObjectStorage implements \JsonSerializable, DocumentDataInterface
{
    public function getHash($resourceIdentifier)
    {
        return $resourceIdentifier->getType() . '/' . $resourceIdentifier->getId();
    }

    /**
     * @return ResourceCollection
     */
    public function getIncludedResources()
    {
        $collection = new ResourceCollection();
        foreach ($this as $resource) {
            foreach ($resource->getIncludedResources() as $includedResource) {
                $collection->attach($includedResource);
            }
        }

        return $collection;
    }

    public function jsonSerialize()
    {
        $resources = [];
        foreach ($this as $resource) {
            $resources[] = $resource;
        }

        return $resources;
    }
}
