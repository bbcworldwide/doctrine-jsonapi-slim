<?php

namespace BBCWorldWide\JsonApi\JsonApi\Data\Traits;

use BBCWorldWide\JsonApi\JsonApi\Data\ResourceCollection;
use BBCWorldWide\JsonApi\JsonApi\Data\Resource as Resrc;

/**
 * Provides includedResources property and related methods
 */
trait IncludedResources
{
    /**
     * @var ResourceCollection
     */
    protected $includedResources;

    /**
     * Get included resources
     *
     * @return ResourceCollection
     */
    public function getIncludedResources()
    {
        return $this->includedResources;
    }

    /**
     * Set included resources
     *
     * @param ResourceCollection $included
     *
     * @return $this
     */
    public function setIncludedResources(ResourceCollection $included)
    {
        $this->includedResources = $included;

        return $this;
    }

    /**
     * Add included resource
     *
     * @param Resrc $resource
     *
     * @return $this
     */
    public function addIncludedResource(Resrc $resource)
    {
        $this->includedResources->attach($resource);

        // Inherit included resources from resource
        foreach ($resource->getIncludedResources() as $inheritedResource) {
            $this->includedResources->attach($inheritedResource);
        }

        return $this;
    }
}
