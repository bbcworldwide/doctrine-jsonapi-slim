<?php

namespace BBCWorldWide\JsonApi\JsonApi\Data\Traits;

/**
 * Provides links property and related methods
 */
trait Links
{
    /**
     * @var array
     */
    protected $links = [];

    /**
     * Get links array
     *
     * @return array
     */
    public function getLinks()
    {
        return $this->links;
    }

    /**
     * Replace links with new array
     *
     * @param array $links URIs keys by identifier
     *
     * @return $this
     */
    public function setLinks(array $links)
    {
        $this->links = $links;

        return $this;
    }

    /**
     * Add link
     *
     * @param string $identifier Link identifier
     * @param string $uri        URI
     *
     * @return $this
     */
    public function addLink(string $identifier, string $uri = null)
    {
        $this->links[$identifier] = $uri;

        return $this;
    }
}
