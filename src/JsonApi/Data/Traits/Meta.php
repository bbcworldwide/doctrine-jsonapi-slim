<?php

namespace BBCWorldWide\JsonApi\JsonApi\Data\Traits;

/**
 * Provides meta property and related methods
 */
trait Meta
{
    /**
     * @var array
     */
    protected $meta = [];

    /**
     * Get meta data
     *
     * @return array
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * Set meta data
     *
     * @param array $meta
     *
     * @return $this
     */
    public function setMeta(array $meta)
    {
        $this->meta = $meta;

        return $this;
    }

    /**
     * Add meta data
     *
     * @param string $key   Meta key
     * @param mixed  $value Data
     *
     * @return $this
     */
    public function addMeta(string $key, $value)
    {
        $this->meta[$key] = $value;

        return $this;
    }
}
