<?php

namespace BBCWorldWide\JsonApi\JsonApi\Data;

/**
 * Resource Identifier collection
 */
class ResourceIdentifierCollection extends \SplObjectStorage implements \JsonSerializable, RelationshipDataInterface
{
    /**
     * @param ResourceIdentifier $resourceIdentifier
     */
    public function getHash($resourceIdentifier)
    {
        return $resourceIdentifier->getType() . '/' . $resourceIdentifier->getId();
    }

    public function jsonSerialize()
    {
        $identifiers = [];
        foreach ($this as $identifier) {
            $identifiers[] = $identifier;
        }

        return $identifiers;
    }
}
