<?php

namespace BBCWorldWide\JsonApi\JsonApi;

interface EntityInterface
{
    /**
     * @return mixed
     */
    public function getId();
}
