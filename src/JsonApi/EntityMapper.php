<?php

namespace BBCWorldWide\JsonApi\JsonApi;

use BBCWorldWide\JsonApi\JsonApi\Data\Error\MissingRelationError;
use BBCWorldWide\JsonApi\JsonApi\Exception\JsonApiException;
use BBCWorldWide\JsonApi\JsonApi\Schema\AttributeDefinition;
use BBCWorldWide\JsonApi\JsonApi\Schema\Schema;
use BBCWorldWide\JsonApi\Util\Methodiser;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Entity Mapper.
 *
 * Updates entities with data from Json:API objects.
 */
class EntityMapper implements EntityMapperInterface
{
    /**
     * @var Schema
     */
    protected $schema;

    /**
     * @var StoreInterface
     */
    protected $store;

    /**
     * Public constructor.
     *
     * @param Schema         $schema
     * @param StoreInterface $store
     */
    public function __construct(Schema $schema, StoreInterface $store)
    {
        $this->schema = $schema;
        $this->store  = $store;
    }

    /**
     * @inheritdoc
     */
    public function mapResourceObject(EntityInterface $entity, $resourceObject): void
    {
        $typeDefinition = $this->schema->getTypeForResource($entity);
        if (isset($resourceObject->data->id) && method_exists($entity, 'setId')) {
            $entity->setId($resourceObject->data->id);
        }

        if (isset($resourceObject->data->attributes)) {
            $attributes = $resourceObject->data->attributes;
            foreach ($typeDefinition->getAttributes() as $attribute => $definition) {
                if (property_exists($attributes, $attribute) && !$definition->isReadOnly()) {
                    $value = $attributes->$attribute;
                    if (!is_null($value) && $definition->getType() === AttributeDefinition::TYPE_DATETIME) {
                        $value = new \DateTime($value);
                    }

                    $method = Methodiser::setter($attribute);
                    $entity->$method($value);
                }
            }
        }

        if (isset($resourceObject->data->relationships)) {
            $relationshipObjects = $resourceObject->data->relationships;
            foreach ($typeDefinition->getRelationships() as $relationship => $definition) {
                if (isset($relationshipObjects->{$relationship}) &&
                    property_exists($relationshipObjects->{$relationship}, 'data') &&
                    $definition->isReadOnly() === false
                ) {
                    $this->mapRelationship($entity, $relationship, $relationshipObjects->{$relationship});
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function mapRelationship(
        EntityInterface $entity,
        string $relationshipName,
        \stdClass $relationshipObject
    ): void {
        $typeDefinition = $this->schema->getTypeForResource($entity);
        if ($typeDefinition->getRelationship($relationshipName)->isMultiple()) {
            $this->mapToManyRelationship($entity, $relationshipName, $relationshipObject);
        } else {
            $this->mapToOneRelationship($entity, $relationshipName, $relationshipObject);
        }
    }

    /**
     * @inheritdoc
     */
    public function addToRelationship(
        EntityInterface $entity,
        string $relationshipName,
        \stdClass $relationshipObject
    ): void {
        $typeDefinition = $this->schema->getTypeForResource($entity);
        if ($typeDefinition->getRelationship($relationshipName)->isMultiple()) {
            $this->mapToManyRelationship($entity, $relationshipName, $relationshipObject, false);
        } else {
            $this->mapToOneRelationship($entity, $relationshipName, $relationshipObject);
        }
    }

    /**
     * @inheritdoc
     */
    public function deleteFromRelationship(
        EntityInterface $entity,
        string $relationshipName,
        \stdClass $relationshipObject = null
    ): void {
        $typeDefinition = $this->schema->getTypeForResource($entity);
        if ($typeDefinition->getRelationship($relationshipName)->isMultiple()) {
            $this->removeToManyRelationship($entity, $relationshipName, $relationshipObject);
        } else {
            $this->mapToOneRelationship($entity, $relationshipName, null);
        }
    }

    /**
     * @param EntityInterface $entity
     * @param                 $relationshipName
     * @param                 $relationshipObject
     * @param bool            $remove
     */
    protected function mapToManyRelationship(
        EntityInterface $entity,
        $relationshipName,
        $relationshipObject,
        $remove = true
    ) {
        $getMethod = Methodiser::getter($relationshipName);

        /** @var ArrayCollection $existing */
        $existing = $entity->$getMethod();
        $new      = new ArrayCollection();

        $typeDefinition = $this->schema->getTypeForResource($entity);
        $singular       = $typeDefinition->getRelationship($relationshipName)->getSingular();
        $addMethod      = Methodiser::adder($singular);
        $removeMethod   = Methodiser::remover($singular);

        foreach ($relationshipObject->data as $relation) {
            $ref = $this->requireRelation($relation->type, $relation->id);
            $new->add($ref);
            if (isset($relation->meta->position)) {
                $entity->$addMethod($ref, $relation->meta->position);
            } elseif (!$existing->contains($ref)) {
                $entity->$addMethod($ref);
            }
        }

        if ($remove) {
            foreach ($existing as $ref) {
                if (!$new->contains($ref)) {
                    $entity->$removeMethod($ref);
                }
            }
        }
    }

    /**
     * @param EntityInterface $entity
     * @param string          $relationshipName
     * @param null|\stdClass  $relationshipObject
     */
    protected function mapToOneRelationship(
        EntityInterface $entity,
        string $relationshipName,
        ?\stdClass $relationshipObject
    ) {
        $method = Methodiser::setter($relationshipName);
        if (!empty($relationshipObject->data->id)) {
            $type = $relationshipObject->data->type;
            $id   = $relationshipObject->data->id;
            $ref  = $this->requireRelation($type, $id);
            $entity->$method($ref);
        } else {
            $entity->$method(null);
        }
    }

    /**
     * @param EntityInterface $entity
     * @param string          $relationshipName
     * @param \stdClass       $relationshipObject
     */
    protected function removeToManyRelationship(
        EntityInterface $entity,
        string $relationshipName,
        \stdClass $relationshipObject
    ) {
        $getMethod = Methodiser::getter($relationshipName);
        /** @var ArrayCollection $existing */
        $existing       = $entity->$getMethod();
        $typeDefinition = $this->schema->getTypeForResource($entity);
        $singular       = $typeDefinition->getRelationship($relationshipName)->getSingular();
        $removeMethod   = Methodiser::remover($singular);

        foreach ($relationshipObject->data as $relation) {
            $ref = $this->requireRelation($relation->type, $relation->id);
            if ($existing->contains($ref)) {
                $entity->$removeMethod($ref);
            }
        }
    }

    /**
     * @param string $type
     * @param mixed  $id
     *
     * @return EntityInterface
     */
    protected function requireRelation(string $type, $id): EntityInterface
    {
        $ref = $this->store->findOne($type, $id);
        if (!$ref) {
            throw new JsonApiException(
                new MissingRelationError("Referenced resource of type '$type' with id '$id' does not exist", 400)
            );
        }

        return $ref;
    }
}
