<?php

namespace BBCWorldWide\JsonApi\JsonApi;

use BBCWorldWide\JsonApi\JsonApi\Data\Error\ParameterError;
use BBCWorldWide\JsonApi\JsonApi\Exception\JsonApiException;
use BBCWorldWide\JsonApi\JsonApi\Parameters\ParameterSet;
use BBCWorldWide\JsonApi\JsonApi\Parameters\SortParameter;
use BBCWorldWide\JsonApi\JsonApi\Schema\Schema;
use BBCWorldWide\JsonApi\JsonApi\Schema\TypeDefinition;
use BBCWorldWide\JsonApi\Pager\PagedQuery;
use BBCWorldWide\JsonApi\Util\Inflector;
use BBCWorldWide\JsonApi\Util\Methodiser;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\CountWalker;

/**
 * Entity store
 *
 * Data access layer which can query using Json:API types and parameters.
 */
class Store implements StoreInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var Schema
     */
    protected $schema;

    /**
     * Public constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param Schema                 $schema
     */
    public function __construct(EntityManagerInterface $entityManager, Schema $schema)
    {
        $this->entityManager = $entityManager;
        $this->schema        = $schema;
    }

    /**
     * @inheritdoc
     */
    public function findOne($type, $id)
    {
        return $this->entityManager->find($this->schema->getType($type)->getEntityClass(), $id);
    }

    /**
     * @inheritdoc
     */
    public function find($type, ParameterSet $parameters)
    {
        $typeDef = $this->schema->getType($type);
        $builder = $this->entityManager
            ->createQueryBuilder()
            ->select('base')
            ->from($typeDef->getEntityClass(), 'base');

        $this->applyParams($builder, $parameters, $type);

        $cacheResults = $typeDef->getCacheResults();
        $query        = $this->getPagedQuery($builder, $parameters, $cacheResults);

        return $query;
    }

    /**
     * @inheritdoc
     */
    public function findByRelation($type, $id, $relationName, ParameterSet $parameters)
    {
        $builder = $this->getRelationQueryBuilder($type, $id, $relationName, $parameters);

        return $this->getPagedQuery($builder, $parameters);
    }

    /**
     * @inheritdoc
     */
    public function findOneByRelation($type, $id, $relationName)
    {
        $builder = $this->getRelationQueryBuilder($type, $id, $relationName);
        $results = $builder->getQuery()->execute();

        return $results[0] ?? null;
    }

    /**
     * @inheritdoc
     */
    public function save(EntityInterface $entity)
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }

    /**
     * @inheritdoc
     */
    public function delete(EntityInterface $entity)
    {
        $this->entityManager->remove($entity);
        $this->entityManager->flush();
    }

    /**
     * Get a query builder configured to select via a named relationship.
     *
     * @param string       $type
     * @param mixed        $id
     * @param string       $relationName
     * @param ParameterSet $parameters
     *
     * @return QueryBuilder
     */
    public function getRelationQueryBuilder(
        $type,
        $id,
        $relationName,
        ParameterSet $parameters = null
    ) {
        $definition  = $this->schema->getType($type)->getRelationship($relationName);
        $baseType    = $this->schema->getType($definition->getTypes()[0])->getBaseType();
        $entityClass = $this->schema->getType($baseType)->getEntityClass();
        $ownerClass  = $this->schema->getType($type)->getEntityClass();

        // Relation-name must be camelCased when on entities or otherwise in the Doctrine world
        $parsedRelationName = Inflector::camelize($relationName);

        $repo = $this->entityManager->getRepository($ownerClass);
        $getBuilderMethod = Methodiser::getter($parsedRelationName) . 'QueryBuilder';

        if (method_exists($repo, $getBuilderMethod)) {
            $builder = $repo->$getBuilderMethod($id);
        } else {
            $baseClass = $this->schema->getType($type)->getEntityClass();
            /** @var ClassMetadataInfo $meta */
            $meta    = $this->entityManager->getClassMetadata($baseClass);
            $mapping = $meta->getAssociationMapping($parsedRelationName);

            $pkName = $this->getPrimaryKeyFieldName($type);

            $builder = $this->entityManager
                ->createQueryBuilder()
                ->select('base')
                ->from($entityClass, 'base');

            // Check for a mapping or inversion.  Some ManyToMany won't have this attribute.
            if ($inverseRelationName = $mapping['isOwningSide'] ? $mapping['inversedBy'] : $mapping['mappedBy']) {
                $builder
                    ->join('base.' . $inverseRelationName, 'relation')
                    ->andWhere(sprintf('relation.%s = :id', $pkName))
                    ->setParameter('id', $id);
            } else {
                // If there is not inversion, do an expensive call to get the object
                // and extract the relation ids.
                $ownerBuilder = $this->entityManager
                    ->createQueryBuilder()
                    ->select(['base', 'relation'])
                    ->from($ownerClass, 'base')
                    ->where(sprintf('base.%s = :id', $pkName))
                    ->setParameter('id', $id)
                    ->join('base.' . $parsedRelationName, 'relation');

                $result = $ownerBuilder
                    ->getQuery()
                    ->getArrayResult();

                // Extract IDs from the relation.
                $ids = array_map(function ($entity) use ($relationName) {
                    return array_column(
                        $entity[$relationName],
                        $this->getPrimaryKeyFieldName($this->schema->getTypeForResource($entity))
                    );
                }, $result);

                $builder
                    ->andWhere(sprintf('base.%s IN (:ids)', $pkName))
                    ->setParameter('ids', isset($ids[0]) ? $ids[0] : []);
            }
        }

        if ($parameters) {
            $this->applyParams($builder, $parameters, $baseType);
        }

        return $builder;
    }

    /**
     * Apply Json:API parameters to a query builder.
     *
     * @param QueryBuilder $builder
     * @param ParameterSet $parameters
     * @param string       $type
     */
    protected function applyParams(QueryBuilder $builder, ParameterSet $parameters, $type)
    {
        $sorts = $parameters->getSorts();
        if (!empty($sorts)) {
            /** @var SortParameter $sort */
            foreach ($sorts as $sort) {
                $builder->addOrderBy('base.' . $sort->getField(), $sort->isAscending() ? 'ASC' : 'DESC');
            }
        } else {
            // Try to get default sort for type.
            $typeDef = $this->schema->getType($type);
            if ($defaultSort = $typeDef->getDefaultSort()) {
                // Parse JSON API sort string:
                foreach (explode(',', $defaultSort) as $param) {
                    $isAscending = ($param[0] !== '-');
                    $sortField   = ltrim($param, '+-');
                    $builder->addOrderBy('base.' . $sortField, $isAscending ? 'ASC' : 'DESC');
                }
            } else {
                // Fall back to sort on PK.
                $builder->addOrderBy(sprintf('base.%s', $this->getPrimaryKeyFieldName($type)));
            }
        }

        if ($filters = $parameters->getFilters()) {
            foreach ($filters as $property => $value) {
                switch ($property) {
                    case 'types':
                        $types = explode(',', $value);
                        $expr  = $builder->expr()->orX();
                        foreach ($types as $schemaType) {
                            if ($def = $this->schema->getType($schemaType)) {
                                $expr->add('base INSTANCE OF ' . $def->getEntityClass());
                            }
                        }
                        $builder->andWhere($expr);
                        break;

                    default:
                        $builder
                            ->andWhere("base.$property = :$property")
                            ->setParameter($property, $value);
                }
            }
        }
    }

    /**
     * Returns the field name for the primary key - for as long as it's not a composite primary key. In that case,
     * assumes ID.
     *
     * @param string $type
     *
     * @return string
     */
    protected function getPrimaryKeyFieldName(string $type): string
    {
        $name       = 'id';
        $definition = $this->schema->getType($type);

        try {
            if ($definition instanceof TypeDefinition) {
                $metadata = $this->entityManager->getClassMetadata($definition->getEntityClass());
                if ($metadata instanceof ClassMetadata) {
                    $name = $metadata->getSingleIdentifierFieldName() ?? $name;
                }
            }
        } catch (\Doctrine\ORM\Mapping\MappingException $ex) {
            // Do nothing, return default id
        }

        return $name;
    }

    /**
     * Get a paged query from a QueryBuilder object and Json:API parameters.
     *
     * @param QueryBuilder $builder
     * @param ParameterSet $parameters
     * @param bool         $cacheResults
     *
     * @return PagedQuery
     *
     * @throws JsonApiException
     * @codeCoverageIgnore Query object cannot be mocked
     */
    protected function getPagedQuery(QueryBuilder $builder, ParameterSet $parameters, $cacheResults = false)
    {
        $query = $builder->getQuery();
        $query->useResultCache($cacheResults);

        $filters = $parameters->getFilters();

        if (isset($filters['search']) && $this->isId($filters['search'])) {
            $query->setHint(CountWalker::HINT_DISTINCT, true);
        }

        try {
            return new PagedQuery($query, $parameters->getPageNumber(), $parameters->getPageSize());
        } catch (\InvalidArgumentException $ex) {
            throw new JsonApiException(new ParameterError('page', $ex->getMessage()));
        }
    }

    /**
     * Check if given value matches ID format.
     *
     * @param $value
     *
     * @return int
     */
    protected function isId($value)
    {
        return
            preg_match('/^[qwrtypsdfghjklzxcvbnm0-9]{8}(_sd|_hd)?$/', $value) ||
            preg_match('/^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}?$/', $value);
    }
}
