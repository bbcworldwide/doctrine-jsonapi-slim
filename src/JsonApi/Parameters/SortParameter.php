<?php

namespace BBCWorldWide\JsonApi\JsonApi\Parameters;

/**
 * Sort Parameter
 */
class SortParameter
{
    /**
     * @var string
     */
    protected $field;

    /**
     * @var bool
     */
    protected $isDescending = false;

    /**
     * SortParameter constructor.
     *
     * @param string $field        Name of field
     * @param bool   $isDescending Reverse sort order
     */
    public function __construct(string $field, bool $isDescending = false)
    {
        $this->field        = $field;
        $this->isDescending = $isDescending;
    }

    /**
     * Get field name
     *
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Is descending order
     *
     * @return boolean
     */
    public function isDescending()
    {
        return $this->isDescending;
    }

    /**
     * Is ascending order
     *
     * @return boolean
     */
    public function isAscending()
    {
        return !$this->isDescending;
    }

    /**
     * Create from string
     *
     * @param $str
     *
     * @return SortParameter
     */
    public static function fromString($str)
    {
        $isDescending = $str[0] === '-';
        $field        = ltrim($str, '-');

        return new self($field, $isDescending);
    }

    public function __toString()
    {
        return ($this->isDescending ? '-' : '') . $this->field;
    }
}
