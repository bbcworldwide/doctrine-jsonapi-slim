<?php

namespace BBCWorldWide\JsonApi\JsonApi\Parameters;

use BBCWorldWide\JsonApi\JsonApi\Schema\Schema;
use BBCWorldWide\JsonApi\Pager\PagedQuery;
use BBCWorldWide\JsonApi\Util\QueryStringEncoder;

/**
 * Parameter Set
 */
class ParameterSet
{
    /**
     * @var Schema
     */
    protected $schema;

    /**
     * @var string[]
     */
    protected $include = [];

    /**
     * @var array
     */
    protected $fields = [];

    /**
     * @var array
     */
    protected $filters = [];

    /**
     * @var SortParameter[]
     */
    protected $sorts = [];

    /**
     * @var int|null
     */
    protected $pageSize;

    /**
     * @var int|null
     */
    protected $pageNumber;

    public function __construct(Schema $schema)
    {
        $this->schema = $schema;
    }

    /**
     * @return \string[]
     */
    public function getIncludes()
    {
        return $this->include;
    }

    /**
     * @param \string[] $include
     *
     * @return ParameterSet
     */
    public function setIncludes($include)
    {
        $this->include = $include;

        return $this;
    }

    /**
     * @param \string[] $include
     *
     * @return ParameterSet
     */
    public function withIncludes($include)
    {
        $instance = clone $this;

        return $instance->setIncludes($include);
    }

    /**
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @param array $fields
     *
     * @return ParameterSet
     */
    public function setFields($fields)
    {
        $this->fields = $fields;

        return $this;
    }

    /**
     * @param array $fields
     *
     * @return ParameterSet
     */
    public function withFields($fields)
    {
        $instance = clone $this;

        return $instance->setFields($fields);
    }

    /**
     * @param string $type
     *
     * @return array
     */
    public function getFieldsForType(string $type, $inherit = true)
    {
        $fields = $this->fields[$type] ?? [];
        if ($inherit) {
            $typeDefinition = $this->schema->getType($type);
            $parentType     = $typeDefinition->getSubtypeOf();
            $fields         = array_merge($fields, $this->fields[$parentType] ?? []);
        }

        return $fields;
    }

    /**
     * @param string $type
     * @param array  $fields
     *
     * @return $this
     */
    public function setFieldsForType(string $type, array $fields)
    {
        $this->fields[$type] = $fields;

        return $this;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * @param $name
     * @param $value
     *
     * @return $this
     */
    public function addFilter($name, $value)
    {
        $this->filters[$name] = $value;

        return $this;
    }

    /**
     * @param array $filters
     *
     * @return ParameterSet
     */
    public function setFilters($filters)
    {
        $this->filters = $filters;

        return $this;
    }

    /**
     * @param array $filters
     *
     * @return ParameterSet
     */
    public function withFilters($filters)
    {
        $instance = clone $this;

        return $instance->setFilters($filters);
    }

    /**
     * @return SortParameter[]
     */
    public function getSorts()
    {
        return $this->sorts;
    }

    /**
     * @param SortParameter $sort
     *
     * @return ParameterSet
     */
    public function addSort(SortParameter $sort)
    {
        $this->sorts[] = $sort;

        return $this;
    }

    /**
     * @param SortParameter[] $sorts
     *
     * @return ParameterSet
     */
    public function setSorts($sorts)
    {
        $this->sorts = $sorts;

        return $this;
    }

    /**
     * @param SortParameter[] $sorts
     *
     * @return ParameterSet
     */
    public function withSorts($sorts)
    {
        $instance = clone $this;

        return $instance->setSorts($sorts);
    }

    /**
     * @return int|null
     */
    public function getPageSize()
    {
        return $this->pageSize ? $this->pageSize : PagedQuery::DEFAULT_PAGE_SIZE;
    }

    /**
     * @param int|null $pageSize
     *
     * @return ParameterSet
     */
    public function setPageSize($pageSize)
    {
        $this->pageSize = $pageSize;

        return $this;
    }

    /**
     * @param int|null $pageSize
     *
     * @return ParameterSet
     */
    public function withPageSize($pageSize)
    {
        $instance = clone $this;

        return $instance->setPageSize($pageSize);
    }

    /**
     * @return int|null
     */
    public function getPageNumber()
    {
        return $this->pageNumber ? $this->pageNumber : 1;
    }

    /**
     * @param int|null $pageNumber
     *
     * @return ParameterSet
     */
    public function setPageNumber($pageNumber)
    {
        $this->pageNumber = $pageNumber;

        return $this;
    }

    /**
     * @param int|null $pageNumber
     *
     * @return ParameterSet
     */
    public function withPageNumber($pageNumber)
    {
        $instance = clone $this;

        return $instance->setPageNumber($pageNumber);
    }

    /**
     * @return array
     */
    public function toQueryArray()
    {
        $query = [];
        if (!empty($this->include)) {
            $query['include'] = implode(',', $this->include);
        }
        if (!empty($this->fields)) {
            foreach ($this->fields as $type => $fields) {
                $query['fields'][$type] = implode(',', $fields);
            }
        }
        if (!empty($this->filters)) {
            $query['filters'] = implode(',', $this->filters);
        }
        if (!empty($this->sorts)) {
            $query['sort'] = implode(',', $this->sorts);
        }
        if (!empty($this->pageSize)) {
            $query['page']['size'] = $this->pageSize;
        }
        if (!empty($this->pageNumber)) {
            $query['page']['number'] = $this->pageNumber;
        }

        return $query;
    }

    public function __toString()
    {
        $query   = $this->toQueryArray();
        $encoder = new QueryStringEncoder();

        return $encoder->encode($query);
    }
}
