<?php

namespace BBCWorldWide\JsonApi\JsonApi\Parameters;

use BBCWorldWide\JsonApi\JsonApi\Data\Error\ParameterError;
use BBCWorldWide\JsonApi\JsonApi\Exception\JsonApiException;
use BBCWorldWide\JsonApi\JsonApi\Schema\Schema;
use BBCWorldWide\JsonApi\Pager\PagedQuery;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Parameters Parser
 */
class ParametersParser
{
    /**
     * Maximum nesting of included relationships.
     */
    const MAX_NESTING_DEPTH = 5;

    /**
     * @var Schema
     */
    protected $schema;

    /**
     * Factory constructor.
     *
     * @param Schema $schema
     */
    public function __construct(Schema $schema)
    {
        $this->schema = $schema;
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @throws JsonApiException
     */
    public function getInstance(ServerRequestInterface $request)
    {
        $params = new ParameterSet($this->schema);

        $query = $request->getQueryParams();

        // Establish base type from path
        $path      = ltrim($request->getUri()->getPath(), '/');
        $pathParts = explode('/', $path);

        if (!isset($pathParts[1]) || !$this->schema->hasType($pathParts[1])) {
            return $params;
        }

        $typeContext = $pathParts[1];
        $typeDef     = $this->schema->getType($typeContext);

        foreach ($query as $key => $value) {
            switch ($key) {
                case 'include':
                    $includes = explode(',', $value);
                    foreach ($includes as $includeType) {
                        if (!$this->isIncludeAllowed($typeContext, $includeType)) {
                            throw new JsonApiException(new ParameterError($key, "Invalid include '$includeType'"));
                        }
                    }
                    $params->setIncludes($includes);
                    break;

                case 'fields':
                    if (!is_array($value)) {
                        throw new JsonApiException(
                            new ParameterError($key, "Fields must be formatted as fields[type]=names")
                        );
                    }
                    foreach ($value as $type => $fields) {
                        try {
                            $def = $this->schema->getType($type);
                        } catch (\InvalidArgumentException $e) {
                            throw new JsonApiException(
                                new ParameterError($key, "Invalid type '$type' specified in fields")
                            );
                        }
                        $allowedFields = $def->getFields();
                        $fields        = explode(',', $fields);
                        foreach ($fields as $field) {
                            if (!in_array($field, $allowedFields)) {
                                throw new JsonApiException(
                                    new ParameterError($key, "Field '$field' is not allowed for type '$type'")
                                );
                            }
                        }
                        $params->setFieldsForType($type, $fields);
                    }
                    break;

                case 'filter':
                    if (!is_array($value)) {
                        throw new JsonApiException(
                            new ParameterError($key, "Filters should be formatted as filter[name]=value")
                        );
                    }
                    $allowedFilters = $this->getAllowedFilters($typeContext);
                    foreach ($value as $name => $val) {
                        if (!in_array($name, $allowedFilters)) {
                            throw new JsonApiException(new ParameterError($key, "Invalid filter '$name'"));
                        }
                        $params->addFilter($name, $val);
                    }
                    break;

                case 'page':
                    if (!is_array($value)) {
                        throw new JsonApiException(
                            new ParameterError($key, "Page options should be formatted as page[size|number]=value")
                        );
                    }
                    if (isset($value['number'])) {
                        if (!ctype_digit($value['number'])) {
                            throw new JsonApiException(new ParameterError($key, "Page number must be an integer"));
                        }
                        $params->setPageNumber((int) $value['number']);
                    }
                    if (isset($value['size'])) {
                        if (!ctype_digit($value['size'])) {
                            throw new JsonApiException(new ParameterError($key, "Page size must be an integer"));
                        }
                        if ($value['size'] > PagedQuery::MAX_PAGE_SIZE) {
                            throw new JsonApiException(
                                new ParameterError($key, "Page size can't exceed " . PagedQuery::MAX_PAGE_SIZE)
                            );
                        }
                        $params->setPageSize((int) $value['size']);
                    }
                    break;

                case 'sort':
                    $sorts = explode(',', $value);
                    foreach ($sorts as $sortString) {
                        $isDesc = $sortString[0] === '-';
                        $field  = ltrim($sortString, '-');
                        try {
                            $attrDef = $typeDef->getAttribute($field);
                        } catch (\InvalidArgumentException $e) {
                            throw new JsonApiException(
                                new ParameterError($key, "Invalid field '$field' specified in sorts")
                            );
                        }
                        if (!$attrDef->isSortable()) {
                            throw new JsonApiException(new ParameterError($key, "Parameter '$key' is not sortable"));
                        }
                        $params->addSort(new SortParameter($field, $isDesc));
                    }
                    break;

                default:
                    throw new JsonApiException(new ParameterError($key, "Invalid parameter '$key'"));
            }
        }

        return $params;
    }

    /**
     * Is included relationship allowed for this type?
     *
     * @param string $type    Type of base entity i.e. the starting point in hierarchy for relationships
     * @param string $include Include string
     *
     * @return bool                Is allowed
     */
    protected function isIncludeAllowed($type, $include)
    {
        $typeDef      = $this->schema->getType($type);
        $includeParts = explode('.', $include);

        if (count($includeParts) > self::MAX_NESTING_DEPTH) {
            return false;
        }

        // White-list relationships from base type
        $allowedRels = $typeDef->getRelationships();

        foreach ($includeParts as $relationshipName) {
            if (!isset($allowedRels[$relationshipName]) || !$allowedRels[$relationshipName]->isIncludable()) {
                return false;
            }
            $relationship = $allowedRels[$relationshipName];

            // Build new white-list for next level in hierarchy.
            // Need to merge schema for each possible related type.
            $allowedRels = [];
            foreach ($relationship->getTypes() as $type) {
                $allowedRels += $this->schema->getType($type)->getRelationships();
            }
        }

        return true;
    }

    /**
     * @param string $type
     *
     * @return array
     */
    protected function getAllowedFilters($type)
    {
        $typeDef        = $this->schema->getType($type);
        $allowedFilters = [];
        foreach ($typeDef->getFilterableAttributes() as $filter) {
            $allowedFilters[] = $filter;
        }
        foreach ($typeDef->getCustomFilters() as $filter) {
            $allowedFilters[] = $filter;
        }

        return $allowedFilters;
    }
}
