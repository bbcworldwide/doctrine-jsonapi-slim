<?php

namespace BBCWorldWide\JsonApi\Exceptions;

/**
 * Thrown when a property is required to be present.
 *
 * @author BBC Worldwide
 * @codeCoverageIgnore
 */
class RequiredPropertyException extends \Exception
{
    public function __construct($propertyName)
    {
        parent::__construct($propertyName);
    }
}
