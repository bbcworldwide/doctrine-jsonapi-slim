<?php

namespace BBCWorldWide\JsonApi\Action\Resource;

use BBCWorldWide\JsonApi\Action\AbstractJsonApiAction;
use BBCWorldWide\JsonApi\JsonApi\Parameters\ParameterSet;
use BBCWorldWide\JsonApi\Util\Inflector;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class IndexAction extends AbstractJsonApiAction
{
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $type = $args['type'];
        $typeDefinition = $this->requireTypeDefinition($type);

        $this->nameTransaction('Index ' . Inflector::pluralize($typeDefinition->getAlias()));

        /** @var ParameterSet $parameters */
        $parameters = $request->getAttribute('parameters');
        $query      = $this->store->find($type, $parameters);
        $path       = $request->getUri()->getPath();
        $document   = $this->pagedDataEncoder->encodeResourcesDocument($query, $parameters, $path);
        $document->addMeta('build', $this->buildInfo);

        return $response->write($document);
    }
}
