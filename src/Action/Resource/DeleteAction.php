<?php

namespace BBCWorldWide\JsonApi\Action\Resource;

use BBCWorldWide\JsonApi\Action\AbstractJsonApiAction;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class DeleteAction extends AbstractJsonApiAction
{
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $id             = $args['id'];
        $type           = $args['type'];
        $typeDefinition = $this->requireTypeDefinition($type);

        $this->nameTransaction('Delete ' . $typeDefinition->getAlias());

        $entity = $this->requireEntity($type, $id);
        $this->store->delete($entity);

        $this->logger->info($this->transactionName, [
            'type'     => 'resource-delete',
            'resource' => ['type' => $type, 'id' => $entity->getId()],
            'duration' => $this->getDuration(),
            'success'  => true,
        ]);

        return $response->withStatus(204);
    }
}
