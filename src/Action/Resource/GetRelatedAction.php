<?php

namespace BBCWorldWide\JsonApi\Action\Resource;

use BBCWorldWide\JsonApi\Action\AbstractJsonApiAction;
use BBCWorldWide\JsonApi\JsonApi\Parameters\ParameterSet;
use BBCWorldWide\JsonApi\Util\Inflector;
use BBCWorldWide\JsonApi\Util\Methodiser;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class GetRelatedAction extends AbstractJsonApiAction
{
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $id               = $args['id'];
        $type             = $args['type'];
        $typeDefinition   = $this->requireTypeDefinition($type);
        $relationshipName = Inflector::camelize($args['relationship']);
        /** @var ParameterSet $parameters */
        $parameters = $request->getAttribute('parameters');

        $entity                 = $this->requireEntity($type, $id);
        $entityType             = $this->schema->getTypeByClassName(get_class($entity))->getName();
        $relationshipDefinition = $this->requireRelationshipDefinition($entityType, $relationshipName);

        $this->nameTransaction(sprintf(
            'Get related %s for %s',
            $relationshipDefinition->getAlias(),
            $typeDefinition->getAlias()
        ));

        if ($relationshipDefinition->isMultiple()) {
            $results  = $this->store->findByRelation($entityType, $id, $relationshipName, $parameters);
            $path     = $request->getUri()->getPath();
            $document = $this->pagedDataEncoder->encodeResourcesDocument($results, $parameters, $path);
        } else {
            // Prefer getter method on to-one relationships.
            $method = Methodiser::getter($relationshipName);
            if (method_exists($entity, $method)) {
                $result = $entity->$method();
            } else {
                $result = $this->store->findOneByRelation($entityType, $id, $relationshipName);
            }
            $document = $this->entityEncoder->encodeResourceDocument($result, $parameters);
        }

        // Add links
        $entityPath       = '/' . $type . '/' . $id;
        $relationshipPath = '/' . Inflector::hyphenate($relationshipName);
        $document->addLink('self', $entityPath . $relationshipPath)
                 ->addLink('related', $entityPath . '/relationships' . $relationshipPath)
                 ->addMeta('build', $this->buildInfo);

        return $response->write($document);
    }
}
