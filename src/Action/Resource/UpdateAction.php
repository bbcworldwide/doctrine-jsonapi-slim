<?php

namespace BBCWorldWide\JsonApi\Action\Resource;

use BBCWorldWide\JsonApi\Action\AbstractJsonApiAction;
use BBCWorldWide\JsonApi\JsonApi\Data\Error\Error;
use BBCWorldWide\JsonApi\JsonApi\Exception\JsonApiException;
use BBCWorldWide\JsonApi\JsonApi\Parameters\ParameterSet;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class UpdateAction extends AbstractJsonApiAction
{
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $id             = $args['id'];
        $type           = $args['type'];
        $typeDefinition = $this->requireTypeDefinition($type);
        /** @var ParameterSet $parameters */
        $parameters = $request->getAttribute('parameters');

        $this->nameTransaction('Update ' . $typeDefinition->getAlias());

        $entity     = $this->requireEntity($type, $id);
        $entityType = $this->schema->getTypeByClassName(get_class($entity))->getName();

        $document = json_decode($request->getBody()->getContents());

        if (!$this->documentValidator->validateResourceObject($document, [$entityType], $request->isPatch())) {
            throw new JsonApiException($this->documentValidator->getErrors());
        }

        // Check the document contains the correct entity.
        if ($document->data->id != $id) {
            throw new JsonApiException(new Error(
                'mismatched-resource',
                'Mismatched Resource',
                'Resource in document does not match path'
            ));
        }

        $this->mapper->mapResourceObject($entity, $document);
        $this->save($entity);

        $this->logger->info($this->transactionName, [
            'type'     => 'resource-update',
            'resource' => ['type' => $type, 'id' => $entity->getId()],
            'payload'  => $document,
            'duration' => $this->getDuration(),
            'success'  => true,
        ]);

        if ($request->isPut()) {
            return $response->withStatus(204);
        } else {
            $document = $this->entityEncoder->encodeResourceDocument($entity, $parameters);
            $document->addMeta('build', $this->buildInfo);

            return $response->write($document);
        }
    }
}
