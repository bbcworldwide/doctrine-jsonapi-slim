<?php

namespace BBCWorldWide\JsonApi\Action\Resource;

use BBCWorldWide\JsonApi\Action\AbstractJsonApiAction;
use BBCWorldWide\JsonApi\JsonApi\Data\Error\ConflictError;
use BBCWorldWide\JsonApi\JsonApi\EntityInterface;
use BBCWorldWide\JsonApi\JsonApi\Exception\JsonApiException;
use BBCWorldWide\JsonApi\JsonApi\Parameters\ParameterSet;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class CreateAction extends AbstractJsonApiAction
{
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $type           = $args['type'];
        $typeDefinition = $this->requireTypeDefinition($type);
        /** @var ParameterSet $parameters */
        $parameters = $request->getAttribute('parameters');

        $this->nameTransaction('Create ' . $typeDefinition->getAlias());

        $document = json_decode($request->getBody()->getContents());

        $typeDefinition->getBaseType();
        if (!$this->documentValidator->validateResourceObject($document, $typeDefinition->getConcreteTypes())) {
            throw new JsonApiException($this->documentValidator->getErrors());
        }

        $entityClass = $this->schema->getType($document->data->type)->getEntityClass();
        /** @var EntityInterface $entity */
        $entity = new $entityClass();
        $this->mapper->mapResourceObject($entity, $document);

        try {
            $this->save($entity);
        } catch (UniqueConstraintViolationException $e) {
            $alias = $typeDefinition->getAlias();
            $id    = $document->data->id;
            throw new JsonApiException(
                new ConflictError("$alias '$id' already exists on this server")
            );
        }

        $this->logger->info($this->transactionName, [
            'type'     => 'resource-create',
            'resource' => ['type' => $type, 'id' => $entity->getId()],
            'payload'  => $document,
            'duration' => $this->getDuration(),
            'success'  => true,
        ]);

        if (!empty($document->data->id)) {
            return $response->withStatus(204);
        } else {
            $document = $this->entityEncoder->encodeResourceDocument($entity, $parameters);
            $document->addMeta('build', $this->buildInfo);

            return $response
                ->withStatus(201)
                ->withHeader('Location', '/' . $type . '/' . $entity->getId())
                ->write($document);
        }
    }
}
