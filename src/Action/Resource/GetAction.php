<?php

namespace BBCWorldWide\JsonApi\Action\Resource;

use BBCWorldWide\JsonApi\Action\AbstractJsonApiAction;
use BBCWorldWide\JsonApi\JsonApi\Parameters\ParameterSet;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class GetAction extends AbstractJsonApiAction
{
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $id             = $args['id'];
        $type           = $args['type'];
        $typeDefinition = $this->requireTypeDefinition($type);
        /** @var ParameterSet $parameters */
        $parameters = $request->getAttribute('parameters');

        $this->nameTransaction('Get ' . $typeDefinition->getAlias());

        $entity   = $this->requireEntity($type, $id);
        $document = $this->entityEncoder->encodeResourceDocument($entity, $parameters);
        $document->addMeta('build', $this->buildInfo);

        return $response->write($document);
    }
}
