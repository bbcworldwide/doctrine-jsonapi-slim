<?php

namespace BBCWorldWide\JsonApi\Action\Relationship;

use BBCWorldWide\JsonApi\Action\AbstractJsonApiAction;
use BBCWorldWide\JsonApi\JsonApi\Data\Error\ParameterError;
use BBCWorldWide\JsonApi\JsonApi\Exception\JsonApiException;
use BBCWorldWide\JsonApi\Util\Inflector;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class DeleteAction extends AbstractJsonApiAction
{
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $id                     = $args['id'];
        $type                   = $args['type'];
        $typeDefinition         = $this->requireTypeDefinition($type);
        $entity                 = $this->requireEntity($type, $id);
        $entityType             = $this->schema->getTypeByClassName(get_class($entity))->getName();
        $relationshipName       = Inflector::camelize($args['relationship']);
        $relationshipDefinition = $this->requireRelationshipDefinition($entityType, $relationshipName);

        $this->nameTransaction(sprintf(
            'Delete %s relationship for %s',
            $relationshipDefinition->getAlias(),
            $typeDefinition->getAlias()
        ));

        $this->ensureWritable($relationshipDefinition);

        $document = json_decode($request->getBody()->getContents());
        if (empty($document) === true) {
            throw new JsonApiException(new ParameterError(
                'payload',
                'Must contain resource identifier of resource (or list of) to delete from relationship'
            ));
        }

        if (!$this->documentValidator->validateRelationshipObject($document, $relationshipDefinition)) {
            throw new JsonApiException($this->documentValidator->getErrors());
        }

        $this->mapper->deleteFromRelationship($entity, $relationshipName, $document);
        $this->store->save($entity);

        $this->logger->info($this->transactionName, [
            'type'         => 'relationship-delete',
            'resource'     => ['type' => $type, 'id' => $id],
            'relationship' => $relationshipName,
            'duration'     => $this->getDuration(),
            'success'      => true,
        ]);

        return $response->withStatus(204);
    }
}
