<?php

namespace BBCWorldWide\JsonApi\Action;

use BBCWorldWide\JsonApi\JsonApi\Schema\Schema;
use BBCWorldWide\JsonApi\JsonApi\Swagger;
use Interop\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Stream;

class SwaggerAction
{
    /**
     * @var Schema
     */
    protected $schema;

    public function __construct(ContainerInterface $container)
    {
        $this->schema = $container->get('schema');
    }

    public function __invoke(RequestInterface $request, ResponseInterface $response)
    {
        $uri    = $request->getUri();
        $host   = $uri->getHost();
        $port   = $uri->getPort();
        $scheme = $uri->getScheme();

        if ($port !== null && !in_array($port, [80, 443])) {
            $host .= ':' . $port;
        }

        $swagger = new Swagger($this->schema);
        $output  = $swagger->generate($host, '/v1', [$scheme]);

        $body = new Stream(fopen('php://temp', 'r+'));
        $body->write(json_encode($output));

        return $response
            ->withBody($body)
            ->withHeader('content-type', 'application/json');
    }
}
