<?php

namespace BBCWorldWide\JsonApi\Action;

use BBCWorldWide\JsonApi\Exceptions\RequiredPropertyException;
use BBCWorldWide\JsonApi\JsonApi\Data\Error\AttributeError;
use BBCWorldWide\JsonApi\JsonApi\Data\Error\ForbiddenError;
use BBCWorldWide\JsonApi\JsonApi\Data\Error\NotFoundError;
use BBCWorldWide\JsonApi\JsonApi\Encoder\EntityEncoder;
use BBCWorldWide\JsonApi\JsonApi\Encoder\PagedDataEncoder;
use BBCWorldWide\JsonApi\JsonApi\EntityInterface;
use BBCWorldWide\JsonApi\JsonApi\EntityMapper;
use BBCWorldWide\JsonApi\JsonApi\Exception\JsonApiException;
use BBCWorldWide\JsonApi\JsonApi\Parameters\ParametersParser;
use BBCWorldWide\JsonApi\JsonApi\Schema\RelationshipDefinition;
use BBCWorldWide\JsonApi\JsonApi\Schema\Schema;
use BBCWorldWide\JsonApi\JsonApi\Schema\TypeDefinition;
use BBCWorldWide\JsonApi\JsonApi\Store;
use BBCWorldWide\JsonApi\JsonApi\Validator\DocumentValidator;
use Interop\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;

/**
 * Base controller class for API CRUD actions.
 *
 * @author BBC Worldwide
 */
abstract class AbstractJsonApiAction
{
    /**
     * @var Schema
     */
    protected $schema;

    /**
     * @var EntityMapper
     */
    protected $mapper;

    /**
     * @var DocumentValidator
     */
    protected $documentValidator;

    /**
     * @var Store
     */
    protected $store;

    /**
     * @var EntityEncoder
     */
    protected $entityEncoder;

    /**
     * @var PagedDataEncoder
     */
    protected $pagedDataEncoder;

    /**
     * @var ParametersParser
     */
    protected $parametersParser;

    /**
     * @var string
     */
    protected $transactionName;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var float
     */
    protected $startTime;

    /**
     * @var array
     */
    protected $buildInfo;

    /**
     * ResourceController constructor.
     *
     * @param ContainerInterface $ci
     */
    public function __construct(ContainerInterface $ci)
    {
        $this->schema            = $ci->get('schema');
        $this->mapper            = $ci->get('entityMapper');
        $this->documentValidator = $ci->get('documentValidator');
        $this->store             = $ci->get('store');
        $this->entityEncoder     = $ci->get('entityEncoder');
        $this->pagedDataEncoder  = $ci->get('pagedDataEncoder');
        $this->logger            = $ci->get('logger');
        $this->buildInfo         = $ci->get('buildInfo');

        $this->startTime = microtime(true);
    }

    /**
     * Action controllers carry out their stuff on implementations of this method.
     *
     * @param ServerRequestInterface $request
     * @param ResponseInterface      $response
     * @param array                  $args
     *
     * @return ResponseInterface
     */
    abstract public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface;

    /**
     * @param $type
     * @param $id
     *
     * @return mixed
     */
    protected function requireEntity($type, $id)
    {
        $entity = $this->store->findOne($type, $id);

        if (!$entity) {
            throw new JsonApiException(
                new NotFoundError($this->schema->getType($type)->getAlias() . " '$id' is not available on this server")
            );
        }

        return $entity;
    }

    /**
     * Get type definition from schema
     *
     * @param string $type
     *
     * @return TypeDefinition
     *
     * @throws JsonApiException
     */
    protected function requireTypeDefinition(string $type)
    {
        return $this->schema->getType($type);
    }

    /**
     * @param string $type
     * @param string $relationshipName
     *
     * @return RelationshipDefinition
     */
    protected function requireRelationshipDefinition(string $type, string $relationshipName)
    {
        try {
            return $this->schema->getType($type)->getRelationship($relationshipName);
        } catch (\InvalidArgumentException $e) {
            throw new JsonApiException(
                new NotFoundError("Relationship '$relationshipName' not defined on '$type' type")
            );
        }
    }

    protected function ensureWritable(RelationshipDefinition $relationship)
    {
        if ($relationship->isReadOnly()) {
            throw new JsonApiException(
                new ForbiddenError("Relationship '{$relationship->getAlias()}' is read-only")
            );
        }
    }

    /**
     * Wrapper around Store save to handle errors common to any action.
     *
     * @param EntityInterface $entity
     *
     * @throws JsonApiException
     */
    protected function save(EntityInterface $entity)
    {
        try {
            $this->store->save($entity);
        } catch (RequiredPropertyException $ex) {
            $name = $ex->getMessage();
            throw new JsonApiException(
                new AttributeError($name, "Attribute '$name' is required")
            );
        }
    }

    /**
     * Assign friendly name to this transaction for profiling/reporting.
     *
     * @param string $name
     *
     * @codeCoverageIgnore
     */
    protected function nameTransaction(string $name)
    {
        $this->transactionName = $name;
        if (extension_loaded('newrelic')) {
            newrelic_name_transaction($name);
        }
    }

    protected function getDuration()
    {
        return microtime(true) - $this->startTime;
    }
}
