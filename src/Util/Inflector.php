<?php

namespace BBCWorldWide\JsonApi\Util;

use Doctrine\Common\Inflector\Inflector as DoctrineInflector;

class Inflector
{
    public static function hyphenate($str)
    {
        return str_replace(['_', ' '], '-', DoctrineInflector::tableize($str));
    }

    public static function classify($str)
    {
        return ucfirst(DoctrineInflector::camelize($str));
    }

    public static function camelize($str)
    {
        return DoctrineInflector::camelize($str);
    }

    public static function pluralize($str)
    {
        return DoctrineInflector::pluralize($str);
    }

    public static function singularize($str)
    {
        return DoctrineInflector::singularize($str);
    }

    public static function words($str)
    {
        return ucfirst(str_replace('_', ' ', DoctrineInflector::tableize($str)));
    }
}
