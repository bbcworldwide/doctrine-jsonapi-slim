<?php

namespace BBCWorldWide\JsonApi\Util;

/**
 * This class takes property names and tries to guess which setter / getter method would correspond to it.
 *
 * @author BBC Worldwide
 */
class Methodiser
{
    /**
     * Turns a property's name into its setter.
     *
     * @param string $name
     *
     * @return string
     */
    public static function setter(string $name): string
    {
        return 'set' . ucfirst(Inflector::camelize($name));
    }

    /**
     * Turns a property's name into its getter.
     *
     * This deals with Doctrine's idiosyncrasy whereby boolean values have to start with 'isBlah' and the getter is
     * called the same as the property.
     *
     * @param string $name
     *
     * @return string
     */
    public static function getter(string $name): string
    {
        $camelised = Inflector::camelize($name);

        if (preg_match('/^(is|has)[A-Z]/', $name)) {
            return $camelised;
        }

        return 'get' . ucfirst(Inflector::camelize($name));
    }

    /**
     * Makes an `addWhatever` method out of a property name.
     *
     * @param string $name
     *
     * @return string
     */
    public static function adder(string $name): string
    {
        return 'add' . ucfirst(Inflector::camelize($name));
    }

    /**
     * Makes an `addWhatever` method out of a property name.
     *
     * @param string $name
     *
     * @return string
     */
    public static function remover(string $name): string
    {
        return 'remove' . ucfirst(Inflector::camelize($name));
    }
}
