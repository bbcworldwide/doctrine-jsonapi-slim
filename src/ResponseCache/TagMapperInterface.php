<?php

namespace BBCWorldWide\JsonApi\ResponseCache;

use BBCWorldWide\JsonApi\JsonApi\EntityInterface;
use Psr\Http\Message\ServerRequestInterface;

interface TagMapperInterface
{
    /**
     * Get appropriate cache tags for a given request
     *
     * @param ServerRequestInterface $request Request object
     *
     * @return string[]                         Tags list
     */
    public function getTagsForRequest(ServerRequestInterface $request);

    /**
     * Get appropriate cache tags for a given entity
     *
     * @param EntityInterface|\stdClass $entity Entity
     *
     * @return string[]                         Tags list
     */
    public function getTagsForEntity($entity);

    /**
     * Does URL match a type in our schema?
     *
     * @return bool
     */
    public function matchesUrl(ServerRequestInterface $request);
}
