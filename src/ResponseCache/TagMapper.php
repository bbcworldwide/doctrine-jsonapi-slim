<?php

namespace BBCWorldWide\JsonApi\ResponseCache;

use BBCWorldWide\JsonApi\JsonApi\EntityInterface;
use BBCWorldWide\JsonApi\JsonApi\Schema\Schema;
use BBCWorldWide\JsonApi\Util\Inflector;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Tag mapper
 */
class TagMapper implements TagMapperInterface
{
    /**
     * @var Schema
     */
    private $schema;

    /**
     * TagMapper constructor.
     *
     * @param Schema $schema
     */
    public function __construct(Schema $schema)
    {
        $this->schema = $schema;
    }

    /**
     * @inheritdoc
     */
    public function matchesUrl(ServerRequestInterface $request)
    {
        $pathLevels = explode('/', $request->getUri()->getPath());
        return $pathLevels[1] === 'v1' && isset($pathLevels[2]) && $this->schema->hasType($pathLevels[2]);
    }

    /**
     * @inheritdoc
     */
    public function getTagsForRequest(ServerRequestInterface $request)
    {
        $tags = [];

        // Parse URI
        $uri        = $request->getUri();
        $path       = $uri->getPath();
        $pathLevels = explode('/', $path);
        $query      = $request->getQueryParams();

        if (!isset($pathLevels[2])) {
            return [];
        }

        // Base type
        $typeName     = $pathLevels[2];
        $typeDef      = $this->schema->getType($typeName);
        $baseTypeName = $this->getBaseType($typeName);

        // Use specific resource ID if present
        $id = $pathLevels[3] ?? null;
        if ($id) {
            $tags[] = "$baseTypeName:$id";
        } else {
            $tags[] = $baseTypeName;
        }

        // Relationship in path
        if (count($pathLevels) > 4) {
            // Relationship name is always last segment:
            // /v1/:type/:id/:relationship
            // /v1/:type/:id/relationships/:relationship
            $relationship = Inflector::camelize(array_pop($pathLevels));

            // Look up possible types for this relationship
            $relatedTypes     = $typeDef->getRelationship($relationship)->getTypes();
            $relatedBaseTypes = $this->getBaseTypes($relatedTypes);

            $tags = array_merge($tags, $relatedBaseTypes);
        }

        // Add types from To-One relationships
        foreach ($typeDef->getRelationships() as $relationship) {
            if (!$relationship->isMultiple()) {
                $relatedBaseTypes = $this->getBaseTypes($relationship->getTypes());
                $tags = array_merge($tags, $relatedBaseTypes);
            }
        }

        // Add types from includes
        if (isset($query['include'])) {
            $includes = explode(',', $query['include']);
            foreach ($includes as $relationshipName) {
                $includedTypes = $this->getRelatedTypes($baseTypeName, $relationshipName);
                $tags          = array_merge($tags, $includedTypes);
            }
        }

        return array_values(array_unique($tags));
    }

    protected function getRelatedTypes($typeName, $relationship)
    {
        $parts            = explode('.', $relationship);
        $relationshipName = array_shift($parts);

        $typeDef = $this->schema->getType($typeName);

        $relationshipDef = $typeDef->getRelationship($relationshipName);
        $relatedTypes    = $relationshipDef->getTypes();

        $types = $this->getBaseTypes($relatedTypes);
        // Should always resolve to a single base type
        $type = $types[0];

        if (!empty($parts)) {
            $nested = $this->getRelatedTypes($type, implode('.', $parts));
            $types  = array_unique(array_merge($types, $nested));
        }

        return $types;
    }

    /**
     * @inheritdoc
     */
    public function getTagsForEntity($entity)
    {
        $tags = [];

        // Schema types
        if ($entity instanceof EntityInterface && $type = $this->schema->getTypeForResource($entity)) {
            if ($baseTypeName = $type->getSubtypeOf()) {
                $type = $this->schema->getType($baseTypeName);
            }
            $typeName = $type->getName();

            $tags[] = $typeName;

            if ($id = $entity->getId()) {
                $tags[] = $typeName . ':' . $id;
            }
        }

        return array_unique($tags);
    }

    /**
     * Get a unique array of base types for an array of types
     *
     * @param string[] $typeNames
     *
     * @return string[]
     */
    private function getBaseTypes(array $typeNames): array
    {
        $types = [];
        foreach ($typeNames as $typeName) {
            $types[] = $this->getBaseType($typeName);
        }

        return array_unique($types);
    }

    /**
     * Get a unique array of base types for a given type
     *
     * @param string $typeName
     *
     * @return string
     */
    private function getBaseType(string $typeName): string
    {
        $typeDef = $this->schema->getType($typeName);
        if ($baseType = $typeDef->getSubtypeOf()) {
            return $baseType;
        }

        return $typeName;
    }
}
