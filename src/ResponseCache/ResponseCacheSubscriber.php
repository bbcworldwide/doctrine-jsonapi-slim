<?php

namespace BBCWorldWide\JsonApi\ResponseCache;

use BBCWorldWide\JsonApi\JsonApi\EntityInterface;
use Cache\Taggable\TaggablePoolInterface;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;

/**
 * Doctrine subscriber to purge response cache
 */
class ResponseCacheSubscriber implements EventSubscriber
{
    /**
     * @var TaggablePoolInterface
     */
    private $pool;

    /**
     * @var TagMapperInterface
     */
    private $mapper;

    /**
     * @var string[]
     */
    private $tags = [];

    /**
     * ResponseCacheSubscriber constructor.
     *
     * @param TaggablePoolInterface $pool
     * @param TagMapperInterface    $mapper
     */
    public function __construct(TaggablePoolInterface $pool, TagMapperInterface $mapper)
    {
        $this->pool   = $pool;
        $this->mapper = $mapper;
    }

    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
            Events::preUpdate,
            Events::preRemove,
            Events::postFlush,
        ];
    }

    /**
     * Pre-persist event
     *
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        if ($entity instanceof EntityInterface) {
            $this->processEntity($entity);
        }
    }

    /**
     * Pre-update event
     *
     * @param PreUpdateEventArgs $args
     */
    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getObject();
        if ($entity instanceof EntityInterface) {
            $this->processEntity($entity);
        }
    }

    /**
     * Pre-remove event
     *
     * @param LifecycleEventArgs $args
     */
    public function preRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        if ($entity instanceof EntityInterface) {
            $this->processEntity($entity);
        }
    }

    private function processEntity(EntityInterface $entity)
    {
        $tags       = $this->mapper->getTagsForEntity($entity);
        $this->tags = array_unique(array_merge($this->tags, $tags));
    }

    /**
     * Post-flush event
     */
    public function postFlush()
    {
        if (!empty($this->tags)) {
            $this->pool->clearTags($this->tags);
            $this->tags = [];
        }
    }
}
