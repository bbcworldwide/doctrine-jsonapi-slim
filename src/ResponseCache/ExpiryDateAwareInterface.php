<?php

namespace BBCWorldWide\JsonApi\ResponseCache;

/**
 * Entities implementing this interface will be able to specify their use by date, otherwise we're caching
 * forever.
 *
 * @package BBCWorldWide\JsonApi\ResponseCache
 */
interface ExpiryDateAwareInterface
{
    /**
     * MUST return the date this entity will be cached until.
     *
     * @return \DateTime
     */
    public function getExpiryDate(): \DateTime;
}
