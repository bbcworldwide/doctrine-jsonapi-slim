<?php

namespace BBCWorldWide\JsonApi\ResponseCache;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Calculates cache expiry date based on loaded entities.
 *
 * Some of these loaded entities will be able to provide an expiryDate, so use that.
 */
class ExpiryChecker
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ExpiryChecker constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Get expiry date
     *
     * @return \DateTime|null
     */
    public function getExpiryDate()
    {
        $loadedEntities = $this->getLoadedEntities();
        $now            = new \DateTime();

        /** @var \DateTime|null $expiryDate */
        $expiryDate = null;

        foreach ($loadedEntities as $entitiesByType) {
            foreach ($entitiesByType as $entity) {
                if ($entity instanceof ExpiryDateAwareInterface) {
                    $date = $entity->getExpiryDate();
                    if ($date && $date > $now) {
                        $expiryDate = $expiryDate ? min($expiryDate, $date) : $date;
                    }
                }
            }
        }

        return $expiryDate;
    }

    /**
     * Get loaded entities from entity manager using reflection.
     *
     * @link https://gist.github.com/lavoiesl/6455752
     *
     * @return array
     */
    private function getLoadedEntities()
    {
        $uow  = $this->entityManager->getUnitOfWork();
        $ref  = new \ReflectionObject($uow);
        $prop = $ref->getProperty('identityMap');
        $prop->setAccessible(true);

        return $prop->getValue($uow);
    }
}
