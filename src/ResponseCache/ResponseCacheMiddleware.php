<?php

namespace BBCWorldWide\JsonApi\ResponseCache;

use Cache\Taggable\TaggablePoolInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Http\Response;
use BBCWorldWide\JsonApi\JsonApi\Exception\JsonApiException;

/**
 * Response Cache Middleware
 */
class ResponseCacheMiddleware
{
    /**
     * @var TaggablePoolInterface
     */
    private $pool;

    /**
     * @var TagMapperInterface
     */
    private $mapper;

    /**
     * @var ExpiryChecker
     */
    private $expiryChecker;

    /**
     * ResponseCacheMiddleware constructor.
     *
     * @param TaggablePoolInterface $pool
     * @param TagMapperInterface    $mapper
     * @param ExpiryChecker         $expiryChecker
     */
    public function __construct(TaggablePoolInterface $pool, TagMapperInterface $mapper, ExpiryChecker $expiryChecker)
    {
        $this->pool          = $pool;
        $this->mapper        = $mapper;
        $this->expiryChecker = $expiryChecker;
    }

    public function __invoke(ServerRequestInterface $request, Response $response, $next)
    {
        $uri  = $request->getUri();
        $path = $uri->getPath();

        if (!$this->mapper->matchesUrl($request)) {
            return $next($request, $response);
        }

        if ($request->getMethod() !== 'GET') {
            $tags = $this->mapper->getTagsForRequest($request);
            $this->pool->clearTags($tags);
            return $next($request, $response);
        }

        $key  = sha1($path . '?' . $uri->getQuery());
        $item = $this->pool->getItem($key);

        if ($item->isHit()) {
            return $response
                ->withHeader('X-Response-Cache-Hit', '1')
                ->withHeader('X-Response-Cache-Tags', implode(',', $item->getTags()))
                ->write($item->get());
        }

        /** @var Response $res */
        $res = $next($request, $response);

        $res->getBody()->rewind();
        $bodyText = $res->getBody()->getContents();
        $item->set($bodyText);

        $tags = $this->mapper->getTagsForRequest($request);

        $item->setTags($tags);

        if ($expiryDate = $this->expiryChecker->getExpiryDate()) {
            $item->expiresAt($expiryDate);
            $res = $res->withHeader('X-Response-Cache-Expires', $expiryDate->format(\DateTime::ISO8601));
        }

        $this->pool->save($item);

        return $res->withHeader('X-Response-Cache-Hit', '0');
    }
}
