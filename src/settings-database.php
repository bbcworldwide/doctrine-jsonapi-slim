<?php
/**
 * Generates valid configuration for Doctrine/MasterSlaveConnection depending on how many slaves we have
 * on the environment
 */

// Base config: master (required)
$databaseSettings = [
    'master' => [
        'driver'        => 'pdo_mysql',
        'host'          => $_SERVER['APP_DATABASE_MASTER_HOST'] ?? 'http://192.168.17.18:18000/v1',
        'port'          => $_SERVER['APP_DATABASE_PORT']        ?? 3306,
        'dbname'        => $_SERVER['APP_DATABASE_NAME']        ?? 'catalogue',
        'user'          => $_SERVER['APP_DATABASE_USER']        ?? 'root',
        'password'      => $_SERVER['APP_DATABASE_PASSWORD']    ?? 'root-password',
        'serverVersion' => $_SERVER['APP_DATABASE_VERSION']     ?? '5.7',
    ],
];

// Add slaves, if any found on the environment. The following SHOULD be a comma separated list of hosts.
// Environment vars are always strings, hence all this `trim` and `strlen` to check whether we got something we can use.
$slaveHosts = $_SERVER['APP_DATABASE_SLAVE_HOSTS'] ?? null;
if (strlen(trim($slaveHosts)) > 0) {
    foreach (explode(',', $slaveHosts) as $key => $slaveHost) {
        if (strlen(trim($slaveHost)) > 0) {
            $key = sprintf('slave_%d', $key);
            $databaseSettings['slaves'][$key] = [
                'host'     => $slaveHost,
                'port'     => $_SERVER['APP_DATABASE_PORT']     ?? 3306,
                'dbname'   => $_SERVER['APP_DATABASE_NAME']     ?? 'catalogue',
                'user'     => $_SERVER['APP_DATABASE_USER']     ?? 'root',
                'password' => $_SERVER['APP_DATABASE_PASSWORD'] ?? 'root-password',
            ];
        }
    }
}

return $databaseSettings;
