<?php

namespace BBCWorldWide\JsonApi\Error;

use BBCWorldWide\JsonApi\JsonApi\Data\Document;
use BBCWorldWide\JsonApi\JsonApi\Data\Error\ApplicationError;
use BBCWorldWide\JsonApi\JsonApi\Exception\JsonApiException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Slim\Handlers\AbstractError;
use Slim\Http\Response;

/**
 * Slim 3 error handler.
 *
 * @author BBC Worldwide
 */
final class ErrorHandler extends AbstractError
{
    /**
     * @var string
     */
    private $env;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var array
     */
    private $buildInfo;

    /**
     * Constructor
     *
     * @param bool            $displayErrorDetails Set to true to display full details
     * @param string          $env                 Environment name
     * @param LoggerInterface $logger
     */
    public function __construct(
        $displayErrorDetails = false,
        string $env = null,
        LoggerInterface $logger = null,
        array $buildInfo
    ) {

        parent::__construct($displayErrorDetails);
        $this->env       = $env;
        $this->logger    = $logger;
        $this->buildInfo = $buildInfo;
    }

    /**
     * Intercept error, log and pass to slim's default error handler.
     *
     * @param ServerRequestInterface     $request
     * @param ResponseInterface|Response $response
     * @param \Throwable                 $error
     *
     * @return ResponseInterface
     * @throws \Throwable
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, \Throwable $error)
    {
        // Work out http error code from exception code
        $httpErrorCode = $error->getCode();
        if ($httpErrorCode < 400 || $httpErrorCode > 599) {
            $httpErrorCode = 500;
        }

        $doc = new Document();

        if ($error instanceof JsonApiException) {
            $doc->setErrors($error->getErrors());
        } else {
            // @codeCoverageIgnoreStart
            if ($this->env === 'test') {
                throw $error;
            }

            $doc->addError(new ApplicationError($error, $this->displayErrorDetails));
            // @codeCoverageIgnoreEnd
        }

        $uri = $request->getUri();
        $uriString = $uri->getScheme() . '://' . $uri->getHost() . $uri->getPath();
        if ($query = $uri->getQuery()) {
            $uriString .= '?' . $query;
        }

        $context = [
            'type'     => 'inbound-service-call',
            'request'  => [
                'method' => $request->getMethod(),
                'uri'    => $uriString,
            ],
            'response' => [
                'status'  => $httpErrorCode,
            ],
            'err' => $error,
        ];

        if ($error instanceof JsonApiException) {
            $context['doc'] = $doc;
        } else {
            $context['err'] = $error;
        }

        if ($httpErrorCode < 500) {
            $this->logger->warning($error->getMessage(), $context);
        } else {
            $this->logger->error($error->getMessage(), $context);
        }

        $doc->addMeta('build', $this->buildInfo);

        return $response
            ->withStatus($httpErrorCode)
            ->withHeader('Content-type', 'application/vnd.api+json')
            ->write($doc);
    }
}
