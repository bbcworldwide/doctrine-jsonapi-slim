<?php

namespace BBCWorldWide\JsonApi\Error;

use BBCWorldWide\JsonApi\JsonApi\Data\Document;
use BBCWorldWide\JsonApi\JsonApi\Data\Error\Error;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Handlers\NotFound;
use Slim\Http\Response;

/**
 * Slim 3 not found handler
 *
 * @author BBC Worldwide
 */
final class NotFoundHandler extends NotFound
{
    /**
     * @var array
     */
    private $buildInfo;

    /**
     * NotFoundHandler constructor.
     *
     * @param array $buildInfo
     */
    public function __construct(array $buildInfo)
    {
        $this->buildInfo = $buildInfo;
    }

    /**
     * Log errors before handling 404's the json api way.
     *
     * @param ServerRequestInterface $request
     * @param ResponseInterface      $response
     *
     * @return ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response)
    {
        $path   = $request->getUri()->getPath();
        $method = $request->getMethod();
        $doc    = new Document();
        $doc->addError(new Error('not-found', 'Page not found', "Could not $method path '$path'", 404));
        $doc->addMeta('build', $this->buildInfo);

        /** @var Response $response */
        return $response
            ->withStatus(404)
            ->withHeader('Content-Type', 'application/vnd.api+json')
            ->write($doc);
    }
}
