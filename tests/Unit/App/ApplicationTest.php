<?php

namespace BBCWorldWide\JsonApi\Tests\Unit\App;

use BBCWorldWide\JsonApi\App\Application;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Slim\App as SlimApp;
use Slim\Http\Response;

class ApplicationTest extends TestCase
{
    /**
     * @var \BBCWorldWide\JsonApi\App\Application
     */
    private $application;

    /**
     * @var SlimApp|\PHPUnit_Framework_MockObject_MockObject
     */
    private $slimMock;

    /**
     * @var LoggerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $loggerMock;

    public function setUp()
    {
        parent::setUp();

        $this->slimMock    = $this->getMockBuilder(SlimApp::class)->disableOriginalConstructor()->getMock();
        $this->loggerMock  = $this->getMockBuilder(LoggerInterface::class)->getMock();
        $this->application = new Application($this->slimMock, $this->loggerMock);
    }

    public function tearDown()
    {
        parent::tearDown();

        $this->slimMock    = null;
        $this->loggerMock  = null;
        $this->application = null;
    }

    /**
     * @test
     * @expectedException \RuntimeException
     */
    public function runDoesntBlockExceptions()
    {
        $this->slimMock
            ->expects(self::any())
            ->method(self::anything())
            ->willThrowException(new \RuntimeException());

        $this->application->run();
    }

    /**
     * @test
     */
    public function runSucceeds()
    {
        $this->slimMock
            ->expects(self::atLeastOnce())
            ->method('run');

        self::assertNull($this->application->run());
    }

    /**
     * @test
     * @expectedException \RuntimeException
     */
    public function processDoesntBlockExceptions()
    {
        /** @var ServerRequestInterface $request */
        $request  = $this->getMockBuilder(ServerRequestInterface::class)->getMock();
        $response = new Response();

        $this->slimMock
            ->expects(self::any())
            ->method(self::anything())
            ->willThrowException(new \RuntimeException());

        $this->application->process($request, $response);
    }

    /**
     * @test
     */
    public function processReturnsPsr7Response()
    {
        /** @var ServerRequestInterface $request */
        $request  = $this->getMockBuilder(ServerRequestInterface::class)->getMock();
        $response = new Response();

        $this->slimMock
            ->expects(self::once())
            ->method('process')
            ->with($request, $response)
            ->willReturn($response);

        self::assertSame($response, $this->application->process($request, $response));
    }
}
