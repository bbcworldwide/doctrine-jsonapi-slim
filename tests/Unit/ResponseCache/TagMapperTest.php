<?php

namespace BBCWorldWide\JsonApi\Tests\Unit\ResponseCache;

use BBCWorldWide\JsonApi\JsonApi\Schema\Schema;
use BBCWorldWide\JsonApi\ResponseCache\TagMapper;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Programme\Brand;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Programme\Episode;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Programme\Series;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Version;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Warning;
use Psr\Http\Message\StreamInterface;
use Slim\Http\Headers;
use Slim\Http\Request;
use Slim\Http\Uri;

class TagMapperTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var TagMapper
     */
    private $mapper;

    public function setUp()
    {
        $schemaArray = require(__DIR__ . '/../../Fixtures/schema.php');
        $schema      = new Schema($schemaArray);

        $this->mapper = new TagMapper($schema);
    }

    /**
     * @test
     * @dataProvider getTagsForEntityProvider
     */
    public function getTagsForEntity($entity, $expectedTags)
    {
        $tags = $this->mapper->getTagsForEntity($entity);
        self::assertEquals($expectedTags, $tags);
    }

    /**
     * @test
     * @dataProvider getTagsForRequestProvider
     */
    public function getTagsForRequest($uri, $expectedTags)
    {
        $body    = $this->getMockBuilder(StreamInterface::class)->getMock();
        $request = new Request('GET', Uri::createFromString($uri), new Headers(), [], [], $body);
        $tags    = $this->mapper->getTagsForRequest($request);
        self::assertEquals(sort($expectedTags), sort($tags));
    }

    public function getTagsForRequestProvider()
    {
        return [
            'base type index'                 => [
                'uri'          => 'http://example.com/v1/categories',
                'expectedTags' => ['categories'],
            ],
            'base type id'                    => [
                'uri'          => 'http://example.com/v1/categories/123',
                'expectedTags' => ['categories:123', 'categories'],
            ],
            'relationship in path'            => [
                'uri'          => 'http://example.com/v1/versions/123/versionTypes',
                'expectedTags' => ['versions:123', 'versionTypes', 'programmes'],
            ],
            'included relationship'           => [
                'uri'          => 'http://example.com/v1/categories/123?include=programmes',
                'expectedTags' => ['categories:123', 'categories', 'programmes'],
            ],
            'multiple included relationships' => [
                'uri'          => 'http://example.com/v1/categories/123?include=programmes,parent',
                'expectedTags' => ['categories:123', 'programmes', 'categories'],
            ],
            'included nested relationship'    => [
                'uri'          => 'http://example.com/v1/episodes/123?include=primaryParent.categories.parent',
                'expectedTags' => ['programmes:123', 'programmes', 'categories'],
            ],
        ];
    }

    public function getTagsForEntityProvider()
    {
        $series = new Brand();
        $series->setId('pr01');

        $episode = new Episode();
        $episode
            ->setId('ep01')
            ->setPrimaryParent($series);

        $version = new Version();
        $version->setId('v01')
                ->setProgramme($episode);

        return [
            'warning' => [
                'entity'       => (new Warning())->setId('foo'),
                'expectedTags' => ['warnings', 'warnings:foo'],
            ],
            'series'        => [
                'entity'       => $series,
                'expectedTags' => ['programmes', 'programmes:pr01'],
            ],
        ];
    }
}
