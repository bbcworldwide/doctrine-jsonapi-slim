<?php

namespace BBCWorldWide\JsonApi\Tests\Unit\JsonApi;

use BBCWorldWide\JsonApi\JsonApi\Schema\AttributeDefinition;
use BBCWorldWide\JsonApi\JsonApi\Schema\Schema;
use BBCWorldWide\JsonApi\JsonApi\Swagger;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Programme\AbstractProgramme;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Programme\Brand;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Programme\Episode;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\VersionType;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Warning;
use Entity\Category;
use Respect\Validation\Rules\Version;

class SwaggerTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @test
     */
    public function build()
    {
        $schema = [];

        $coreProgrammeAttributes = [
            'displayName'    => ['required' => true],
            'sortName'       => ['sortable' => true],
            'synopsisShort'  => [],
            'synopsisMedium' => [],
            'synopsisLong'   => [],
            'crid'           => ['filterable' => true],
            'createdAt'      => ['sortable' => true, 'readOnly' => true, 'type' => AttributeDefinition::TYPE_DATETIME],
            'updatedAt'      => ['sortable' => true, 'readOnly' => true, 'type' => AttributeDefinition::TYPE_DATETIME],
        ];

        $episodeAttributes = [
            'coreNumber'      => [],
            'origination'     => [],
            'contentLanguage' => [],
            'duration'        => ['type' => AttributeDefinition::TYPE_INT],
            'isRevoked'       => ['type' => AttributeDefinition::TYPE_BOOL, 'filterable' => true],
            'releaseDate'     => ['type' => AttributeDefinition::TYPE_DATETIME, 'sortable' => true],
        ];

        $groupAttributes = [
            'statedItems' => ['type' => AttributeDefinition::TYPE_INT],
        ];

        $schema['programmes'] = [
            'entityClass'   => AbstractProgramme::class,
            'alias'         => 'Programme',
            'attributes'    => $coreProgrammeAttributes + $groupAttributes + $episodeAttributes,
            'relationships' => [
                'descendants'   => ['multiple' => true, 'types' => ['episodes', 'collections'], 'readOnly' => true],
                'ancestors'     => ['multiple' => true, 'types' => ['brands', 'collections'], 'readOnly' => true],
                'primaryParent' => ['types' => ['brands'], 'readOnly' => true],
                'parents'       => ['multiple' => true, 'types' => ['brands', 'collections', 'programmes']],
                'children'      => ['multiple' => true, 'types' => ['episodes', 'collections']],
                'versions'      => ['multiple' => true, 'types' => ['versions']],
                'categories'    => ['multiple' => true, 'types' => ['categories']],
            ],
        ];

        $schema['brands'] = [
            'entityClass'   => Brand::class,
            'subtypeOf'     => 'programmes',
            'attributes'    => $coreProgrammeAttributes + $groupAttributes,
            'relationships' => [
                'descendants' => ['multiple' => true, 'types' => ['episodes', 'collections'], 'readOnly' => true],
                'children'    => ['multiple' => true, 'types' => ['episodes', 'collections']],
                'categories'  => ['multiple' => true, 'types' => ['categories']],
            ],
        ];

        $schema['episodes'] = [
            'entityClass'   => Episode::class,
            'subtypeOf'     => 'programmes',
            'attributes'    => $coreProgrammeAttributes + $episodeAttributes,
            'relationships' => [
                'ancestors'     => ['multiple' => true, 'types' => ['brands', 'collections'], 'readOnly' => true],
                'primaryParent' => ['types' => ['brands'], 'readOnly' => true],
                'parents'       => ['multiple' => true, 'types' => ['brands', 'collections', 'programmes']],
                'versions'      => ['multiple' => true, 'types' => ['versions']],
                'categories'    => ['multiple' => true, 'types' => ['categories']],
            ],
        ];

        $schema['categories'] = [
            'entityClass'   => Category::class,
            'attributes'    => [
                'crid'      => ['filterable' => true],
                'name'      => [],
                'position'  => ['sortable' => true, 'filterable' => true, 'type' => AttributeDefinition::TYPE_INT],
                'createdAt' => ['sortable' => true, 'readOnly' => true, 'type' => AttributeDefinition::TYPE_DATETIME],
                'updatedAt' => ['sortable' => true, 'readOnly' => true, 'type' => AttributeDefinition::TYPE_DATETIME],
            ],
            'relationships' => [
                'parent'     => ['types' => ['categories']],
                'children'   => ['multiple' => true, 'types' => ['categories']],
                'programmes' => ['multiple' => true, 'types' => ['programmes']],
            ],
        ];

        $schema['versions'] = [
            'entityClass'   => Version::class,
            'attributes'    => [
                'crid'             => ['filterable' => true],
                'cvCode'           => [],
                'deliveryStart'    => ['type' => AttributeDefinition::TYPE_DATETIME, 'sortable' => true],
                'deliveryEnd'      => ['type' => AttributeDefinition::TYPE_DATETIME, 'sortable' => true],
                'salesStart'       => ['type' => AttributeDefinition::TYPE_DATETIME, 'sortable' => true],
                'salesEnd'         => ['type' => AttributeDefinition::TYPE_DATETIME, 'sortable' => true],
                'isSigned'         => ['type' => AttributeDefinition::TYPE_BOOL, 'filterable' => true],
                'isAudioDescribed' => ['type' => AttributeDefinition::TYPE_BOOL, 'filterable' => true],
                'isPropagated'     => ['type' => AttributeDefinition::TYPE_BOOL, 'filterable' => true],
                'duration'         => ['type' => AttributeDefinition::TYPE_INT, 'required' => true],
                'contentLocation'  => ['required' => true],
                'encodingProfile'  => ['required' => true],
                'captureType'      => [],
                'captionLanguage'  => [],
                'aspectRatio'      => [],
                'createdAt'        => [
                    'sortable' => true,
                    'readOnly' => true,
                    'type'     => AttributeDefinition::TYPE_DATETIME,
                ],
                'updatedAt'        => [
                    'sortable' => true,
                    'readOnly' => true,
                    'type'     => AttributeDefinition::TYPE_DATETIME,
                ],
            ],
            'relationships' => [
                'versionTypes' => ['multiple' => true, 'types' => ['versionTypes']],
                'warnings'     => ['multiple' => true, 'types' => ['warnings']],
                'programme'    => ['types' => ['episodes']],
            ],
        ];

        $schema['versionTypes'] = [
            'entityClass'   => VersionType::class,
            'attributes'    => [],
            'relationships' => [
                'versions' => ['multiple' => true, 'types' => ['versions']],
            ],
        ];

        $schema['warnings'] = [
            'entityClass'   => Warning::class,
            'attributes'    => [
                'warningTextShort' => ['required' => true],
                'warningTextLong'  => ['required' => true],
            ],
            'relationships' => [
                'versions' => ['multiple' => true, 'types' => ['versions']],
            ],
        ];

        $swagger = new Swagger(new Schema($schema));

        $output = $swagger->generate('http://localhost', '/v1', ['http', 'https']);

        self::assertEquals($output['host'], 'http://localhost');
        self::assertEquals($output['basePath'], '/v1');
        self::assertEquals($output['schemes'], ['http', 'https']);
        self::assertNotEmpty($output['definitions']);
        self::assertNotEmpty($output['paths']);
        // Todo: add more assertions.
    }
}
