<?php

namespace BBCWorldWide\JsonApi\Tests\Unit\JsonApi\Exception;

use BBCWorldWide\JsonApi\JsonApi\Data\Error\Error;
use BBCWorldWide\JsonApi\JsonApi\Data\ErrorCollection;
use BBCWorldWide\JsonApi\JsonApi\Exception\JsonApiException;
use PHPUnit\Framework\TestCase;

/**
 * Normally wouldn't test exceptions, but this one is at the core of JSONAPI error handling.
 *
 * @author BBC Worldwide
 */
class JsonApiExceptionTest extends TestCase
{
    /**
     * @test
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage JsonApiException requires an Error or ErrorCollection
     */
    public function handlesNonSupportedErrors()
    {
        $error = [];

        new JsonApiException($error);
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage ErrorCollection cannot be empty
     */
    public function handlesEmptyErrorCollections()
    {
        $errors = new ErrorCollection();

        new JsonApiException($errors);
    }

    /**
     * @test
     */
    public function handlesSingleError()
    {
        $error          = new Error('foo', 'bar', 'foobar');
        $expectedErrors = new ErrorCollection();
        $expectedErrors->attach($error);

        $exception = new JsonApiException($error);

        self::assertEquals($expectedErrors, $exception->getErrors());
    }

    /**
     * @test
     */
    public function handlesMultipleErrors()
    {
        $error          = new Error('foo', 'bar', 'foobar');
        $expectedErrors = new ErrorCollection();
        $expectedErrors->attach($error);

        $exception = new JsonApiException($expectedErrors);

        self::assertEquals($expectedErrors, $exception->getErrors());
    }
}
