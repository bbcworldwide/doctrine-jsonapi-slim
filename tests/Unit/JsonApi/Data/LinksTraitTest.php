<?php

namespace BBCWorldWide\JsonApi\Tests\Unit\JsonApi\Data;

use BBCWorldWide\JsonApi\JsonApi\Data\Traits\Links;
use PHPUnit\Framework\TestCase;

class LinksTraitTest extends TestCase
{
    /**
     * @var BogusLinksTrait
     */
    private $instance;

    public function setUp()
    {
        parent::setUp();

        $this->instance = new BogusLinksTrait();
    }

    /**
     * @test
     */
    public function settersWork()
    {
        $links = [
            'one' => 'foo',
            'two' => 'bar',
        ];

        self::assertEmpty($this->instance->getLinks());
        self::assertSame($this->instance, $this->instance->setLinks($links));
        self::assertSame($links, $this->instance->getLinks());
    }

    /**
     * @test
     */
    public function addLink()
    {
        $links = [
            'one' => 'foo',
            'two' => 'bar',
        ];

        self::assertEmpty($this->instance->getLinks());

        foreach ($links as $name => $uri) {
            self::assertSame($this->instance, $this->instance->addLink($name, $uri));
        }

        self::assertSame($links, $this->instance->getLinks());
    }
}

class BogusLinksTrait
{
    use Links;
}
