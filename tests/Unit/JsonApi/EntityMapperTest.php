<?php

namespace BBCWorldWide\JsonApi\Tests\Unit\JsonApi;

use BBCWorldWide\JsonApi\JsonApi\EntityInterface;
use BBCWorldWide\JsonApi\JsonApi\EntityMapper;
use BBCWorldWide\JsonApi\JsonApi\Schema\AttributeDefinition;
use BBCWorldWide\JsonApi\JsonApi\Schema\Schema;
use BBCWorldWide\JsonApi\JsonApi\StoreInterface;
use Doctrine\Common\Collections\ArrayCollection;

class ExampleResourceEntity implements EntityInterface
{
    protected $id;

    protected $stringField;

    protected $intField;

    protected $floatField;

    protected $isBoolField;

    protected $dateTimeField;

    protected $parent;

    protected $children;

    protected $readOnlyRelationship;

    protected $position;

    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return ExampleResourceEntity
     */
    public function setId(string $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStringField()
    {
        return $this->stringField;
    }

    /**
     * @param mixed $stringField
     *
     * @return ExampleResourceEntity
     */
    public function setStringField($stringField)
    {
        $this->stringField = $stringField;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIntField()
    {
        return $this->intField;
    }

    /**
     * @param mixed $intField
     *
     * @return ExampleResourceEntity
     */
    public function setIntField($intField)
    {
        $this->intField = $intField;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFloatField()
    {
        return $this->floatField;
    }

    /**
     * @param mixed $floatField
     *
     * @return ExampleResourceEntity
     */
    public function setFloatField($floatField)
    {
        $this->floatField = $floatField;

        return $this;
    }

    /**
     * @return mixed
     */
    public function isBoolField()
    {
        return $this->isBoolField;
    }

    /**
     * @param mixed $isBoolField
     *
     * @return ExampleResourceEntity
     */
    public function setIsBoolField($isBoolField)
    {
        $this->isBoolField = $isBoolField;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateTimeField()
    {
        return $this->dateTimeField;
    }

    /**
     * @param mixed $dateTimeField
     *
     * @return ExampleResourceEntity
     */
    public function setDateTimeField($dateTimeField)
    {
        $this->dateTimeField = $dateTimeField;

        return $this;
    }

    /**
     * @return ExampleResourceEntity
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param ExampleResourceEntity $parent
     *
     * @return ExampleResourceEntity
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    public function getChildren()
    {
        return $this->children;
    }

    public function addChild($parent, $position = null)
    {
        if (!$this->children->contains($parent)) {
            $this->children->add($parent);
            $this->position = $position;
        }

        return $this;
    }

    public function removeChild(ExampleResourceEntity $parent)
    {
        if ($this->children->contains($parent)) {
            $this->children->removeElement($parent);
        }

        return $this;
    }

    /**
     * @return ExampleResourceEntity
     */
    public function getReadOnlyRelationship()
    {
        return $this->readOnlyRelationship;
    }

    /**
     * @param ExampleResourceEntity $readOnlyRelationship
     *
     * @return ExampleResourceEntity
     */
    public function setReadOnlyRelationship($readOnlyRelationship)
    {
        $this->readOnlyRelationship = $readOnlyRelationship;

        return $this;
    }

    public function positionWasSet()
    {
        return $this->position !== null;
    }
}

class EntityMapperTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var EntityMapper
     */
    protected $mapper;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|StoreInterface
     */
    protected $store;

    public function setUp()
    {
        $schema = new Schema([
            'example' => [
                'entityClass'   => ExampleResourceEntity::class,
                'attributes'    => [
                    'stringField'   => ['type' => AttributeDefinition::TYPE_STRING],
                    'floatField'    => ['type' => AttributeDefinition::TYPE_FLOAT],
                    'intField'      => ['type' => AttributeDefinition::TYPE_INT],
                    'isBoolField'   => ['type' => AttributeDefinition::TYPE_BOOL],
                    'dateTimeField' => ['type' => AttributeDefinition::TYPE_DATETIME],
                ],
                'relationships' => [
                    'parent'               => [
                        'types' => ['example'],
                    ],
                    'children'             => [
                        'types'    => ['example'],
                        'multiple' => true,
                    ],
                    'readOnlyRelationship' => [
                        'types'    => ['example'],
                        'readOnly' => true,
                    ],
                ],
            ],
        ]);

        $this->store  = $this->getMockBuilder(StoreInterface::class)->getMock();
        $this->mapper = new EntityMapper($schema, $this->store);
    }

    /**
     * @test
     */
    public function mapResourceObject()
    {
        $id          = '1';
        $stringVal   = 'foo';
        $floatVal    = 1.1;
        $intVal      = 2;
        $boolVal     = true;
        $dateTimeVal = new \DateTime();

        $resource = (object) [
            'data' => (object) [
                'id'            => $id,
                'type'          => 'example',
                'attributes'    => (object) [
                    'stringField'   => $stringVal,
                    'floatField'    => $floatVal,
                    'intField'      => $intVal,
                    'isBoolField'   => $boolVal,
                    'dateTimeField' => $dateTimeVal->format('Y-m-d\TH:i:s.uO'),
                ],
                'relationships' => (object) [
                    'parent'   => (object) [
                        'data' => (object) [
                            'id'   => '2',
                            'type' => 'example',
                        ],
                    ],
                    'children' => (object) [
                        'data' => [
                            (object) [
                                'id'   => '3',
                                'type' => 'example',
                            ],
                            (object) [
                                'id'   => '4',
                                'type' => 'example',
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $relatedEntity1 = new ExampleResourceEntity();
        $relatedEntity2 = new ExampleResourceEntity();
        $relatedEntity3 = new ExampleResourceEntity();
        $relatedEntity4 = new ExampleResourceEntity();

        $entities = [
            'example' => [
                '2' => $relatedEntity1,
                '3' => $relatedEntity2,
                '4' => $relatedEntity3,
            ],
        ];

        $this->store->method('findOne')->will($this->returnCallback(function ($type, $id) use ($entities) {
            return $entities[$type][$id];
        }));

        $entity = new ExampleResourceEntity();

        // Add existing relation, not in resource object, to be removed.
        $entity->addChild($relatedEntity4);

        $this->mapper->mapResourceObject($entity, $resource);

        self::assertEquals($id, $entity->getId());
        self::assertEquals($stringVal, $entity->getStringField());
        self::assertEquals($stringVal, $entity->getStringField());
        self::assertEquals($floatVal, $entity->getFloatField());
        self::assertEquals($intVal, $entity->getIntField());
        self::assertEquals($boolVal, $entity->isBoolField());
        self::assertEquals($dateTimeVal, $entity->getDateTimeField());

        self::assertCount(2, $entity->getChildren());
    }

    /**
     * @test
     */
    public function readOnlyRelationshipIsIgnored()
    {
        $id          = '1';
        $stringVal   = 'foo';
        $floatVal    = 1.1;
        $intVal      = 2;
        $boolVal     = true;
        $dateTimeVal = new \DateTime();

        $resource = (object) [
            'data' => (object) [
                'id'            => $id,
                'type'          => 'example',
                'attributes'    => (object) [
                    'dateTimeField' => $dateTimeVal->format('Y-m-d\TH:i:s.uO'),
                ],
                'relationships' => (object) [
                    'readOnlyRelationship' => (object) [
                        'data' => (object) [
                            'id'   => '2',
                            'type' => 'example',
                        ],
                    ],

                ],
            ],
        ];

        $relatedEntity1 = new ExampleResourceEntity();

        $entities = [
            'example' => [
                '2' => $relatedEntity1,
            ],
        ];

        $this->store->method('findOne')->will($this->returnCallback(function ($type, $id) use ($entities) {
            return $entities[$type][$id];
        }));

        $entity = new ExampleResourceEntity();

        $this->mapper->mapResourceObject($entity, $resource);

        self::assertNull($entity->getReadOnlyRelationship());
    }

    /**
     * @test
     * @expectedException \BBCWorldWide\JsonApi\JsonApi\Exception\JsonApiException
     */
    public function missingRelation()
    {
        $resource = (object) [
            'data' => (object) [
                'id'   => '2',
                'type' => 'example',
            ],
        ];

        $this->store->method('findOne')->willReturn(null);

        $entity = new ExampleResourceEntity();
        $this->mapper->mapRelationship($entity, 'parent', $resource);
    }

    /**
     * @test
     */
    public function addToRelationshipToOne()
    {
        $resource = (object) [
            'data' => (object) [
                'id'   => '2',
                'type' => 'example',
            ],
        ];

        $relatedEntity = new ExampleResourceEntity();

        $this->store->method('findOne')->willReturn($relatedEntity);

        $entity = new ExampleResourceEntity();
        $this->mapper->addToRelationship($entity, 'parent', $resource);

        self::assertEquals($relatedEntity, $entity->getParent());
    }

    /**
     * @test
     */
    public function addToRelationshipToMany()
    {
        $resource = (object) [
            'data' => [
                (object) [
                    'id'   => '2',
                    'type' => 'example',
                ],
                (object) [
                    'id'   => '3',
                    'type' => 'example',
                ],
            ],
        ];

        $relatedEntity1 = new ExampleResourceEntity();
        $relatedEntity2 = new ExampleResourceEntity();
        $relatedEntity3 = new ExampleResourceEntity();

        $entities = [
            'example' => [
                '2' => $relatedEntity1,
                '3' => $relatedEntity2,
            ],
        ];

        $this->store->method('findOne')->will($this->returnCallback(function ($type, $id) use ($entities) {
            return $entities[$type][$id];
        }));

        $entity = new ExampleResourceEntity();

        // Add existing relation - should not be removed.
        $entity->addChild($relatedEntity3);

        $this->mapper->addToRelationship($entity, 'children', $resource);

        self::assertCount(3, $entity->getChildren());
    }

    /**
     * @test
     */
    public function addToRelationshipToManyWithPosition()
    {
        $resource = (object) [
            'data' => [
                (object) [
                    'id'   => '2',
                    'type' => 'example',
                    'meta' => (object) [
                        'position' => 1,
                    ],
                ],
            ],
        ];

        $relatedEntity1 = new ExampleResourceEntity();
        $this->store->method('findOne')->willReturn($relatedEntity1);

        $entity = new ExampleResourceEntity();

        $this->mapper->addToRelationship($entity, 'children', $resource);

        self::assertCount(1, $entity->getChildren());
        self::assertTrue($entity->positionWasSet());
    }

    /**
     * @test
     */
    public function removeRelationshipToOne()
    {
        $entity = new ExampleResourceEntity();
        $entity->setParent(new ExampleResourceEntity());

        $this->mapper->deleteFromRelationship($entity, 'parent', null);
        self::assertNull($entity->getParent());
    }

    /**
     * @test
     */
    public function removeRelationshipToMany()
    {
        $resource = (object) [
            'data' => [
                (object) [
                    'id'   => '2',
                    'type' => 'example',
                ],
                (object) [
                    'id'   => '3',
                    'type' => 'example',
                ],
            ],
        ];

        $relatedEntity1 = new ExampleResourceEntity();
        $relatedEntity2 = new ExampleResourceEntity();
        $relatedEntity3 = new ExampleResourceEntity();

        $entities = [
            'example' => [
                '2' => $relatedEntity1,
                '3' => $relatedEntity2,
            ],
        ];

        $this->store->method('findOne')->will($this->returnCallback(function ($type, $id) use ($entities) {
            return $entities[$type][$id];
        }));

        $entity = new ExampleResourceEntity();
        $entity
            ->addChild($relatedEntity1)
            ->addChild($relatedEntity2)
            ->addChild($relatedEntity3); // Should not be removed - not in object.

        $this->mapper->deleteFromRelationship($entity, 'children', $resource);

        self::assertCount(1, $entity->getChildren());
    }
}
