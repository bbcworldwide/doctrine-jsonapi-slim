<?php


namespace BBCWorldWide\JsonApi\Tests\Unit\JsonApi\Schema;


use BBCWorldWide\JsonApi\JsonApi\Schema\AttributeDefinition;
use BBCWorldWide\JsonApi\JsonApi\Schema\RelationshipDefinition;
use BBCWorldWide\JsonApi\JsonApi\Schema\TypeDefinition;

class TypeDefinitionTest extends \PHPUnit\Framework\TestCase
{
    /** @var array */
    protected $config;

    /** @var TypeDefinition */
    protected $type;

    public function setUp()
    {
        $this->config = [
            'entityClass'   => 'ExampleClass',
            'attributes'    => [
                'name'      => ['filterable' => true],
                'createdAt' => ['sortable' => true, 'readOnly' => true, 'type' => AttributeDefinition::TYPE_DATETIME],
                'updatedAt' => ['sortable' => true, 'readOnly' => true, 'type' => AttributeDefinition::TYPE_DATETIME],
            ],
            'relationships' => [
                'example2'  => ['multiple' => false, 'types' => ['example2']],
                'example3'  => ['includable' => false, 'types' => ['example3']],
            ],
            'customFilters' => ['filter1', 'filter2', 'filter3'],
            'subtypeOf'     => 'base-type',
            'subtypes'      => ['subtype1', 'subtype2'],
        ];
        $this->type = new TypeDefinition('example1', $this->config);
    }

    public function testGetters()
    {
        $this->assertEquals('example1', $this->type->getName());
        $this->assertCount(3, $this->type->getAttributes());
        $this->assertInstanceOf(AttributeDefinition::class, $this->type->getAttribute('name'));
        $this->assertCount(2, $this->type->getRelationships());
        $this->assertInstanceOf(RelationshipDefinition::class, $this->type->getRelationship('example2'));
        $this->assertEquals('ExampleClass', $this->type->getEntityClass());
        $this->assertEquals('base-type', $this->type->getSubtypeOf());
        $this->assertCount(2, $this->type->getSubtypes());
        $this->assertCount(3, $this->type->getCustomFilters());
    }

    public function testDefaultAlias()
    {
        $this->assertEquals('Example class', $this->type->getAlias());
    }

    public function testCustomAlias()
    {
        $config = $this->config;
        $config['alias'] = 'Custom alias';
        $type = new TypeDefinition('example1', $config);
        $this->assertEquals('Custom alias', $type->getAlias());
    }

    public function testGetBaseType()
    {
        $this->assertEquals('base-type', $this->type->getBaseType());
    }

    public function testGetBaseTypeSelf()
    {
        $config = $this->config;
        unset($config['subtypeOf']);
        $type = new TypeDefinition('example1', $config);
        $this->assertEquals('example1', $type->getBaseType());
    }

    public function testGetConcreteTypes()
    {
        $this->assertEquals($this->config['subtypes'], $this->type->getConcreteTypes());
    }

    public function testGetConcreteTypesSelf()
    {
        $config = $this->config;
        unset($config['subtypes']);
        $type = new TypeDefinition('example1', $config);
        $this->assertEquals(['example1'], $type->getConcreteTypes());
    }

    public function testGetFilterableAttributes()
    {
        $this->assertCount(4, $this->type->getFilterableAttributes());
    }

    public function testGetSortableAttributes()
    {
        $this->assertEquals(['createdAt', 'updatedAt'], $this->type->getSortableAttributes());
    }

    public function testGetIncludableRelationships()
    {
        $this->assertEquals(['example2'], $this->type->getIncludableRelationships());
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testGetAttributeInvalid()
    {
        $this->type->getAttribute('notAnAttribute');
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testGetRelationshipInvalid()
    {
        $this->type->getRelationship('notARelationship');
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testNoEntityClass()
    {
        $config = $this->config;
        unset($config['entityClass']);
        new TypeDefinition('example1', $config);
    }
}
