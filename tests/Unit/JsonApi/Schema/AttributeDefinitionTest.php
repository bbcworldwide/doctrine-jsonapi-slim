<?php

namespace BBCWorldWide\JsonApi\Tests\Unit\JsonApi\Schema;

use BBCWorldWide\JsonApi\JsonApi\Schema\AttributeDefinition;

class AttributeDefinitionTest extends \PHPUnit\Framework\TestCase
{
    public function testDefaults()
    {
        $def = new AttributeDefinition('myExample');
        $this->assertEquals('My example', $def->getAlias());
        $this->assertEquals(AttributeDefinition::TYPE_STRING, $def->getType());
        $this->assertFalse($def->isFilterable());
        $this->assertFalse($def->isRequired());
        $this->assertFalse($def->isReadOnly());
        $this->assertFalse($def->isSortable());
    }

    public function testConfig()
    {
        $conf = [
            'alias' => 'Custom alias',
            'sortable' => true,
            'filterable' => true,
            'readOnly' => true,
            'required' => true,
            'type' => AttributeDefinition::TYPE_FLOAT,
        ];
        $def = new AttributeDefinition('myExample', $conf);
        $this->assertEquals('Custom alias', $def->getAlias());
        $this->assertEquals(AttributeDefinition::TYPE_FLOAT, $def->getType());
        $this->assertTrue($def->isFilterable());
        $this->assertTrue($def->isRequired());
        $this->assertTrue($def->isReadOnly());
        $this->assertTrue($def->isSortable());
    }
}
