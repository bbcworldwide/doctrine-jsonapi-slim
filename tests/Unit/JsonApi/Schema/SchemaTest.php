<?php

namespace BBCWorldWide\JsonApi\Tests\Unit\JsonApi\Schema;

use BBCWorldWide\JsonApi\JsonApi\EntityInterface;
use BBCWorldWide\JsonApi\JsonApi\Schema\AttributeDefinition;
use BBCWorldWide\JsonApi\JsonApi\Schema\Schema;
use BBCWorldWide\JsonApi\JsonApi\Schema\TypeDefinition;

class Example1 implements EntityInterface
{
    protected $id;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id)
    {
        $this->id = $id;

        return $this;
    }
}

class Example2 implements EntityInterface
{
    protected $id;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id)
    {
        $this->id = $id;

        return $this;
    }
}

class SchemaTest extends \PHPUnit\Framework\TestCase
{
    /** @var Schema */
    protected $schema;

    public function setUp()
    {
        $config = [
            'example1' => [
                'entityClass'   => Example1::class,
                'attributes'    => [
                    'name'      => [],
                    'createdAt' => [
                        'sortable' => true,
                        'readOnly' => true,
                        'type'     => AttributeDefinition::TYPE_DATETIME,
                    ],
                    'updatedAt' => [
                        'sortable' => true,
                        'readOnly' => true,
                        'type'     => AttributeDefinition::TYPE_DATETIME,
                    ],
                ],
                'relationships' => [
                    'example2' => ['multiple' => false, 'types' => ['example2']],
                ],
            ],
            'example2' => [
                'entityClass'   => Example2::class,
                'attributes'    => [
                    'name'      => [],
                    'createdAt' => [
                        'sortable' => true,
                        'readOnly' => true,
                        'type'     => AttributeDefinition::TYPE_DATETIME,
                    ],
                    'updatedAt' => [
                        'sortable' => true,
                        'readOnly' => true,
                        'type'     => AttributeDefinition::TYPE_DATETIME,
                    ],
                ],
                'relationships' => [
                    'example1' => ['multiple' => true, 'types' => ['example1']],
                ],
            ],
        ];

        $this->schema = new Schema($config);
    }

    /**
     * @test
     */
    public function getTypes()
    {
        $this->assertCount(2, $this->schema->getTypes());
    }

    /**
     * @test
     */
    public function getType()
    {
        $type = $this->schema->getType('example1');
        $this->assertInstanceOf(TypeDefinition::class, $type);
        $this->assertEquals('example1', $type->getName());
    }

    /**
     * @test
     * @expectedException \BBCWorldWide\JsonApi\JsonApi\Exception\JsonApiException
     */
    public function getTypeInvalid()
    {
        $this->schema->getType('notAType');
    }

    /**
     * @test
     */
    public function getTypeByClassName()
    {
        $type = $this->schema->getTypeByClassName(Example2::class);
        $this->assertInstanceOf(TypeDefinition::class, $type);
        $this->assertEquals('example2', $type->getName());

        $this->assertNull($this->schema->getTypeByClassName('notAClass'));
    }

    /**
     * @test
     */
    public function getTypeForResource()
    {
        $type = $this->schema->getTypeForResource(new Example1());
        $this->assertInstanceOf(TypeDefinition::class, $type);
        $this->assertEquals('example1', $type->getName());
    }

    /**
     * @test
     */
    public function hasType()
    {
        $this->assertTrue($this->schema->hasType('example1'));
    }
}
