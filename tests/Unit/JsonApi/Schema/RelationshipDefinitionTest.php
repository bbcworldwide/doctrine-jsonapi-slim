<?php


namespace BBCWorldWide\JsonApi\Tests\Unit\JsonApi\Schema;


use BBCWorldWide\JsonApi\JsonApi\Schema\RelationshipDefinition;

class RelationshipDefinitionTest extends \PHPUnit\Framework\TestCase
{
    /*
     *     protected $name;
    protected $alias;
    protected $singular;
    protected $readOnly         = false;
    protected $includable       = true;
    protected $includeByDefault = false;
    protected $required         = false;
    protected $multiple         = false;
    protected $types            = [];
     */
    public function testDefaults()
    {
        $conf = [
            'types' => ['example'],
        ];
        $def = new RelationshipDefinition('myExample', $conf);
        $this->assertEquals('My example', $def->getAlias());
        $this->assertTrue($def->isIncludable());
        $this->assertFalse($def->isIncludeByDefault());
        $this->assertFalse($def->isRequired());
        $this->assertFalse($def->isReadOnly());
        $this->assertFalse($def->isMultiple());
    }

    public function testDefaultSingular()
    {
        $conf = [
            'multiple' => true,
            'types' => ['example'],
        ];
        $def = new RelationshipDefinition('myExamples', $conf);
        $this->assertEquals('myExample', $def->getSingular());
    }

    public function testConfig()
    {
        $conf = [
            'alias' => 'Custom alias',
            'singular' => 'Example',
            'readOnly' => true,
            'includable' => false,
            'includeByDefault' => false,
            'required' => true,
            'multiple' => true,
            'types' => ['example'],
        ];
        $def = new RelationshipDefinition('myExample', $conf);
        $this->assertEquals('myExample', $def->getName());
        $this->assertEquals('Custom alias', $def->getAlias());
        $this->assertEquals('Example', $def->getSingular());
        $this->assertFalse($def->isIncludable());
        $this->assertTrue($def->isRequired());
        $this->assertTrue($def->isReadOnly());
        $this->assertTrue($def->isMultiple());
        $this->assertEquals(['example'], $def->getTypes());
    }
}
