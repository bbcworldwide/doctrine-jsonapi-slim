<?php


namespace BBCWorldWide\JsonApi\Tests\Unit\JsonApi\Validator;

use BBCWorldWide\JsonApi\JsonApi\Schema\AttributeDefinition;
use BBCWorldWide\JsonApi\JsonApi\Schema\RelationshipDefinition;
use BBCWorldWide\JsonApi\JsonApi\Schema\Schema;
use BBCWorldWide\JsonApi\JsonApi\Validator\DocumentValidator;

class DocumentValidatorTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var DocumentValidator
     */
    protected $validator;

    public function setUp()
    {
        $schema = new Schema([
            'example' => [
                'entityClass' => 'DummyClass',
                'attributes' => [
                    'stringField'   => ['type' => AttributeDefinition::TYPE_STRING, 'sortable' => true],
                    'floatField'    => ['type' => AttributeDefinition::TYPE_FLOAT, 'sortable' => true],
                    'intField'      => ['type' => AttributeDefinition::TYPE_INT, 'filterable' => true],
                    'isBoolField'   => ['type' => AttributeDefinition::TYPE_BOOL, 'filterable' => true],
                    'dateTimeField' => ['type' => AttributeDefinition::TYPE_DATETIME],
                ],
                'relationships' => [
                    'parent' => ['types' => ['example']],
                    'children' => ['types' => ['example'], 'multiple' => true, 'includable' => false],
                ]
            ],
        ]);
        $this->validator = new DocumentValidator($schema);
    }

    /**
     * @test
     */
    public function ensureObjectSuccess()
    {
        $val = new \stdClass();
        $result = $this->validator->ensureObject($val);

        self::assertTrue($result);
        self::assertTrue($this->validator->isValid());
        self::assertEmpty($this->validator->getErrors());
    }

    /**
     * @test
     */
    public function ensureObjectFail()
    {
        $val = 'notAnObject';
        $result = $this->validator->ensureObject($val);

        self::assertFalse($result);
        self::assertFalse($this->validator->isValid());
        self::assertNotEmpty($this->validator->getErrors());
    }

    /**
     * @test
     */
    public function ensureDataSuccess()
    {
        $val = new \stdClass();
        $val->data = [];
        $result = $this->validator->ensureData($val);

        self::assertTrue($result);
        self::assertTrue($this->validator->isValid());
        self::assertEmpty($this->validator->getErrors());
    }

    /**
     * @test
     */
    public function ensureDataFail()
    {
        $val = new \stdClass();
        $result = $this->validator->ensureData($val);

        self::assertFalse($result);
        self::assertFalse($this->validator->isValid());
        self::assertNotEmpty($this->validator->getErrors());
    }

    /**
     * @test
     */
    public function validateAttributeSuccess()
    {
        $def = new AttributeDefinition('example', [
            'type' => AttributeDefinition::TYPE_DATETIME,
        ]);
        $result = $this->validator->validateAttribute('2016-06-03', $def);

        self::assertTrue($result);
        self::assertTrue($this->validator->isValid());
        self::assertEmpty($this->validator->getErrors());
    }

    /**
     * @test
     */
    public function validateAttributeOptionalSuccess()
    {
        $def = new AttributeDefinition('example', [
            'type' => AttributeDefinition::TYPE_DATETIME,
        ]);
        $result = $this->validator->validateAttribute(null, $def);

        self::assertTrue($result);
        self::assertTrue($this->validator->isValid());
        self::assertEmpty($this->validator->getErrors());
    }

    /**
     * @test
     */
    public function validateAttributeRequiredFail()
    {
        $def = new AttributeDefinition('example', [
            'type' => AttributeDefinition::TYPE_STRING,
            'required' => true,
        ]);
        $result = $this->validator->validateAttribute(null, $def);

        self::assertFalse($result);
        self::assertFalse($this->validator->isValid());
        self::assertNotEmpty($this->validator->getErrors());
    }

    /**
     * @test
     */
    public function validateAttributeWrongTypeFail()
    {
        $def = new AttributeDefinition('example', [
            'type' => AttributeDefinition::TYPE_STRING,
        ]);
        $result = $this->validator->validateAttribute(123, $def);

        self::assertFalse($result);
        self::assertFalse($this->validator->isValid());
        self::assertNotEmpty($this->validator->getErrors());
    }

    /**
     * @test
     */
    public function validateAttributeInvalidDateFail()
    {
        $def = new AttributeDefinition('example', [
            'type' => AttributeDefinition::TYPE_DATETIME,
        ]);
        $result = $this->validator->validateAttribute('NotADate', $def);

        self::assertFalse($result);
        self::assertFalse($this->validator->isValid());
        self::assertNotEmpty($this->validator->getErrors());
    }

    /**
     * @test
     */
    public function validateResourceIdentifierObjectSuccess()
    {
        $res = (object) ['id' => '123', 'type' => 'example'];
        $def = new RelationshipDefinition('example', ['types' => ['example']]);
        $result = $this->validator->validateResourceIdentifierObject($res, $def);

        self::assertTrue($result);
        self::assertTrue($this->validator->isValid());
        self::assertEmpty($this->validator->getErrors());
    }

    /**
     * @test
     */
    public function validateResourceIdentifierObjectMissingIdFail()
    {
        $res = (object) ['type' => 'example'];
        $def = new RelationshipDefinition('example', ['types' => ['example']]);
        $result = $this->validator->validateResourceIdentifierObject($res, $def);

        self::assertFalse($result);
        self::assertFalse($this->validator->isValid());
        self::assertNotEmpty($this->validator->getErrors());
    }

    /**
     * @test
     */
    public function validateResourceIdentifierObjectMissingTypeFail()
    {
        $res = (object) ['id' => '123'];
        $def = new RelationshipDefinition('example', ['types' => ['example']]);
        $result = $this->validator->validateResourceIdentifierObject($res, $def);

        self::assertFalse($result);
        self::assertFalse($this->validator->isValid());
        self::assertNotEmpty($this->validator->getErrors());
    }

    /**
     * @test
     */
    public function validateResourceIdentifierObjectInvalidTypeFail()
    {
        $res = (object) ['id' => '123', 'type' => 'series'];
        $def = new RelationshipDefinition('example', ['types' => ['example']]);
        $result = $this->validator->validateResourceIdentifierObject($res, $def);

        self::assertFalse($result);
        self::assertFalse($this->validator->isValid());
        self::assertNotEmpty($this->validator->getErrors());
    }

    /**
     * @test
     */
    public function validateResourceIdentifierObjectNotFoundTypeFail()
    {
        $res = (object) ['id' => '123', 'type' => 'not-found-type'];
        $def = new RelationshipDefinition('example', ['types' => ['example']]);
        $result = $this->validator->validateResourceIdentifierObject($res, $def);

        self::assertFalse($result);
        self::assertFalse($this->validator->isValid());
        self::assertNotEmpty($this->validator->getErrors());
    }

    /**
     * @test
     */
    public function validateRelationshipObjectToOneSuccess()
    {
        $res = (object) ['data' => (object) ['id' => '123', 'type' => 'example']];

        $def = new RelationshipDefinition('example', ['types' => ['example']]);
        $result = $this->validator->validateRelationshipObject($res, $def);

        self::assertTrue($result);
        self::assertTrue($this->validator->isValid());
        self::assertEmpty($this->validator->getErrors());
    }

    /**
     * @test
     */
    public function validateRelationshipObjectToManySuccess()
    {
        $res = (object) ['data' => [(object) ['id' => '123', 'type' => 'example']]];

        $def = new RelationshipDefinition('example', ['types' => ['example'], 'multiple' => true]);
        $result = $this->validator->validateRelationshipObject($res, $def);

        self::assertTrue($result);
        self::assertTrue($this->validator->isValid());
        self::assertEmpty($this->validator->getErrors());
    }

    /**
     * @test
     */
    public function validateRelationshipObjectToManyFail()
    {
        $res = (object) ['data' => (object) ['id' => '123', 'type' => 'example']];

        $def = new RelationshipDefinition('example', ['types' => ['example'], 'multiple' => true]);
        $result = $this->validator->validateRelationshipObject($res, $def);

        self::assertFalse($result);
        self::assertFalse($this->validator->isValid());
        self::assertNotEmpty($this->validator->getErrors());
    }

    /**
     * @test
     */
    public function validateRelationshipOptionalSuccess()
    {
        $res = (object) ['data' => null];

        $def = new RelationshipDefinition('example', ['types' => ['example']]);
        $result = $this->validator->validateRelationshipObject($res, $def);

        self::assertTrue($result);
        self::assertTrue($this->validator->isValid());
        self::assertEmpty($this->validator->getErrors());
    }

    /**
     * @test
     */
    public function validateRelationshipRequiredFail()
    {
        $res = (object) ['data' => null];

        $def = new RelationshipDefinition('example', ['types' => ['example'], 'required' => true]);
        $result = $this->validator->validateRelationshipObject($res, $def);

        self::assertFalse($result);
        self::assertFalse($this->validator->isValid());
        self::assertNotEmpty($this->validator->getErrors());
    }

    /**
     * @test
     */
    public function validateResourceObjectSuccess()
    {
        $id = '1';
        $stringVal = 'foo';
        $floatVal = 1.1;
        $intVal = 2;
        $boolVal = true;
        $dateTimeVal = new \DateTime();

        $res = (object) [
            'data' => (object) [
                'id' => $id,
                'type' => 'example',
                'attributes' => (object) [
                    'stringField'   => $stringVal,
                    'floatField'    => $floatVal,
                    'intField'      => $intVal,
                    'isBoolField'   => $boolVal,
                    'dateTimeField' => $dateTimeVal->format(\DateTime::ISO8601),
                ],
                'relationships' => (object) [
                    'parent' => (object) [
                        'data' => (object) [
                            'id' => '2',
                            'type' => 'example',
                        ],
                    ],
                    'children' => (object) [
                        'data' => [
                            (object) [
                                'id' => '3',
                                'type' => 'example',
                            ],
                            (object) [
                                'id' => '4',
                                'type' => 'example',
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $result = $this->validator->validateResourceObject($res, ['example']);

        self::assertTrue($result);
        self::assertTrue($this->validator->isValid());
        self::assertEmpty($this->validator->getErrors());
    }

    /**
     * @test
     */
    public function validateResourceObjectNoTypeFail()
    {
        $res = (object) [
            'data' => (object) [
                'id' => '1',
            ],
        ];
        $result = $this->validator->validateResourceObject($res, ['example']);

        self::assertFalse($result);
        self::assertFalse($this->validator->isValid());
        self::assertNotEmpty($this->validator->getErrors());
    }

    /**
     * @test
     */
    public function validateResourceObjectDisallowedTypeFail()
    {
        $res = (object) [
            'data' => (object) [
                'id' => '1',
                'type' => 'disallowed-type',
            ],
        ];
        $result = $this->validator->validateResourceObject($res, ['example']);

        self::assertFalse($result);
        self::assertFalse($this->validator->isValid());
        self::assertNotEmpty($this->validator->getErrors());
    }

    /**
     * @test
     */
    public function validateResourceObjectNotObjectFail()
    {
        $res = 'notAnObject';
        $result = $this->validator->validateResourceObject($res, ['example']);

        self::assertFalse($result);
        self::assertFalse($this->validator->isValid());
        self::assertNotEmpty($this->validator->getErrors());
    }

    /**
     * @test
     */
    public function reset()
    {
        $res = 'notAnObject';
        $this->validator->validateResourceObject($res, ['example']);

        self::assertFalse($this->validator->isValid());
        self::assertNotEmpty($this->validator->getErrors());
        $this->validator->reset();
        self::assertTrue($this->validator->isValid());
        self::assertEmpty($this->validator->getErrors());
    }
}
