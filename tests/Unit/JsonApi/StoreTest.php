<?php

namespace BBCWorldWide\JsonApi\Tests\Unit\JsonApi;

use BBCWorldWide\JsonApi\JsonApi\Parameters\ParameterSet;
use BBCWorldWide\JsonApi\JsonApi\Parameters\SortParameter;
use BBCWorldWide\JsonApi\JsonApi\Schema\Schema;
use BBCWorldWide\JsonApi\JsonApi\Store;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Category;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\QueryBuilder;

class TestableStore extends Store
{
    protected function getPagedQuery(QueryBuilder $builder, ParameterSet $parameters, $cacheResults = false)
    {
        // NOOP this method because Queries cannot be mocked.
    }
}

class RepoWithBuilderMethod
{
    protected $builder;

    public function __construct($builder)
    {
        $this->builder = $builder;
    }

    public function getParentQueryBuilder()
    {
        return $this->builder;
    }
}

class StoreTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Store
     */
    protected $store;

    /**
     * @var Schema
     */
    protected $schema;

    /**
     * @var EntityManagerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $em;

    public function setUp()
    {
        $this->em = $this->getMockBuilder(EntityManagerInterface::class)->getMock();

        $this->schema = new Schema([
            'type1' => [
                'entityClass'   => 'Type1Entity',
                'relationships' => [
                    'parent' => [
                        'types' => ['type2'],
                    ],
                ],
                'defaultSort'   => '-createdAt',
            ],
            'type2' => ['entityClass' => 'Type2Entity'],
        ]);
        $this->store  = new TestableStore($this->em, $this->schema);
    }

    public function testFindOne()
    {
        $this->em->expects($this->once())
                 ->method('find')
                 ->with('Type1Entity', '123');

        $this->store->findOne('type1', '123');
    }

    public function testFind()
    {
        $queryBuilder = $this->getMockBuilder(QueryBuilder::class)->disableOriginalConstructor()->getMock();
        $queryBuilder->expects($this->once())->method('select')->willReturn($queryBuilder);
        $queryBuilder->expects($this->once())->method('from')->willReturn($queryBuilder);

        $this->em->expects($this->once())->method('createQueryBuilder')->willReturn($queryBuilder);

        $params = new ParameterSet($this->schema);

        $this->store->find('type1', $params);
    }

    public function testFindWithSort()
    {
        $queryBuilder = $this->getMockBuilder(QueryBuilder::class)->disableOriginalConstructor()->getMock();
        $queryBuilder->expects($this->once())->method('select')->willReturn($queryBuilder);
        $queryBuilder->expects($this->once())->method('from')->willReturn($queryBuilder);
        $queryBuilder->expects($this->once())->method('addOrderBy')->with('base.myField', 'ASC');

        $this->em->expects($this->once())->method('createQueryBuilder')->willReturn($queryBuilder);

        $params = new ParameterSet($this->schema);
        $params->addSort(new SortParameter('myField'));

        $this->store->find('type1', $params);
    }

    public function testFindWithDefaultSort()
    {
        $queryBuilder = $this->getMockBuilder(QueryBuilder::class)->disableOriginalConstructor()->getMock();
        $queryBuilder->expects($this->once())->method('select')->willReturn($queryBuilder);
        $queryBuilder->expects($this->once())->method('from')->willReturn($queryBuilder);
        $queryBuilder->expects($this->once())->method('addOrderBy')->with('base.createdAt', 'DESC');

        $this->em->expects($this->once())->method('createQueryBuilder')->willReturn($queryBuilder);

        $params = new ParameterSet($this->schema);

        $this->store->find('type1', $params);
    }

    public function testFindWithNoDefaultSort()
    {
        $queryBuilder = $this->getMockBuilder(QueryBuilder::class)->disableOriginalConstructor()->getMock();
        $queryBuilder->expects($this->once())->method('select')->willReturn($queryBuilder);
        $queryBuilder->expects($this->once())->method('from')->willReturn($queryBuilder);
        $queryBuilder->expects($this->once())->method('addOrderBy')->with('base.id');

        $this->em->expects($this->once())->method('createQueryBuilder')->willReturn($queryBuilder);

        $params = new ParameterSet($this->schema);

        $this->store->find('type2', $params);
    }

    public function testFindWithFilter()
    {
        $queryBuilder = $this->getMockBuilder(QueryBuilder::class)->disableOriginalConstructor()->getMock();
        $queryBuilder->expects($this->once())->method('select')->willReturn($queryBuilder);
        $queryBuilder->expects($this->once())->method('from')->willReturn($queryBuilder);
        $queryBuilder->expects($this->once())->method('andWhere')->willReturn($queryBuilder);
        $queryBuilder->expects($this->once())->method('setParameter')->with('foo', 'bar')->willReturn($queryBuilder);

        $this->em->expects($this->once())->method('createQueryBuilder')->willReturn($queryBuilder);

        $params = new ParameterSet($this->schema);
        $params->addFilter('foo', 'bar');

        $this->store->find('type1', $params);
    }

    public function testFindByRelation()
    {
        $queryBuilder = $this->getMockBuilder(QueryBuilder::class)->disableOriginalConstructor()->getMock();
        $queryBuilder->expects($this->once())->method('select')->willReturn($queryBuilder);
        $queryBuilder->expects($this->once())->method('from')->willReturn($queryBuilder);
        $queryBuilder->expects($this->once())->method('join')->willReturn($queryBuilder);
        $queryBuilder->expects($this->once())->method('andWhere')->willReturn($queryBuilder);
        $queryBuilder->expects($this->once())->method('setParameter')->with('id', '123')->willReturn($queryBuilder);

        $meta = $this->getMockBuilder(ClassMetadata::class)->disableOriginalConstructor()->getMock();
        $meta->expects($this->once())->method('getAssociationMapping')->with('parent')->willReturn([
            'isOwningSide' => true,
            'inversedBy'   => 'parent',
        ]);

        $this->em->expects($this->once())->method('createQueryBuilder')->willReturn($queryBuilder);
        $this->em->expects(self::any())->method('getClassMetadata')->willReturn($meta);

        $params = new ParameterSet($this->schema);
        $this->store->findByRelation('type1', '123', 'parent', $params);
    }

    public function testOneByRelation()
    {
        $queryBuilder = $this->getMockBuilder(QueryBuilder::class)->disableOriginalConstructor()->getMock();

        $entity = new \stdClass();
        $query  = $this->getMockBuilder(AbstractQuery::class)->disableOriginalConstructor()->getMock();
        $query->expects($this->once())->method('execute')->willReturn([$entity]);

        $queryBuilder->expects($this->once())->method('select')->willReturn($queryBuilder);
        $queryBuilder->expects($this->once())->method('from')->willReturn($queryBuilder);
        $queryBuilder->expects($this->once())->method('join')->willReturn($queryBuilder);
        $queryBuilder->expects($this->once())->method('andWhere')->willReturn($queryBuilder);
        $queryBuilder->expects($this->once())->method('setParameter')->with('id', '123')->willReturn($queryBuilder);
        $queryBuilder->expects($this->once())->method('getQuery')->willReturn($query);

        $meta = $this->getMockBuilder(ClassMetadata::class)->disableOriginalConstructor()->getMock();
        $meta->expects($this->once())->method('getAssociationMapping')->with('parent')->willReturn([
            'isOwningSide' => true,
            'inversedBy'   => 'parent',
        ]);

        $this->em->expects($this->once())->method('createQueryBuilder')->willReturn($queryBuilder);
        $this->em->expects(self::any())->method('getClassMetadata')->willReturn($meta);

        $result = $this->store->findOneByRelation('type1', '123', 'parent');
        $this->assertEquals($entity, $result);
    }

    public function testFindOneByRelationBuilderMethod()
    {
        $entity = new \stdClass();
        $query  = $this->getMockBuilder(AbstractQuery::class)->disableOriginalConstructor()->getMock();
        $query->expects($this->once())->method('execute')->willReturn([$entity]);

        $queryBuilder = $this->getMockBuilder(QueryBuilder::class)->disableOriginalConstructor()->getMock();
        $queryBuilder->expects($this->once())->method('getQuery')->willReturn($query);

        $this->em->expects($this->once())
                 ->method('getRepository')
                 ->willReturn(new RepoWithBuilderMethod($queryBuilder));

        $result = $this->store->findOneByRelation('type1', '123', 'parent');
        $this->assertEquals($entity, $result);
    }

    public function testDelete()
    {
        $entity = new Category();
        $this->em->expects($this->once())->method('remove')->with($entity);
        $this->em->expects($this->once())->method('flush');

        $this->store->delete($entity);
    }

    public function testSave()
    {
        $entity = new Category();
        $this->em->expects($this->once())->method('persist')->with($entity);
        $this->em->expects($this->once())->method('flush');

        $this->store->save($entity);
    }
}
