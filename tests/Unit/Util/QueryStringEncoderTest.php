<?php

namespace BBCWorldWide\JsonApi\Tests\Unit\Util;

use BBCWorldWide\JsonApi\Util\QueryStringEncoder;

class WithValidEncodeMethod
{
    public function toQueryString()
    {
        return ['foo' => 'bar'];
    }
}

class QueryStringEncoderTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var QueryStringEncoder
     */
    protected $encoder;

    public function setUp()
    {
        parent::setUp();
        $this->encoder = new QueryStringEncoder();
    }

    /**
     * @test
     */
    public function preserveSquareBrackets()
    {
        $val     = [
            'field' => [
                'foo' => 'bar',
            ],
        ];
        $encoded = $this->encoder->encode($val);
        $this->assertEquals('field[foo]=bar', $encoded);
    }

    /**
     * @test
     */
    public function rawEncode()
    {
        $val     = 'foo bar';
        $encoded = $this->encoder->encode($val);
        $this->assertEquals('foo%20bar', $encoded);
    }

    /**
     * @test
     */
    public function object()
    {
        $val     = [
            'field' => (object) [
                'foo' => 'bar',
            ],
        ];
        $encoded = $this->encoder->encode($val);
        $this->assertEquals('field[foo]=bar', $encoded);
    }

    /**
     * @test
     */
    public function prefix()
    {
        $val     = [
            'field' => [
                'foo' => 'bar',
            ],
        ];
        $encoded = $this->encoder->encode($val, 'x');
        $this->assertEquals('xfield[foo]=bar', $encoded);
    }

    /**
     * @test
     */
    public function defaultSeparator()
    {
        $val     = [
            'foo' => 'bar',
            'baz' => 'woo',
        ];
        $sep     = ini_get('arg_separator.output');
        $encoded = $this->encoder->encode($val, null, null);
        $this->assertEquals('foo=bar' . $sep . 'baz=woo', $encoded);
    }

    /**
     * @test
     */
    public function validEncodeMethod()
    {
        $val = [
            'field' => new WithValidEncodeMethod(),
        ];

        $encoded = $this->encoder->encode($val);
        $this->assertEquals('field[foo]=bar', $encoded);
    }
}
