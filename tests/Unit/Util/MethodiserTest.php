<?php

namespace BBCWorldWide\JsonApi\Tests\Unit\Util;

use BBCWorldWide\JsonApi\Util\Methodiser;
use PHPUnit\Framework\TestCase;

class MethodiserTest extends TestCase
{
    /**
     * @test
     * @dataProvider wordsDataProvider
     */
    public function setter(string $name, string $expectedGetter, string $expectedSetter)
    {
        self::assertSame($expectedSetter, Methodiser::setter($name));
    }

    /**
     * @test
     * @dataProvider wordsDataProvider
     */
    public function getter(string $name, string $expectedGetter, string $expectedSetter)
    {
        self::assertSame($expectedGetter, Methodiser::getter($name));
    }

    /**
     * @test
     * @dataProvider wordsDataProvider
     */
    public function adder(string $name, string $expectedGetter, string $expectedSetter, string $expectedAdder)
    {
        self::assertSame($expectedAdder, Methodiser::adder($name));
    }

    /**
     * @test
     * @dataProvider wordsDataProvider
     */
    public function remover(
        string $name,
        string $expectedGetter,
        string $expectedSetter,
        string $expectedAdder,
        string $expectedRemover
    ) {
        self::assertSame($expectedRemover, Methodiser::remover($name));
    }

    public function wordsDataProvider()
    {
        return [
            ['isActive', 'isActive', 'setIsActive', 'addIsActive', 'removeIsActive'],
            ['foo', 'getFoo', 'setFoo', 'addFoo', 'removeFoo'],
            ['bar', 'getBar', 'setBar', 'addBar', 'removeBar'],
            ['hyphenated-foo', 'getHyphenatedFoo', 'setHyphenatedFoo', 'addHyphenatedFoo', 'removeHyphenatedFoo']
        ];
    }
}
