<?php

namespace BBCWorldWide\JsonApi\s\Unit\Util;

use BBCWorldWide\JsonApi\Util\Inflector;
use PHPUnit\Framework\TestCase;

class InflectorTest extends TestCase
{
    /**
     * @test
     */
    public function classify()
    {
        $val    = 'Foo Bar';
        $output = Inflector::classify($val);
        $this->assertEquals('FooBar', $output);
    }

    /**
     * @test
     */
    public function camelize()
    {
        $val    = 'Foo Bar';
        $output = Inflector::camelize($val);
        $this->assertEquals('fooBar', $output);
    }

    /**
     * @test
     */
    public function hyphenateWords()
    {
        $val    = 'Foo Bar';
        $output = Inflector::hyphenate($val);
        $this->assertEquals('foo-bar', $output);
    }

    /**
     * @test
     */
    public function hyphenateCamelCase()
    {
        $val    = 'fooBar';
        $output = Inflector::hyphenate($val);
        $this->assertEquals('foo-bar', $output);
    }

    /**
     * @test
     */
    public function pluralize()
    {
        $val    = 'Pony';
        $output = Inflector::pluralize($val);
        $this->assertEquals('Ponies', $output);
    }

    /**
     * @test
     */
    public function singularize()
    {

        $val    = 'Ponies';
        $output = Inflector::singularize($val);
        $this->assertEquals('Pony', $output);
    }

    /**
     * @test
     */
    public function wordsCamelCase()
    {
        $val    = 'fooBarBaz';
        $output = Inflector::words($val);
        $this->assertEquals('Foo bar baz', $output);
    }

    /**
     * @test
     */
    public function wordsUnderscored()
    {
        $val    = 'foo_bar_baz';
        $output = Inflector::words($val);
        $this->assertEquals('Foo bar baz', $output);
    }
}
