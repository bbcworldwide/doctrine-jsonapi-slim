<?php

use BBCWorldWide\JsonApi\JsonApi\Schema\AttributeDefinition as Attr;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Category;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Programme\AbstractProgramme;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Programme\Brand;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Programme\Episode;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Version;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\VersionType;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Warning;

$schema = [];

// Common attributes:

$coreProgrammeAttributes = [
    'fullName'                     => ['sortable' => true],
    'displayName'                  => [],
    'displayNamePs'                => [],
    'isDisplayNameOverridden'      => ['type' => Attr::TYPE_BOOL],
    'sortName'                     => ['sortable' => true],
    'synopsisShort'                => [],
    'synopsisShortPs'              => [],
    'isSynopsisShortOverridden'    => ['type' => Attr::TYPE_BOOL],
    'isAutoCategorised'            => ['type' => Attr::TYPE_BOOL],
    'synopsisMedium'               => [],
    'synopsisMediumPs'             => [],
    'isSynopsisMediumOverridden'   => ['type' => Attr::TYPE_BOOL],
    'synopsisLong'                 => [],
    'synopsisLongPs'               => [],
    'isSynopsisLongOverridden'     => ['type' => Attr::TYPE_BOOL],
    'picture16x9'                  => [],
    'picture16x9Ps'                => [],
    'isPicture16x9Overridden'      => ['type' => Attr::TYPE_BOOL],
    'picturePoster'                => [],
    'pictureSquare'                => [],
    'pictureSquarePs'              => [],
    'isPictureSquareOverridden'    => ['type' => Attr::TYPE_BOOL],
    'pictureWallpaper'             => [],
    'isHidden'                     => ['type' => Attr::TYPE_BOOL],
    'crid'                         => ['filterable' => true],
    'createdAt'                    => ['sortable' => true, 'readOnly' => true, 'type' => Attr::TYPE_DATETIME],
    'updatedAt'                    => ['sortable' => true, 'readOnly' => true, 'type' => Attr::TYPE_DATETIME],
    'isIngested'                   => ['type' => Attr::TYPE_BOOL],
];

$groupableAttributes = [
    'position'                   => ['sortable' => true, 'type' => Attr::TYPE_INT],
    'positionPs'                 => ['type' => Attr::TYPE_INT],
    'isPositionOverridden'       => ['type' => Attr::TYPE_BOOL],
    'isPrimaryParentOverridden'  => ['type' => Attr::TYPE_BOOL],
];

$episodeAttributes = $groupableAttributes + [
    'coreNumber'                 => [],
    'origination'                => [],
    'contentLanguage'            => [],
    'duration'                   => ['type' => Attr::TYPE_INT],
    'isRevoked'                  => ['type' => Attr::TYPE_BOOL, 'filterable' => true],
    'releaseDate'                => ['type' => Attr::TYPE_DATETIME, 'sortable' => true],
    'releaseDatePs'              => ['type' => Attr::TYPE_DATETIME],
    'isReleaseDateOverridden'    => ['type' => Attr::TYPE_BOOL],
    'isSpecial'                  => ['type' => Attr::TYPE_BOOL],
];

$groupAttributes = [
    'statedItems'                => ['type' => Attr::TYPE_INT],
    'statedItemsPs'              => ['type' => Attr::TYPE_INT],
    'isStatedItemsOverridden'    => ['type' => Attr::TYPE_BOOL],
];


// Common relationships:

// Type groups:
$groups = ['brands'];
$groupables = ['episodes'];

$coreProgrammeRelationships = [
    'categories'             => ['multiple' => true, 'types' => ['categories']],
    'effectiveCategories'    => ['multiple' => true, 'types' => ['categories'], 'readOnly' => true],
];

$groupRelationships = [
    'children'         => ['multiple' => true, 'types' => $groupables, 'readOnly' => true, 'includable' => true],
    'descendants'      => ['multiple' => true, 'types' => $groupables, 'readOnly' => true, 'includable' => false],
];

$groupableRelationships = [
    'tleo'             => ['types' => ['brands'], 'readOnly' => true, 'referenced' => false],
    'primaryParent'    => ['types' => ['brands', 'programmes']],
    // PS value allows base type, avoiding type check to aid ingest.
    'primaryParentPs'  => ['types' => ['programmes']],
    // All groupable types are also purchasable.
    'parents'          => ['multiple' => true, 'types' => $groups, 'readOnly' => true],
    'ancestors'        => ['multiple' => true, 'types' => $groups, 'readOnly' => true],
    'primaryAncestors' => ['multiple' => true, 'types' => $groups, 'readOnly' => true],
];

$episodeRelationships = $groupableRelationships + [
    'versions'                => ['multiple' => true, 'types' => ['versions']],
    'primaryVersion'          => [
        'types'      => ['versions'],
        'readOnly'   => true,
        'referenced' => false,
        'includable' => true
    ],
    'mainVersion'             => ['types' => ['versions'], 'readOnly' => true, 'referenced' => false],
    'mainVersionSd'           => ['types' => ['versions'], 'readOnly' => true, 'referenced' => false],
    'signedVersion'           => ['types' => ['versions'], 'readOnly' => true, 'referenced' => false],
    'signedVersionSd'         => ['types' => ['versions'], 'readOnly' => true, 'referenced' => false],
    'audioDescribedVersion'   => ['types' => ['versions'], 'readOnly' => true, 'referenced' => false],
    'audioDescribedVersionSd' => ['types' => ['versions'], 'readOnly' => true, 'referenced' => false],
];

// Common filters:

$programmeFilters = ['search', 'categories', 'suggest', 'types', 'noProducts', 'tleo'];


// Type definitions:

// Include all attributes and relationships on base type to allow use in generic queries.
$schema['programmes'] = [
    'entityClass'   => AbstractProgramme::class,
    'alias'         => 'Programme',
    'attributes'    => $coreProgrammeAttributes + $groupAttributes + $episodeAttributes,
    'customFilters' => $programmeFilters,
    'relationships' => $coreProgrammeRelationships + $episodeRelationships + $groupRelationships,
    'defaultSort'   => '-createdAt',
];

$schema['brands'] = [
    'entityClass'   => Brand::class,
    'subtypeOf'     => 'programmes',
    'attributes'    => $coreProgrammeAttributes + $groupAttributes,
    'customFilters' => $programmeFilters,
    'relationships' => $coreProgrammeRelationships + $groupRelationships,
    'defaultSort'   => '-createdAt',
];

$schema['episodes'] = [
    'entityClass'   => Episode::class,
    'subtypeOf'     => 'programmes',
    'attributes'    => $coreProgrammeAttributes + $episodeAttributes,
    'customFilters' => $programmeFilters,
    'relationships' => $coreProgrammeRelationships + $episodeRelationships,
    'defaultSort'   => '-createdAt',
];

$schema['categories'] = [
    'entityClass'   => Category::class,
    'attributes'    => [
        'crid'             => ['filterable' => true],
        'name'             => [],
        'position'         => ['type' => Attr::TYPE_INT,      'sortable' => true, 'filterable' => true],
        'createdAt'        => ['type' => Attr::TYPE_DATETIME, 'sortable' => true, 'readOnly' => true],
        'updatedAt'        => ['type' => Attr::TYPE_DATETIME, 'sortable' => true, 'readOnly' => true],
    ],
    'relationships' => [
        'parent'           => ['types' => ['categories']],
        'children'         => ['multiple' => true, 'types' => ['categories'], 'readOnly' => true],
        'programmes'       => ['multiple' => true, 'types' => ['programmes'], 'includable' => false],
    ],
    'defaultSort'   => 'position',
];

$schema['versions'] = [
    'entityClass'   => Version::class,
    'attributes'    => [
        'crid'             => ['filterable' => true],
        'cvCode'           => [],
        'deliveryStart'    => ['type' => Attr::TYPE_DATETIME, 'sortable' => true],
        'deliveryEnd'      => ['type' => Attr::TYPE_DATETIME, 'sortable' => true],
        'salesStart'       => ['type' => Attr::TYPE_DATETIME, 'sortable' => true],
        'salesEnd'         => ['type' => Attr::TYPE_DATETIME, 'sortable' => true],
        'isSigned'         => ['type' => Attr::TYPE_BOOL,     'filterable' => true],
        'isAudioDescribed' => ['type' => Attr::TYPE_BOOL,     'filterable' => true],
        'isPropagated'     => ['type' => Attr::TYPE_BOOL,     'filterable' => true],
        'duration'         => ['type' => Attr::TYPE_INT,      'required' => true],
        'contentLocation'  => ['required' => true],
        'encodingProfile'  => ['required' => true],
        'captureType'      => [],
        'captionLanguage'  => [],
        'aspectRatio'      => [],
        'createdAt'        => ['sortable' => true, 'readOnly' => true, 'type' => Attr::TYPE_DATETIME],
        'updatedAt'        => ['sortable' => true, 'readOnly' => true, 'type' => Attr::TYPE_DATETIME],
    ],
    'relationships' => [
        'versionTypes'    => ['multiple' => true, 'types' => ['versionTypes']],
        'warnings'         => ['multiple' => true, 'types' => ['warnings']],
        'programme'        => ['types' => ['episodes']],
    ],
    'defaultSort'   => '-createdAt',
];

$schema['versionTypes'] = [
    'entityClass'   => VersionType::class,
    'attributes'    => [],
    'relationships' => [
        'versions'         => ['multiple' => true, 'types' => ['versions'], 'includable' => false],
    ],
    'cacheResults'  => true,
];

$schema['warnings'] = [
    'entityClass'   => Warning::class,
    'attributes'    => [
        'warningTextShort' => ['required' => true],
        'warningTextLong'  => ['required' => true],
    ],
    'relationships' => [
        'versions'         => ['multiple' => true, 'types' => ['versions'], 'includable' => false],
    ],
    'cacheResults'  => true,
];


return $schema;
