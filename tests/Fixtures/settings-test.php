<?php

$settings                             = require __DIR__ . '/../../src/settings.php';
$settings['settings']['db']['master'] = [
    'driver' => 'pdo_sqlite',
    'path'   => '/tmp/test-db.sqlite',
];

$settings['cacheFolder'] = '/tmp';

$settings['settings']['env'] = 'test';

$settings['settings']['logger']['logLevel'] = Monolog\Logger::EMERGENCY;

$settings['settings']['updatesQueue']['type'] = 'null queue';

$settings['settings']['cacheResponses'] = false;

$settings['settings']['cacheDriver'] = 'array';

return $settings;
