<?php
return [
    'programmes'    => [
        'entityClass'   => 'BBCWorldWide\\JsonApi\\Tests\\Fixtures\\Entity\\Programme\\AbstractProgramme',
        'alias'         => 'Programme',
        'attributes'    => [
            'fullName'                   => ['sortable' => true],
            'displayName'                => [],
            'displayNamePs'              => [],
            'isDisplayNameOverridden'    => ['type' => 'bool'],
            'sortName'                   => [
                'sortable' => true,
            ],
            'synopsisShort'              => [],
            'synopsisShortPs'            => [],
            'isSynopsisShortOverridden'  => ['type' => 'bool'],
            'isAutoCategorised'          => ['type' => 'bool'],
            'synopsisMedium'             => [],
            'synopsisMediumPs'           => [],
            'isSynopsisMediumOverridden' => ['type' => 'bool'],
            'synopsisLong'               => [],
            'synopsisLongPs'             => [],
            'isSynopsisLongOverridden'   => ['type' => 'bool'],
            'picture16x9'                => [],
            'picture16x9Ps'              => [],
            'isPicture16x9Overridden'    => ['type' => 'bool'],
            'picturePoster'              => [],
            'pictureSquare'              => [],
            'pictureSquarePs'            => [],
            'isPictureSquareOverridden'  => ['type' => 'bool'],
            'pictureWallpaper'           => [],
            'isHidden'                   => ['type' => 'bool'],
            'crid'                       => ['filterable' => true],
            'createdAt'                  => [
                'sortable' => true,
                'readOnly' => true,
                'type'     => 'datetime',
            ],
            'updatedAt'                  => [
                'sortable' => true,
                'readOnly' => true,
                'type'     => 'datetime',
            ],
            'isIngested'                 => ['type' => 'bool'],
            'statedItems'                => ['type' => 'int'],
            'statedItemsPs'              => ['type' => 'int'],
            'isStatedItemsOverridden'    => ['type' => 'bool'],
            'position'                   => [
                'sortable' => true,
                'type'     => 'int',
            ],
            'positionPs'                 => ['type' => 'int'],
            'isPositionOverridden'       => ['type' => 'bool'],
            'isPrimaryParentOverridden'  => ['type' => 'bool'],
            'coreNumber'                 => [],
            'origination'                => [],
            'contentLanguage'            => [],
            'duration'                   => ['type' => 'int'],
            'isRevoked'                  => [
                'type'       => 'bool',
                'filterable' => true,
            ],
            'releaseDate'                => [
                'type'     => 'datetime',
                'sortable' => true,
            ],
            'releaseDatePs'              => [
                'type' => 'datetime',
            ],
            'isReleaseDateOverridden'    => ['type' => 'bool'],
            'isSpecial'                  => ['type' => 'bool'],
        ],
        'customFilters' => [
            'search',
            'categories',
            'suggest',
            'types',
            'noProducts',
            'tleo',
        ],
        'relationships' => [
            'categories'              => [
                'multiple' => true,
                'types'    => ['categories'],
            ],
            'effectiveCategories'     => [
                'multiple' => true,
                'types'    => ['categories'],
                'readOnly' => true,
            ],
            'tleo'                    => [
                'types'      => ['brands'],
                'readOnly'   => true,
                'referenced' => false,
            ],
            'primaryParent'           => [
                'types' =>
                    [
                        'brands',
                        'programmes',
                    ],
            ],
            'primaryParentPs'         => [
                'types' => ['programmes'],
            ],
            'parents'                 => [
                'multiple' => true,
                'types'    => ['brands'],
                'readOnly' => true,
            ],
            'ancestors'               => [
                'multiple' => true,
                'types'    => ['brands'],
                'readOnly' => true,
            ],
            'primaryAncestors'        => [
                'multiple' => true,
                'types'    => ['brands'],
                'readOnly' => true,
            ],
            'versions'                => [
                'multiple' => true,
                'types'    => ['versions'],
            ],
            'primaryVersion'          => [
                'types'      => ['versions'],
                'readOnly'   => true,
                'referenced' => false,
                'includable' => true,
            ],
            'mainVersion'             => [
                'types'      => ['versions'],
                'readOnly'   => true,
                'referenced' => false,
            ],
            'mainVersionSd'           => [
                'types'      => ['versions'],
                'readOnly'   => true,
                'referenced' => false,
            ],
            'signedVersion'           => [
                'types'      => ['versions'],
                'readOnly'   => true,
                'referenced' => false,
            ],
            'signedVersionSd'         => [
                'types'      => ['versions'],
                'readOnly'   => true,
                'referenced' => false,
            ],
            'audioDescribedVersion'   => [
                'types'      => ['versions'],
                'readOnly'   => true,
                'referenced' => false,
            ],
            'audioDescribedVersionSd' => [
                'types'      => ['versions'],
                'readOnly'   => true,
                'referenced' => false,
            ],
            'children'                => [
                'multiple'   => true,
                'types'      => ['episodes'],
                'readOnly'   => true,
                'includable' => true,
            ],
            'descendants'             => [
                'multiple'   => true,
                'types'      => ['episodes'],
                'readOnly'   => true,
                'includable' => false,
            ],
        ],
        'defaultSort'   => '-createdAt',
    ],
    'brands'        => [
        'entityClass'   => 'BBCWorldWide\\JsonApi\\Tests\\Fixtures\\Entity\\Programme\\Brand',
        'subtypeOf'     => 'programmes',
        'attributes'    => [
            'fullName'                   => [
                'sortable' => true,
            ],
            'displayName'                => [],
            'displayNamePs'              => [],
            'isDisplayNameOverridden'    => ['type' => 'bool'],
            'sortName'                   => [
                'sortable' => true,
            ],
            'synopsisShort'              => [],
            'synopsisShortPs'            => [],
            'isSynopsisShortOverridden'  => ['type' => 'bool'],
            'isAutoCategorised'          => ['type' => 'bool'],
            'synopsisMedium'             => [],
            'synopsisMediumPs'           => [],
            'isSynopsisMediumOverridden' => ['type' => 'bool'],
            'synopsisLong'               => [],
            'synopsisLongPs'             => [],
            'isSynopsisLongOverridden'   => ['type' => 'bool'],
            'picture16x9'                => [],
            'picture16x9Ps'              => [],
            'isPicture16x9Overridden'    => ['type' => 'bool'],
            'picturePoster'              => [],
            'pictureSquare'              => [],
            'pictureSquarePs'            => [],
            'isPictureSquareOverridden'  => ['type' => 'bool'],
            'pictureWallpaper'           => [],
            'isHidden'                   => ['type' => 'bool'],
            'crid'                       => ['filterable' => true],
            'createdAt'                  => [
                'sortable' => true,
                'readOnly' => true,
                'type'     => 'datetime',
            ],
            'updatedAt'                  => [
                'sortable' => true,
                'readOnly' => true,
                'type'     => 'datetime',
            ],
            'isIngested'                 => ['type' => 'bool'],
            'statedItems'                => ['type' => 'int'],
            'statedItemsPs'              => ['type' => 'int'],
            'isStatedItemsOverridden'    => ['type' => 'bool'],
        ],
        'customFilters' => [
            'search',
            'categories',
            'suggest',
            'types',
            'noProducts',
            'tleo',
        ],
        'relationships' => [
            'categories'          => [
                'multiple' => true,
                'types'    => ['categories'],
            ],
            'effectiveCategories' => [
                'multiple' => true,
                'types'    => ['categories'],
                'readOnly' => true,
            ],
            'children'            => [
                'multiple'   => true,
                'types'      => ['episodes'],
                'readOnly'   => true,
                'includable' => true,
            ],
            'descendants'         => [
                'multiple'   => true,
                'types'      => ['episodes'],
                'readOnly'   => true,
                'includable' => false,
            ],
        ],
        'defaultSort'   => '-createdAt',
    ],
    'episodes'      => [
        'entityClass'   => 'BBCWorldWide\\JsonApi\\Tests\\Fixtures\\Entity\\Programme\\Episode',
        'subtypeOf'     => 'programmes',
        'attributes'    => [
            'fullName'                   => [
                'sortable' => true,
            ],
            'displayName'                => [],
            'displayNamePs'              => [],
            'isDisplayNameOverridden'    => ['type' => 'bool'],
            'sortName'                   => [
                'sortable' => true,
            ],
            'synopsisShort'              => [],
            'synopsisShortPs'            => [],
            'isSynopsisShortOverridden'  => ['type' => 'bool'],
            'isAutoCategorised'          => ['type' => 'bool'],
            'synopsisMedium'             => [],
            'synopsisMediumPs'           => [],
            'isSynopsisMediumOverridden' => ['type' => 'bool'],
            'synopsisLong'               => [],
            'synopsisLongPs'             => [],
            'isSynopsisLongOverridden'   => ['type' => 'bool'],
            'picture16x9'                => [],
            'picture16x9Ps'              => [],
            'isPicture16x9Overridden'    => ['type' => 'bool'],
            'picturePoster'              => [],
            'pictureSquare'              => [],
            'pictureSquarePs'            => [],
            'isPictureSquareOverridden'  => ['type' => 'bool'],
            'pictureWallpaper'           => [],
            'isHidden'                   => ['type' => 'bool'],
            'crid'                       => ['filterable' => true],
            'createdAt'                  => [
                'sortable' => true,
                'readOnly' => true,
                'type'     => 'datetime',
            ],
            'updatedAt'                  => [
                'sortable' => true,
                'readOnly' => true,
                'type'     => 'datetime',
            ],
            'isIngested'                 => ['type' => 'bool'],
            'position'                   => [
                'sortable' => true,
                'type'     => 'int',
            ],
            'positionPs'                 => ['type' => 'int'],
            'isPositionOverridden'       => ['type' => 'bool'],
            'isPrimaryParentOverridden'  => ['type' => 'bool'],
            'coreNumber'                 => [],
            'origination'                => [],
            'contentLanguage'            => [],
            'duration'                   => ['type' => 'int'],
            'isRevoked'                  => [
                'type'       => 'bool',
                'filterable' => true,
            ],
            'releaseDate'                => [
                'type'     => 'datetime',
                'sortable' => true,
            ],
            'releaseDatePs'              => [
                'type' => 'datetime',
            ],
            'isReleaseDateOverridden'    => ['type' => 'bool'],
            'isSpecial'                  => ['type' => 'bool'],
        ],
        'customFilters' => [
            'search',
            'categories',
            'suggest',
            'types',
            'noProducts',
            'tleo',
        ],
        'relationships' => [
            'categories'              => [
                'multiple' => true,
                'types'    => ['categories'],
            ],
            'effectiveCategories'     => [
                'multiple' => true,
                'types'    => ['categories'],
                'readOnly' => true,
            ],
            'tleo'                    => [
                'types'      => ['brands'],
                'readOnly'   => true,
                'referenced' => false,
            ],
            'primaryParent'           => [
                'types' => [
                    'brands',
                    'programmes',
                ],
            ],
            'primaryParentPs'         => [
                'types' => ['programmes'],
            ],
            'parents'                 => [
                'multiple' => true,
                'types'    => ['brands'],
                'readOnly' => true,
            ],
            'ancestors'               => [
                'multiple' => true,
                'types'    => ['brands'],
                'readOnly' => true,
            ],
            'primaryAncestors'        => [
                'multiple' => true,
                'types'    => ['brands'],
                'readOnly' => true,
            ],
            'versions'                => [
                'multiple' => true,
                'types'    => ['versions'],
            ],
            'primaryVersion'          => [
                'types'      => ['versions'],
                'readOnly'   => true,
                'referenced' => false,
                'includable' => true,
            ],
            'mainVersion'             => [
                'types'      => ['versions'],
                'readOnly'   => true,
                'referenced' => false,
            ],
            'mainVersionSd'           => [
                'types'      => ['versions'],
                'readOnly'   => true,
                'referenced' => false,
            ],
            'signedVersion'           => [
                'types'      => ['versions'],
                'readOnly'   => true,
                'referenced' => false,
            ],
            'signedVersionSd'         => [
                'types'      => ['versions'],
                'readOnly'   => true,
                'referenced' => false,
            ],
            'audioDescribedVersion'   => [
                'types'      => ['versions'],
                'readOnly'   => true,
                'referenced' => false,
            ],
            'audioDescribedVersionSd' => [
                'types'      => ['versions'],
                'readOnly'   => true,
                'referenced' => false,
            ],
        ],
        'defaultSort'   => '-createdAt',
    ],
    'categories'    => [
        'entityClass'   => 'BBCWorldWide\\JsonApi\\Tests\\Fixtures\\Entity\\Category',
        'attributes'    => [
            'crid'      => ['filterable' => true],
            'name'      => [],
            'position'  => [
                'type'       => 'int',
                'sortable'   => true,
                'filterable' => true,
            ],
            'createdAt' => [
                'type'     => 'datetime',
                'sortable' => true,
                'readOnly' => true,
            ],
            'updatedAt' => [
                'type'     => 'datetime',
                'sortable' => true,
                'readOnly' => true,
            ],
        ],
        'relationships' => [
            'parent'     => [
                'types' => ['categories'],
            ],
            'children'   => [
                'multiple' => true,
                'types'    => ['categories'],
                'readOnly' => true,
            ],
            'programmes' => [
                'multiple'   => true,
                'types'      => ['programmes'],
                'includable' => false,
            ],
        ],
        'defaultSort'   => 'position',
    ],
    'versions'      => [
        'entityClass'   => 'BBCWorldWide\\JsonApi\\Tests\\Fixtures\\Entity\\Version',
        'attributes'    => [
            'crid'             => ['filterable' => true],
            'cvCode'           => [],
            'deliveryStart'    => [
                'type'     => 'datetime',
                'sortable' => true,
            ],
            'deliveryEnd'      => [
                'type'     => 'datetime',
                'sortable' => true,
            ],
            'salesStart'       => [
                'type'     => 'datetime',
                'sortable' => true,
            ],
            'salesEnd'         => [
                'type'     => 'datetime',
                'sortable' => true,
            ],
            'isSigned'         => [
                'type'       => 'bool',
                'filterable' => true,
            ],
            'isAudioDescribed' => [
                'type'       => 'bool',
                'filterable' => true,
            ],
            'isPropagated'     => [
                'type'       => 'bool',
                'filterable' => true,
            ],
            'duration'         => [
                'type'     => 'int',
                'required' => true,
            ],
            'contentLocation'  => [
                'required' => true,
            ],
            'encodingProfile'  => [
                'required' => true,
            ],
            'captureType'      => [],
            'captionLanguage'  => [],
            'aspectRatio'      => [],
            'createdAt'        => [
                'sortable' => true,
                'readOnly' => true,
                'type'     => 'datetime',
            ],
            'updatedAt'        => [
                'sortable' => true,
                'readOnly' => true,
                'type'     => 'datetime',
            ],
        ],
        'relationships' => [
            'versionTypes' => [
                'multiple' => true,
                'types'    =>
                    [
                        'versionTypes',
                    ],
            ],
            'warnings'      => [
                'multiple' => true,
                'types'    =>
                    [
                        'warnings',
                    ],
            ],
            'programme'     => [
                'types' => ['episodes'],
            ],
        ],
        'defaultSort'   => '-createdAt',
    ],
    'versionTypes' => [
        'entityClass'   => 'BBCWorldWide\\JsonApi\\Tests\\Fixtures\\Entity\\VersionType',
        'attributes'    => [
        ],
        'relationships' => [
            'versions' => [
                'multiple'   => true,
                'types'      => ['versions'],
                'includable' => false,
            ],
        ],
        'cacheResults'  => true,
    ],
    'warnings'      => [
        'entityClass'   => 'BBCWorldWide\\JsonApi\\Tests\\Fixtures\\Entity\\Warning',
        'attributes'    => [
            'warningTextShort' => [
                'required' => true,
            ],
            'warningTextLong'  => [
                'required' => true,
            ],
        ],
        'relationships' => [
            'versions' => [
                'multiple'   => true,
                'types'      => ['versions'],
                'includable' => false,
            ],
        ],
        'cacheResults'  => true,
    ],
];
