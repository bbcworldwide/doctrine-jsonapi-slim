<?php

namespace BBCWorldWide\JsonApi\Tests\Fixtures;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\SchemaTool;
use Fidry\AliceDataFixtures\Bridge\Doctrine\Persister\ObjectManagerPersister;
use Fidry\AliceDataFixtures\Loader\MultiPassLoader;
use Fidry\AliceDataFixtures\Loader\PersisterLoader;
use Nelmio\Alice\Loader\NativeLoader;

/**
 * Loads data fixtures from YAML files with caching to reduce setup time.
 */
class DataLoader
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var string
     */
    protected $fixturesDir;

    /**
     * @var string
     */
    protected $dataDir;

    /**
     * @var array Default files to load
     */
    protected $defaultFiles;

    /**
     * @var string Path of active db file
     */
    protected $dbPath;

    /**
     * FixtureLoader constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param string $dataDir       Directory SQLite data files
     */
    public function __construct(EntityManagerInterface $entityManager, string $dataDir)
    {
        if (!is_dir($dataDir)) {
            throw new \InvalidArgumentException("Data directory '$dataDir' not found.");
        }

        $this->entityManager = $entityManager;
        $this->dataDir = $dataDir;
        $this->dbPath = $this->dataDir . '/test-db.sqlite';
        $this->fixturesDir = __DIR__ . '/data';

        // Default to all files.
        $defaultFiles = array_diff(scandir($this->fixturesDir . '/standard'), ['..', '.']);

        $this->defaultFiles = array_map(function ($fileName) {
            return 'standard/' . $fileName;
        }, $defaultFiles);
    }

    /**
     * Load fixtures from files or cached data.
     *
     * @param array|null $fixtureFiles Filenames of YAML fixtures to load (defaults to all)
     */
    public function load($fixtureFiles = null)
    {
        if (!$fixtureFiles) {
            $fixtureFiles = $this->defaultFiles;
        }

        // Map to full file paths
        $fixtureFiles = array_map(function ($file) {
            return $this->fixturesDir . '/' . $file;
        }, $fixtureFiles);

        $checksum = $this->getChecksum($fixtureFiles);

        // Load cached DB file if exists:
        if ($this->restoreSnapshot($checksum)) {
            return;
        }

        $this->createSchema();
        $this->import($fixtureFiles);
        $this->createSnapshot($checksum);
    }

    /**
     * Create cached backup
     *
     * @param string $name      Snapshot name/hash
     */
    private function createSnapshot($name)
    {
        $dbBackupPath = $this->dataDir . '/' . $name . '.sqlite';
        copy($this->dbPath, $dbBackupPath);
    }

    /**
     * Load cached DB file if exists
     *
     * @param string $name      Snapshot name/hash
     * @return bool             Load success
     */
    private function restoreSnapshot($name)
    {
        $dbBackupPath = $this->dataDir . '/' . $name . '.sqlite';

        if (file_exists($dbBackupPath)) {
            copy($dbBackupPath, $this->dbPath);
            return true;
        }
        return false;
    }

    /**
     * Purge fixtures data
     */
    public function purge()
    {
        $this->createSchema();
    }

    /**
     * Generate hash of current db schema.
     *
     * @return string
     */
    protected function getSchemaHash()
    {
        $metadata = $this->entityManager->getMetadataFactory()->getAllMetadata();
        return md5(serialize($metadata));
    }

    /**
     * (Re)create database schema
     *
     * @throws \Doctrine\ORM\Tools\ToolsException
     */
    protected function createSchema()
    {
        $checksum = $this->getSchemaHash();
        if ($this->restoreSnapshot($checksum)) {
            return;
        }

        $metadata = $this->entityManager->getMetadataFactory()->getAllMetadata();
        $tool = new SchemaTool($this->entityManager);
        $tool->dropSchema($metadata);
        $tool->createSchema($metadata);

        $this->createSnapshot($checksum);
    }

    /**
     * Import fixtures from files.
     *
     * @param $fixtureFiles
     */
    protected function import($fixtureFiles)
    {
        $fileLoader = new NativeLoader();
        $decoratedLoader = new MultiPassLoader($fileLoader);
        $persister = new ObjectManagerPersister($this->entityManager);

        $loader = new PersisterLoader($decoratedLoader, $persister, []);
        $loader->load($fixtureFiles);
    }

    /**
     * Calculate a unique checksum for a specific list of fixtures to use as cache key.
     *
     * @param array $fixtureFiles   Full filenames of YAML to load
     * @return string               Checksum
     *
     * @throws \InvalidArgumentException Missing file
     */
    protected function getChecksum(array $fixtureFiles)
    {
        $checksum = $this->getSchemaHash();

        foreach ($fixtureFiles as $file) {
            if (!file_exists($file)) {
                throw new \InvalidArgumentException("Fixture file '$file'' not found.");
            }
            $checksum .= md5_file($file);
        }
        return md5($checksum);
    }
}
