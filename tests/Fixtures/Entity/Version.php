<?php
namespace BBCWorldWide\JsonApi\Tests\Fixtures\Entity;

use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Interfaces\CridInterface;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Programme\AbstractProgramme;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Programme\Episode;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Traits\TimestampableTrait;
use BBCWorldWide\JsonApi\JsonApi\EntityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection as DoctrineCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Index;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;

/**
 * Version
 *
 * @Entity
 * @Table(indexes={
 *     @Index(name="version_crid", columns={"crid"}),
 *     @Index(name="version_delivery", columns={"delivery_start", "delivery_end"}),
 *     @Index(name="version_sales", columns={"sales_start", "sales_end"}),
 *     @Index(name="version_encoding_profile", columns={"encoding_profile"}),
 *     @Index(name="version_created", columns={"created_at"}),
 *     @Index(name="version_updated", columns={"updated_at"})
 * })
 */
class Version implements CridInterface, EntityInterface
{
    const ENCODING_PROFILE_HD = 'HD';

    const ENCODING_PROFILE_SD = 'SD';

    /**
     * Get ID - Must be outside trait for Doctrine proxies to work.
     *
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Bring in primary key property.
     */
    use Traits\StringIdTrait;

    /**
     * Bring in CRID property
     */
    use Traits\CridTrait;

    /**
     * Bring in timestampable behaviour
     */
    use TimestampableTrait;

    /**
     * @var string
     * @Column(type="string", length=16, nullable=TRUE)
     */
    protected $cvCode;

    /**
     * @var \DateTime
     * @Column(type="datetime", nullable=TRUE)
     */
    protected $deliveryStart;

    /**
     * @var \DateTime
     * @Column(type="datetime", nullable=TRUE)
     */
    protected $deliveryEnd;

    /**
     * @var \DateTime
     * @Column(type="datetime", nullable=TRUE)
     */
    protected $salesStart;

    /**
     * @var \DateTime
     * @Column(type="datetime", nullable=TRUE)
     */
    protected $salesEnd;

    /**
     * @var bool
     * @Column(type="boolean", nullable=TRUE)
     */
    protected $isSigned;

    /**
     * @var bool
     * @Column(type="boolean", nullable=TRUE)
     */
    protected $isAudioDescribed;

    /**
     * @var bool
     * @Column(type="boolean", nullable=TRUE)
     */
    protected $isPropagated;

    /**
     * @var int
     * @Column(type="smallint", options={"unsigned":true})
     */
    protected $duration;

    /**
     * @Column(type="string", length=255)
     */
    protected $contentLocation;

    /**
     * @Column(type="string", length=32, nullable=TRUE)
     */
    protected $encodingProfile;

    /**
     * @Column(type="string", length=255, nullable=TRUE)
     */
    protected $captureType;

    /**
     * @Column(type="string", length=8, nullable=TRUE)
     */
    protected $captionLanguage;

    /**
     * @Column(type="string", length=32, nullable=TRUE)
     */
    protected $aspectRatio;

    /**
     * @var DoctrineCollection|Warning[]
     * @ManyToMany(targetEntity="Warning", inversedBy="versions")
     * @JoinTable(name="version_warning")
     */
    protected $warnings;

    /**
     * @var DoctrineCollection|VersionType[]
     * @ManyToMany(targetEntity="VersionType", inversedBy="versions", cascade={"persist"})
     * @JoinTable(name="version_version_type")
     */
    protected $versionTypes;

    /**
     * @var Episode
     * @ManyToOne(targetEntity="\BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Programme\Episode", inversedBy="versions")
     * @JoinColumn(onDelete="CASCADE")
     */
    protected $programme;

    /**
     * @var array Lazy loaded mapping file.
     */
    protected static $warningsMap;

    /**
     * @var string Concatenated list of Warning IDs
     */
    protected $warningIds;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->warnings     = new ArrayCollection();
        $this->versionTypes = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getCvCode()
    {
        return $this->cvCode;
    }

    /**
     * @param string $cvCode
     *
     * @return self
     */
    public function setCvCode($cvCode) : self
    {
        $this->cvCode = $cvCode;

        return $this;
    }

    /**
     * Set deliveryStart
     *
     * @param \DateTime|string|null $deliveryStart
     *
     * @return self
     */
    public function setDeliveryStart($deliveryStart = null) : self
    {
        $this->deliveryStart = is_string($deliveryStart) ? new \DateTime($deliveryStart) : $deliveryStart;

        return $this;
    }

    /**
     * Get deliveryStart
     *
     * @return \DateTime
     */
    public function getDeliveryStart()
    {
        return $this->deliveryStart;
    }

    /**
     * Set deliveryEnd
     *
     * @param \DateTime|string|null $deliveryEnd
     *
     * @return self
     */
    public function setDeliveryEnd($deliveryEnd = null) : self
    {
        $this->deliveryEnd = is_string($deliveryEnd) ? new \DateTime($deliveryEnd) : $deliveryEnd;

        return $this;
    }

    /**
     * Get deliveryEnd
     *
     * @return \DateTime
     */
    public function getDeliveryEnd()
    {
        return $this->deliveryEnd;
    }

    /**
     * Set salesStart
     *
     * @param \DateTime|string|null $salesStart
     *
     * @return self
     */
    public function setSalesStart($salesStart) : self
    {
        $this->salesStart = is_string($salesStart) ? new \DateTime($salesStart) : $salesStart;

        return $this;
    }

    /**
     * Get salesStart
     *
     * @return \DateTime
     */
    public function getSalesStart()
    {
        return $this->salesStart;
    }

    /**
     * Set salesEnd
     *
     * @param \DateTime|string|null $salesEnd
     *
     * @return self
     */
    public function setSalesEnd($salesEnd) : self
    {
        $this->salesEnd = is_string($salesEnd) ? new \DateTime($salesEnd) : $salesEnd;

        return $this;
    }

    /**
     * Get salesEnd
     *
     * @return \DateTime
     */
    public function getSalesEnd()
    {
        return $this->salesEnd;
    }

    /**
     * Set isSigned
     *
     * @param boolean $isSigned
     *
     * @return self
     */
    public function setIsSigned(bool $isSigned) : self
    {
        $this->isSigned = $isSigned;

        return $this;
    }

    /**
     * Get isSigned
     *
     * @return boolean
     */
    public function isSigned()
    {
        return $this->isSigned;
    }

    /**
     * Set isAudioDescribed
     *
     * @param boolean $isAudioDescribed
     *
     * @return self
     */
    public function setIsAudioDescribed(bool $isAudioDescribed) : self
    {
        $this->isAudioDescribed = $isAudioDescribed;

        return $this;
    }

    /**
     * Get isAudioDescribed
     *
     * @return boolean
     */
    public function isAudioDescribed()
    {
        return $this->isAudioDescribed;
    }

    /**
     * Set isPropagated
     *
     * @param boolean $isPropagated
     *
     * @return self
     */
    public function setIsPropagated(bool $isPropagated) : self
    {
        $this->isPropagated = $isPropagated;

        return $this;
    }

    /**
     * Get isPropagated
     *
     * @return boolean
     */
    public function isPropagated()
    {
        return $this->isPropagated;
    }

    /**
     * Set duration
     *
     * @param int $duration
     *
     * @return self
     */
    public function setDuration(int $duration) : self
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return int
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set programUrl
     *
     * @param string $contentLocation
     *
     * @return self
     */
    public function setContentLocation(string $contentLocation) : self
    {
        $this->contentLocation = $contentLocation;

        return $this;
    }

    /**
     * Get programUrl
     *
     * @return string
     */
    public function getContentLocation()
    {
        return $this->contentLocation;
    }

    /**
     * Set encodingProfile
     *
     * @param string $encodingProfile
     *
     * @return self
     */
    public function setEncodingProfile(string $encodingProfile) : self
    {
        $this->encodingProfile = $encodingProfile;

        return $this;
    }

    /**
     * Get encodingProfile
     *
     * @return string
     */
    public function getEncodingProfile()
    {
        return $this->encodingProfile;
    }

    /**
     * Set captureType
     *
     * @param string $captureType
     *
     * @return self
     */
    public function setCaptureType(string $captureType) : self
    {
        $this->captureType = $captureType;

        return $this;
    }

    /**
     * Get captureType
     *
     * @return string
     */
    public function getCaptureType()
    {
        return $this->captureType;
    }

    /**
     * Add warning
     *
     * @param Warning $warning
     *
     * @return self
     */
    public function addWarning(Warning $warning) : self
    {
        if ($this->warnings->contains($warning) === false) {
            $this->warnings[] = $warning;
        }

        return $this;
    }

    /**
     * Remove warning
     *
     * @param Warning $warning
     *
     * @return self
     */
    public function removeWarning(Warning $warning) : self
    {
        if ($this->warnings->contains($warning) === true) {
            $this->warnings->removeElement($warning);
        }

        return $this;
    }

    /**
     * Get warnings
     *
     * @return DoctrineCollection|Warning[]
     */
    public function getWarnings()
    {
        return $this->warnings;
    }

    /**
     * Add versionType
     *
     * @param VersionType $versionType
     *
     * @return self
     */
    public function addVersionType(VersionType $versionType) : self
    {
        if ($this->versionTypes->contains($versionType) === false) {
            $this->versionTypes[] = $versionType;
        }

        return $this;
    }

    /**
     * Remove versionType
     *
     * @param VersionType $versionType
     *
     * @return self
     */
    public function removeVersionType(VersionType $versionType) : self
    {
        if ($this->versionTypes->contains($versionType) === true) {
            $this->versionTypes->removeElement($versionType);
        }

        return $this;
    }

    /**
     * Get versionTypes
     *
     * @return DoctrineCollection|VersionType[]
     */
    public function getVersionTypes()
    {
        return $this->versionTypes;
    }

    /**
     * Set programme
     *
     * @param Episode $programme
     *
     * @return self
     */
    public function setProgramme(Episode $programme = null) : self
    {
        if ($this->programme !== $programme) {
            if ($this->programme) {
                $this->programme->removeVersion($this);
            }
            $this->programme = $programme;
            if ($programme) {
                $programme->addVersion($this);
            }
        }

        return $this;
    }

    /**
     * Get programme
     *
     * @return AbstractProgramme
     */
    public function getProgramme()
    {
        return $this->programme;
    }

    /**
     * Set captionLanguage
     *
     * @param string $captionLanguage
     *
     * @return self
     */
    public function setCaptionLanguage(string $captionLanguage) : self
    {
        $this->captionLanguage = $captionLanguage;

        return $this;
    }

    /**
     * Get captionLanguage
     *
     * @return string
     */
    public function getCaptionLanguage()
    {
        return $this->captionLanguage;
    }

    /**
     * @return string
     */
    public function getAspectRatio()
    {
        return $this->aspectRatio;
    }

    /**
     * @param string $aspectRatio
     *
     * @return self
     */
    public function setAspectRatio($aspectRatio)
    {
        $this->aspectRatio = $aspectRatio;

        return $this;
    }


    /**
     * Check whether this version has a specific version type.
     *
     * @param string $typeId    Version Type ID to check for. Not case sensitive.
     *
     * @return bool             Version has this type
     */
    public function hasType($typeId)
    {
        $typeId = strtolower($typeId);
        foreach ($this->getVersionTypes() as $type) {
            if (strtolower($type->getId()) == $typeId) {
                return true;
            }
        }
        return false;
    }
}
