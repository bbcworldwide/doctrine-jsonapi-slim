<?php

namespace BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Programme;

use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Interfaces\GroupableInterface;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Traits\GroupableTrait;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Version;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;

/**
 * @Entity()
 */
class Episode extends AbstractProgramme implements GroupableInterface
{
    /**
     * Implement GroupableInterface
     */
    use GroupableTrait;

    /**
     * @var bool
     * @Column(type="boolean", nullable=true)
     */
    protected $isRevoked = false;

    /**
     * Duration in seconds.
     *
     * @Column(name="duration", type="smallint", options={"unsigned":true})
     */
    protected $duration = 0;

    /**
     * @var \DateTime
     * @Column(type="datetime", nullable=TRUE)
     */
    protected $releaseDate;

    /**
     * @var \DateTime
     * @Column(type="datetime", nullable=TRUE)
     */
    protected $releaseDatePs;

    /**
     * @var bool
     * @Column(type="boolean")
     */
    protected $isReleaseDateOverridden = false;

    /**
     * @var string
     * @Column(type="string", length=16, nullable=TRUE)
     */
    protected $coreNumber;

    /**
     * @var string
     * @Column(type="string", length=64, nullable=TRUE)
     */
    protected $origination;

    /**
     * @var string
     * @Column(type="string", length=2, nullable=TRUE)
     */
    protected $contentLanguage;

    /**
     * @var bool Is a special episode - used to manage display
     *
     * @Column(type="boolean", nullable=TRUE)
     */
    protected $isSpecial = false;

    // Version type priorities:
    protected $mainPriorities           = [
        'StoreOnly',
        'Legal',
        'TechnicalReplacement',
        'Original',
        'Editorial',
        'iPlayerVersion',
        'Lengthened',
        'WarningsHigher',
        'WarningLower',
        'WarningsNone',
        'OpenSubtitled',
        'PostWatershed',
        'Shortened',
        'PreWatershed',
        'Other',
        'Unknown',
        'Duplication',
    ];
    protected $audioDescribedPriorities = ['AudioDescribed', 'DubbedAudioDescribed'];
    protected $signedPriorities         = ['Signed'];

    /**
     * Episode constructor.
     */
    public function __construct()
    {
        $this->versions = new ArrayCollection();
        parent::__construct();
    }

    /**
     * Returns whether the item has been hard-revoked.
     *
     * @return bool
     */
    public function isRevoked(): bool
    {
        return $this->isRevoked;
    }

    /**
     * @param bool $value
     *
     * @return self
     */
    public function setIsRevoked(bool $value): self
    {
        $this->isRevoked = $value;

        return $this;
    }

    /**
     * Get duration
     *
     * @return int
     */
    public function getDuration(): int
    {
        return $this->duration;
    }

    /**
     * Set duration
     *
     * @param int $duration
     *
     * @return self
     */
    public function setDuration(int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get releaseDate
     *
     * @return \DateTime
     */
    public function getReleaseDate()
    {
        return $this->releaseDate;
    }

    /**
     * Set releaseDate
     *
     * @param \DateTime|string|null $releaseDate
     *
     * @return self
     */
    public function setReleaseDate($releaseDate): self
    {
        $this->releaseDate = is_string($releaseDate) ? new \DateTime($releaseDate) : $releaseDate;

        return $this;
    }

    /**
     * Get releaseDatePs
     *
     * @return \DateTime
     */
    public function getReleaseDatePs()
    {
        return $this->releaseDatePs;
    }

    /**
     * Set releaseDatePs
     *
     * @param \DateTime|string|null $releaseDate
     *
     * @return self
     */
    public function setReleaseDatePs($releaseDatePs): self
    {
        if (is_string($releaseDatePs) && !empty($releaseDatePs)) {
            $releaseDatePs = new \DateTime($releaseDatePs);
        }
        $this->releaseDatePs = $releaseDatePs;

        return $this;
    }

    /**
     * Get isReleaseDateOverridden
     *
     * @return bool
     */
    public function isReleaseDateOverridden()
    {
        return $this->isReleaseDateOverridden;
    }

    /**
     * Set isReleaseDateOverridden
     *
     * @param bool $isReleaseDateOverridden
     *
     * @return self
     */
    public function setIsReleaseDateOverridden(bool $isReleaseDateOverridden): self
    {
        $this->isReleaseDateOverridden = $isReleaseDateOverridden;

        return $this;
    }

    /**
     * @return string
     */
    public function getCoreNumber()
    {
        return $this->coreNumber;
    }

    /**
     * @param string $coreNumber
     *
     * @return Episode
     */
    public function setCoreNumber($coreNumber): self
    {
        $this->coreNumber = $coreNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getOrigination()
    {
        return $this->origination;
    }

    /**
     * @param string $origination
     *
     * @return Episode
     */
    public function setOrigination($origination): self
    {
        $this->origination = $origination;

        return $this;
    }

    /**
     * @return string
     */
    public function getContentLanguage()
    {
        return $this->contentLanguage;
    }

    /**
     * @param string $contentLanguage
     *
     * @return Episode
     */
    public function setContentLanguage($contentLanguage): self
    {
        $this->contentLanguage = $contentLanguage;

        return $this;
    }

    /**
     * Get versions
     *
     * @return Version[]
     */
    public function getVersions()
    {
        return $this->versions;
    }

    /**
     * Add version
     *
     * @param Version $version
     *
     * @return self
     */
    public function addVersion(Version $version): self
    {
        $this->versions[] = $version;

        return $this;
    }

    /**
     * Remove version
     *
     * @param Version $version
     *
     * @return self
     */
    public function removeVersion(Version $version): self
    {
        $this->versions->removeElement($version);

        return $this;
    }

    /**
     * @return boolean
     */
    public function isSpecial()
    {
        return $this->isSpecial;
    }

    /**
     * @param boolean $isSpecial
     *
     * @return Episode
     */
    public function setIsSpecial($isSpecial)
    {
        $this->isSpecial = $isSpecial;

        return $this;
    }
}
