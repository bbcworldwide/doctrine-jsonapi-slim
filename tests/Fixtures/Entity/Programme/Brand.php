<?php

namespace BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Programme;

use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Interfaces\GroupableInterface;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Traits\GroupTrait;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Interfaces\GroupInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\Common\Collections\Collection as DoctrineCollection;

/**
 * @Entity()
 */
class Brand extends AbstractProgramme implements GroupInterface
{
    /**
     * Implement GroupInterface
     */
    use GroupTrait;

    /**
     * @var DoctrineCollection|GroupableInterface[]
     * @OneToMany(targetEntity="AbstractProgramme", mappedBy="primaryBrandParent")
     */
    protected $children;

    /**
     * @var DoctrineCollection|GroupableInterface[]
     * @OneToMany(targetEntity="AbstractProgramme", mappedBy="primaryBrandParentPs")
     */
    protected $childrenPs;

    /**
     * Brand constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->children = new ArrayCollection();
        $this->childrenPs = new ArrayCollection();
    }
}
