<?php

namespace BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Programme;

use BBCWorldWide\JsonApi\JsonApi\EntityInterface;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Category;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Interfaces\CridInterface;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Traits\CridTrait;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Traits\StringIdTrait;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Traits\TimestampableTrait;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Version;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection as DoctrineCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Index;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;

/**
 * AbstractProgramme
 *
 * @Entity()
 * @HasLifecycleCallbacks()
 * @InheritanceType("SINGLE_TABLE")
 * @DiscriminatorColumn(name="type", type="string")
 * @DiscriminatorMap({
 *     "brand" = "Brand",
 *     "episode" = "Episode"
 * })
 * @Table(indexes={
 *     @Index(name="programme_crid", columns={"crid"}),
 *     @Index(name="programme_discriminator", columns={"type"}),
 *     @Index(name="programme_position", columns={"position"}),
 *     @Index(name="programme_created", columns={"created_at"}),
 *     @Index(name="programme_updated", columns={"updated_at"}),
 *     @Index(name="programme_title_text", columns={"full_name"}, flags={"fulltext"}),
 *     @Index(
 *         name="programme_all_text",
 *         columns={"full_name", "synopsis_short", "synopsis_medium", "synopsis_long"},
 *         flags={"fulltext"}
 *     )
 * })
 */
abstract class AbstractProgramme implements CridInterface, EntityInterface
{
    /**
     * Get ID - Must be outside trait for Doctrine proxies to work.
     *
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Bring in primary key property.
     *
     * We're still overriding the property to allow for extra serializer annotations.
     */
    use StringIdTrait;

    /**
     * Bring in CRID property
     */
    use CridTrait;

    /**
     * Bring in timestampable behaviour
     */
    use TimestampableTrait;

    /**
     * @var string Full name
     * @Column(type="string", length=255)
     */
    protected $fullName;

    /**
     * @var string Display name
     * @Column(type="string", length=255)
     */
    protected $displayName;

    /**
     * @var string Original Display name from Public Service
     * @Column(type="string", length=255, nullable=TRUE)
     */
    protected $displayNamePs;

    /**
     * @var bool Display name Public Service value overridden
     * @Column(type="boolean")
     */
    protected $isDisplayNameOverridden = false;

    /**
     * @var string Sort name
     * @Column(type="string", length=255)
     */
    protected $sortName;

    /**
     * @var bool Auto categorise programme
     * @Column(type="boolean")
     */
    protected $isAutoCategorised = false;

    /**
     * @var string Short synopsis
     * @Column(type="text", nullable=TRUE)
     */
    protected $synopsisShort;

    /**
     * @var string Original Short synopsis from Public Service
     * @Column(type="text", nullable=TRUE)
     */
    protected $synopsisShortPs;

    /**
     * @var bool Short synopsis Public Service value overridden
     * @Column(type="boolean")
     */
    protected $isSynopsisShortOverridden = false;

    /**
     * @var string
     * @Column(type="text", nullable=TRUE)
     */
    protected $synopsisMedium;

    /**
     * @var string Original Medium synopsis from Public Service
     * @Column(type="text", nullable=TRUE)
     */
    protected $synopsisMediumPs;

    /**
     * @var bool Medium synopsis Public Service value overridden
     * @Column(type="boolean")
     */
    protected $isSynopsisMediumOverridden = false;

    /**
     * @var string
     * @Column(type="text", nullable=TRUE)
     */
    protected $synopsisLong;

    /**
     * @var string Original Long synopsis from Public Service
     * @Column(type="text", nullable=TRUE)
     */
    protected $synopsisLongPs;

    /**
     * @var bool Long synopsis Public Service value overridden
     * @Column(type="boolean")
     */
    protected $isSynopsisLongOverridden = false;

    /**
     * @var string
     * @Column(type="string", length=255, nullable=TRUE)
     */
    protected $picture16x9;

    /**
     * @var string Original 16x9 picture from Public Service
     * @Column(type="string", length=255, nullable=TRUE)
     */
    protected $picture16x9Ps;

    /**
     * @var bool 16x9 picture Public Service value overridden
     * @Column(type="boolean")
     */
    protected $isPicture16x9Overridden = false;

    /**
     * @var string
     * @Column(type="string", length=255, nullable=TRUE)
     */
    protected $picturePoster;

    /**
     * @var string
     * @Column(type="string", length=255, nullable=TRUE)
     */
    protected $pictureSquare;

    /**
     * @var string Original Square picture from Public Service
     * @Column(type="string", length=255, nullable=TRUE)
     */
    protected $pictureSquarePs;

    /**
     * @var bool Square picture Public Service value overridden
     * @Column(type="boolean")
     */
    protected $isPictureSquareOverridden = false;

    /**
     * @var string
     * @Column(type="string", length=255, nullable=TRUE)
     */
    protected $pictureWallpaper;

    /**
     * @var bool
     * @Column(type="boolean", nullable=TRUE)
     */
    protected $isHidden;

    /**
     * @var bool Originated from public service
     * @Column(type="boolean", nullable=TRUE)
     */
    protected $isIngested = false;

    // Define all relationships on base class so we can use them in generic programme queries.
    // Getters / setters are defined in traits / interfaces where they're allowed.

    /**
     * @var Brand
     * @ManyToOne(targetEntity="Brand", inversedBy="children")
     * @JoinColumn(onDelete="SET NULL")
     */
    protected $primaryBrandParent;

    /**
     * @var Brand
     * @ManyToOne(targetEntity="Brand", inversedBy="childrenPs")
     * @JoinColumn(onDelete="SET NULL")
     */
    protected $primaryBrandParentPs;

    /**
     * @var bool Primary parent Public Service value overridden
     * @Column(type="boolean")
     */
    protected $isPrimaryParentOverridden = false;

    /**
     * @var DoctrineCollection|Version[]
     * @OneToMany(targetEntity="\BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Version", mappedBy="programme")
     */
    protected $versions;

    /**
     * @var DoctrineCollection|Category[]
     * @ManyToMany(targetEntity="\BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Category", inversedBy="programmes")
     */
    protected $categories;

    /**
     * @var DoctrineCollection|Category[]
     * @ManyToMany(targetEntity="\BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Category", inversedBy="effectiveProgrammes")
     * @JoinTable(name="effective_categories")
     */
    protected $effectiveCategories;

    /**
     * @var int
     * @Column(type="smallint", options={"unsigned":true})
     */
    protected $position = 0;

    /**
     * @var int Original Position from Public Service
     * @Column(type="smallint", options={"unsigned":true})
     */
    protected $positionPs = 0;

    /**
     * @var int
     */
    protected $metaPosition;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categories          = new ArrayCollection();
        $this->effectiveCategories = new ArrayCollection();
        $this->itemRelationships   = new ArrayCollection();
    }

    /**
     * Set displayName
     *
     * @param string $displayName
     *
     * @return self
     */
    public function setDisplayName($displayName): self
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * Get displayName
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * @return string
     */
    public function getDisplayNamePs()
    {
        return $this->displayNamePs;
    }

    /**
     * @param string $displayNamePs
     *
     * @return self
     */
    public function setDisplayNamePs($displayNamePs)
    {
        $this->displayNamePs = $displayNamePs;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isDisplayNameOverridden()
    {
        return $this->isDisplayNameOverridden;
    }

    /**
     * @param boolean $isDisplayNameOverridden
     *
     * @return self
     */
    public function setIsDisplayNameOverridden($isDisplayNameOverridden)
    {
        $this->isDisplayNameOverridden = $isDisplayNameOverridden;

        return $this;
    }

    /**
     * Set sortName
     *
     * @param string $sortName
     *
     * @return self
     */
    public function setSortName($sortName): self
    {
        $this->sortName = $sortName;

        return $this;
    }

    /**
     * Get sortName
     *
     * @return string
     */
    public function getSortName()
    {
        return $this->sortName;
    }

    /**
     * @return boolean
     */
    public function isAutoCategorised()
    {
        return $this->isAutoCategorised;
    }

    /**
     * @param boolean $isAutoCategorised
     *
     * @return self
     */
    public function setIsAutoCategorised($isAutoCategorised)
    {
        $this->isAutoCategorised = $isAutoCategorised;

        return $this;
    }

    /**
     * Set shortSynopsis
     *
     * @param string $synopsisShort
     *
     * @return self
     */
    public function setSynopsisShort($synopsisShort): self
    {
        $this->synopsisShort = $synopsisShort;

        return $this;
    }

    /**
     * Get shortSynopsis
     *
     * @return string
     */
    public function getSynopsisShort()
    {
        return $this->synopsisShort;
    }

    /**
     * @return string
     */
    public function getSynopsisShortPs()
    {
        return $this->synopsisShortPs;
    }

    /**
     * @param string $synopsisShortPs
     *
     * @return self
     */
    public function setSynopsisShortPs($synopsisShortPs)
    {
        $this->synopsisShortPs = $synopsisShortPs;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isSynopsisShortOverridden()
    {
        return $this->isSynopsisShortOverridden;
    }

    /**
     * @param boolean $isSynopsisShortOverridden
     *
     * @return self
     */
    public function setIsSynopsisShortOverridden($isSynopsisShortOverridden)
    {
        $this->isSynopsisShortOverridden = $isSynopsisShortOverridden;

        return $this;
    }

    /**
     * Set mediumSynopsis
     *
     * @param string $synopsisMedium
     *
     * @return self
     */
    public function setSynopsisMedium($synopsisMedium): self
    {
        $this->synopsisMedium = $synopsisMedium;

        return $this;
    }

    /**
     * Get mediumSynopsis
     *
     * @return string
     */
    public function getSynopsisMedium()
    {
        return $this->synopsisMedium;
    }

    /**
     * @return string
     */
    public function getSynopsisMediumPs()
    {
        return $this->synopsisMediumPs;
    }

    /**
     * @param string $synopsisMediumPs
     *
     * @return self
     */
    public function setSynopsisMediumPs($synopsisMediumPs)
    {
        $this->synopsisMediumPs = $synopsisMediumPs;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isSynopsisMediumOverridden()
    {
        return $this->isSynopsisMediumOverridden;
    }

    /**
     * @param boolean $isSynopsisMediumOverridden
     *
     * @return self
     */
    public function setIsSynopsisMediumOverridden($isSynopsisMediumOverridden)
    {
        $this->isSynopsisMediumOverridden = $isSynopsisMediumOverridden;

        return $this;
    }

    /**
     * Set longSynopsis
     *
     * @param string $synopsisLong
     *
     * @return self
     */
    public function setSynopsisLong($synopsisLong): self
    {
        $this->synopsisLong = $synopsisLong;

        return $this;
    }

    /**
     * Get longSynopsis
     *
     * @return string
     */
    public function getSynopsisLong()
    {
        return $this->synopsisLong;
    }

    /**
     * @return string
     */
    public function getSynopsisLongPs()
    {
        return $this->synopsisLongPs;
    }

    /**
     * @param string $synopsisLongPs
     *
     * @return self
     */
    public function setSynopsisLongPs($synopsisLongPs)
    {
        $this->synopsisLongPs = $synopsisLongPs;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isSynopsisLongOverridden()
    {
        return $this->isSynopsisLongOverridden;
    }

    /**
     * @param boolean $isSynopsisLongOverridden
     *
     * @return self
     */
    public function setIsSynopsisLongOverridden($isSynopsisLongOverridden)
    {
        $this->isSynopsisLongOverridden = $isSynopsisLongOverridden;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isIngested()
    {
        return $this->isIngested;
    }

    /**
     * @param boolean $isIngested
     *
     * @return self
     */
    public function setIsIngested($isIngested)
    {
        $this->isIngested = $isIngested;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        $parts = explode('\\', get_class($this));

        return array_pop($parts);
    }

    /**
     * Set fullName
     *
     * @param string $fullName
     *
     * @return self
     */
    public function setFullName(string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Get fullName
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Add category.
     *
     * Note: do not remove programme from category for performance reasons, this is the owning side.
     *
     * @param Category $category
     *
     * @return self
     */
    public function addCategory(Category $category): self
    {
        if ($this->categories->contains($category) === false) {
            $this->categories[] = $category;
        }

        return $this;
    }

    /**
     * Remove category
     *
     * Note: do not add programme to category for performance reasons, this is the owning side.
     *
     * @param Category $category
     *
     * @return self
     */
    public function removeCategory(Category $category): self
    {
        if ($this->categories->contains($category) === true) {
            $this->categories->removeElement($category);
        }

        return $this;
    }

    /**
     * Get categories
     *
     * @return DoctrineCollection|Category[]
     */
    public function getCategories(): DoctrineCollection
    {
        return $this->categories;
    }

    /**
     * Add effective category
     *
     * @param Category $category
     *
     * @return self
     */
    public function addEffectiveCategory(Category $category): self
    {
        if ($this->effectiveCategories->contains($category) === false) {
            $this->effectiveCategories[] = $category;
        }

        return $this;
    }

    /**
     * Remove effective category
     *
     * @param Category $category
     *
     * @return self
     */
    public function removeEffectiveCategory(Category $category): self
    {
        if ($this->effectiveCategories->contains($category) === true) {
            $this->effectiveCategories->removeElement($category);
        }

        return $this;
    }

    /**
     * Get effective categories
     *
     * @return DoctrineCollection
     */
    public function getEffectiveCategories()
    {
        return $this->effectiveCategories;
    }

    /**
     * Set picture16x9
     *
     * @param string $picture16x9
     *
     * @return self
     */
    public function setPicture16x9($picture16x9): self
    {
        $this->picture16x9 = $picture16x9;

        return $this;
    }

    /**
     * Get picture16x9
     *
     * @return string
     */
    public function getPicture16x9()
    {
        return $this->picture16x9;
    }

    /**
     * @return string
     */
    public function getPicture16x9Ps()
    {
        return $this->picture16x9Ps;
    }

    /**
     * @param string $picture16x9Ps
     */
    public function setPicture16x9Ps($picture16x9Ps)
    {
        $this->picture16x9Ps = $picture16x9Ps;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isPicture16x9Overridden()
    {
        return $this->isPicture16x9Overridden;
    }

    /**
     * @param boolean $isPicture16x9Overridden
     */
    public function setIsPicture16x9Overridden($isPicture16x9Overridden)
    {
        $this->isPicture16x9Overridden = $isPicture16x9Overridden;

        return $this;
    }

    /**
     * Set picturePoster
     *
     * @param string $picturePoster
     *
     * @return self
     */
    public function setPicturePoster($picturePoster): self
    {
        $this->picturePoster = $picturePoster;

        return $this;
    }

    /**
     * Get picturePoster
     *
     * @return string
     */
    public function getPicturePoster()
    {
        return $this->picturePoster;
    }

    /**
     * Set pictureSquare
     *
     * @param string $pictureSquare
     *
     * @return self
     */
    public function setPictureSquare($pictureSquare): self
    {
        $this->pictureSquare = $pictureSquare;

        return $this;
    }

    /**
     * Get pictureSquare
     *
     * @return string
     */
    public function getPictureSquare()
    {
        return $this->pictureSquare;
    }

    /**
     * @return string
     */
    public function getPictureSquarePs()
    {
        return $this->pictureSquarePs;
    }

    /**
     * @param string $pictureSquarePs
     */
    public function setPictureSquarePs($pictureSquarePs)
    {
        $this->pictureSquarePs = $pictureSquarePs;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isPictureSquareOverridden()
    {
        return $this->isPictureSquareOverridden;
    }

    /**
     * @param boolean $isPictureSquareOverridden
     */
    public function setIsPictureSquareOverridden($isPictureSquareOverridden)
    {
        $this->isPictureSquareOverridden = $isPictureSquareOverridden;

        return $this;
    }

    /**
     * Set pictureWallpaper
     *
     * @param string $pictureWallpaper
     *
     * @return self
     */
    public function setPictureWallpaper($pictureWallpaper): self
    {
        $this->pictureWallpaper = $pictureWallpaper;

        return $this;
    }

    /**
     * Get pictureWallpaper
     *
     * @return string
     */
    public function getPictureWallpaper()
    {
        return $this->pictureWallpaper;
    }

    /**
     * @return boolean
     */
    public function isHidden()
    {
        return $this->isHidden;
    }

    /**
     * @param boolean $isHidden
     *
     * @return self
     */
    public function setIsHidden($isHidden): self
    {
        $this->isHidden = (bool) $isHidden;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getFullName();
    }

    /**
     * Set position value to display in Json:API metadata
     *
     * Bit of a hack but this needs to be stored on the entity due to the way the serializer works.
     *
     * @param int $metaPosition
     *
     * @return self
     */
    public function setMetaPosition(int $metaPosition): self
    {
        $this->metaPosition = $metaPosition;

        return $this;
    }

    /**
     * Get position value to display in Json:API metadata
     */
    public function getMetaPosition()
    {
        return $this->metaPosition;
    }
}
