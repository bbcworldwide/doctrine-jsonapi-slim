<?php
namespace BBCWorldWide\JsonApi\Tests\Fixtures\Entity;

use BBCWorldWide\JsonApi\JsonApi\EntityInterface;
use Doctrine\ORM\Mapping\Cache;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToMany;

/**
 * Version type.
 *
 * @Entity(readOnly=true)
 * @Cache(usage="READ_ONLY", region="version_type_region")
 */
class VersionType implements EntityInterface
{
    /**
     * @var string
     * @Id()
     * @Column(type="string", length=32)
     */
    protected $id;

    /**
     * @ManyToMany(targetEntity="Version", mappedBy="versionTypes")
     */
    protected $versions;

    /**
     * @return mixed
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return VersionType
     */
    public function setId(string $id)
    {
        $this->id = $id;

        return $this;
    }
}
