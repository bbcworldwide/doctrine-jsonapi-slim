<?php
namespace BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Traits;

use Doctrine\ORM\Mapping\Column;

/**
 * Name property.
 */
trait NameTrait
{
    /**
     * @var string
     * @Column(type="string", length=255)
     */
    protected $name;

    /**
     * Set name
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName($name) : self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
