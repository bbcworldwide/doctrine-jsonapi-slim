<?php

namespace BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Traits;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Id;

/**
 * Primary key property (id).
 */
trait StringIdTrait
{
    /**
     * @var string
     * @Column(type="string", length=36)
     * @Id()
     */
    protected $id;

    // Getter must be outside trait for Doctrine proxies to work correctly.

    /**
     * Set ID
     *
     * @param string $id
     *
     * @return self
     */
    public function setId(string $id)
    {
        $this->id = $id;

        return $this;
    }

}
