<?php

namespace BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Traits;

use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Interfaces\GroupableInterface;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Programme\Brand;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;

/**
 * Use this trait to implement the groupable interface
 */
trait GroupableTrait
{
    /**
     * @var bool Position Public Service value overridden
     * @Column(type="boolean")
     */
    protected $isPositionOverridden = false;

    /**
     * @inheritdoc
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @inheritdoc
     */
    public function setPositionPs($positionPs)
    {
        $this->positionPs = $positionPs;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getPositionPs()
    {
        return $this->positionPs;
    }

    /**
     * @return boolean
     */
    public function isPositionOverridden()
    {
        return $this->isPositionOverridden;
    }

    /**
     * @inheritdoc
     */
    public function setIsPositionOverridden(bool $isPositionOverridden)
    {
        $this->isPositionOverridden = $isPositionOverridden;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setPrimaryParent($primaryParent)
    {
        if ($primaryParent instanceof Brand) {
            $this->primaryBrandParent = $primaryParent;
        } elseif (is_null($primaryParent)) {
            $this->primaryBrandParent = null;
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getPrimaryParent()
    {
        return $this->primaryBrandParent;
    }

    /**
     * @inheritdoc
     */
    public function setPrimaryParentPs($primaryParentPs)
    {
        $this->primaryBrandParentPs = $primaryParentPs;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getPrimaryParentPs()
    {
        return $this->primaryBrandParentPs;
    }

    /**
     * @inheritdoc
     */
    public function isPrimaryParentOverridden()
    {
        return $this->isPrimaryParentOverridden;
    }

    /**
     * @inheritdoc
     */
    public function setIsPrimaryParentOverridden(bool $isPrimaryParentOverridden)
    {
        $this->isPrimaryParentOverridden = $isPrimaryParentOverridden;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getParents()
    {
        $parents = new ArrayCollection();
        if ($parent = $this->getPrimaryParent()) {
            $parents[] = $parent;
        }

        foreach ($this->getCollections() as $parent) {
            $parents[] = $parent;
        }

        return $parents;
    }

    /**
     * @inheritdoc
     */
    public function getAncestors($includeSelf = false)
    {
        $ancestors = new ArrayCollection();
        if ($includeSelf) {
            $ancestors[] = $this;
        }

        if ($parent = $this->getPrimaryParent()) {
            $parents[] = $parent;
        }

        foreach ($this->getCollections() as $parent) {
            if ($parent instanceof GroupableInterface) {
                foreach ($parent->getAncestors(true) as $ancestor) {
                    $ancestors[] = $ancestor;
                }
            }
        }

        return $ancestors;
    }

    /**
     * @inheritdoc
     */
    public function getPrimaryAncestors($includeSelf = false)
    {
        $ancestors = [];
        if ($includeSelf) {
            $ancestors[] = $this;
        }
        $current = $this;

        while ($current instanceof GroupableInterface && $current = $current->getPrimaryParent()) {
            array_unshift($ancestors, $current);
        }

        return new ArrayCollection($ancestors);
    }
}
