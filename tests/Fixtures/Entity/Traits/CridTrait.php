<?php
namespace BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Traits;

use Doctrine\ORM\Mapping\Column;

/**
 * CRID property
 */
trait CridTrait
{
    /**
     * @Column(type="string", length=255, nullable=true)
     */
    protected $crid;

    /**
     * Set crid
     *
     * @param string $crid
     *
     * @return $this
     */
    public function setCrid(string $crid = null) : self
    {
        $this->crid = $crid;

        return $this;
    }

    /**
     * Get crid
     *
     * @return string
     */
    public function getCrid()
    {
        return $this->crid;
    }
}
