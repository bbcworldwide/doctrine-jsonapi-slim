<?php
namespace BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Traits;

use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Interfaces\GroupInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;

/**
 * Use this trait to implement the group interface
 */
trait GroupTrait
{
    /**
     * @var int
     * @Column(type="smallint", options={"unsigned":true}, nullable=TRUE)
     */
    protected $statedItems;

    /**
     * @var int
     * @Column(type="smallint", options={"unsigned":true}, nullable=TRUE)
     */
    protected $statedItemsPs;

    /**
     * @var bool
     * @Column(type="boolean")
     */
    protected $isStatedItemsOverridden = false;

    /**
     * @inheritdoc
     */
    public function getStatedItems()
    {
        return $this->statedItems;
    }

    /**
     * @inheritdoc
     */
    public function setStatedItems($statedItems) : self
    {
        $this->statedItems = $statedItems;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getStatedItemsPs()
    {
        return $this->statedItemsPs;
    }

    /**
     * @inheritdoc
     */
    public function setStatedItemsPs($statedItemsPs) : self
    {
        $this->statedItemsPs = $statedItemsPs;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function isStatedItemsOverridden()
    {
        return $this->isStatedItemsOverridden;
    }

    /**
     * @inheritdoc
     */
    public function setIsStatedItemsOverridden(bool $isStatedItemsOverridden) : self
    {
        $this->isStatedItemsOverridden = $isStatedItemsOverridden;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @inheritdoc
     */
    public function getDescendants($includeSelf = false)
    {
        $descendants = new ArrayCollection();
        if ($includeSelf) {
            $descendants[] = $this;
        }
        foreach ($this->getChildren() as $child) {
            if ($child instanceof GroupInterface) {
                foreach ($child->getDescendants(true) as $descendant) {
                    $descendants[] = $descendant;
                }
            } else {
                $descendants[] = $child;
            }
        }

        return $descendants;
    }
}
