<?php
namespace BBCWorldWide\JsonApi\Tests\Fixtures\Entity;

use BBCWorldWide\JsonApi\JsonApi\EntityInterface;
use Doctrine\ORM\Mapping\Cache;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToMany;

/**
 * Warning message.
 *
 * @link http://downloads.bbc.co.uk/commissioning/site/Guidance_labels_final.pdf
 *
 * @Entity(readOnly=true)
 * @Cache(usage="READ_ONLY", region="warning_region")
 */
class Warning implements EntityInterface
{
    /**
     * @var string Warning id
     * @Id()
     * @Column(type="string", length=8)
     */
    protected $id;

    /**
     * @var string
     * @Column(type="string", length=255)
     */
    protected $warningTextLong;

    /**
     * @var string
     * @Column(type="string", length=255)
     */
    protected $warningTextShort;

    /**
     * @ManyToMany(targetEntity="Version", mappedBy="warnings")
     */
    protected $versions;

    /**
     * Set id
     *
     * @param string $id
     *
     * @return Warning
     */
    public function setId(string $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Set warningTextLong
     *
     * @param string $warningTextLong
     *
     * @return Warning
     */
    public function setWarningTextLong(string $warningTextLong) : self
    {
        $this->warningTextLong = $warningTextLong;

        return $this;
    }

    /**
     * Get warningTextLong
     *
     * @return string
     */
    public function getWarningTextLong()
    {
        return $this->warningTextLong;
    }

    /**
     * Set warningTextShort
     *
     * @param string $warningTextShort
     *
     * @return Warning
     */
    public function setWarningTextShort(string $warningTextShort) : self
    {
        $this->warningTextShort = $warningTextShort;

        return $this;
    }

    /**
     * Get warningTextShort
     *
     * @return string
     */
    public function getWarningTextShort()
    {
        return $this->warningTextShort;
    }
}
