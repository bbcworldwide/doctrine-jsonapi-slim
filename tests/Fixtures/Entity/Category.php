<?php
namespace BBCWorldWide\JsonApi\Tests\Fixtures\Entity;

use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Interfaces\CridInterface;
use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Programme\AbstractProgramme;
use BBCWorldWide\JsonApi\JsonApi\EntityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection as DoctrineCollection;
use Doctrine\Common\EventArgs;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OrderBy;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\PreUpdate;
use Doctrine\ORM\Mapping\Table;

/**
 * Category.
 *
 * @Entity()
 * @HasLifecycleCallbacks()
 * @Table(indexes={
 *     @Index(name="category_crid", columns={"crid"}),
 *     @Index(name="category_sort", columns={"parent_id", "position"}),
 *     @Index(name="category_created", columns={"created_at"}),
 *     @Index(name="category_updated", columns={"updated_at"})
 * })
 */
class Category implements CridInterface, EntityInterface
{
    /**
     * Get ID - Must be outside trait for Doctrine proxies to work.
     *
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Bring in primary key property
     */
    use Traits\StringIdTrait;

    /**
     * Bring in CRID property
     */
    use Traits\CridTrait;

    /**
     * Bring in name property.
     */
    use Traits\NameTrait;

    /**
     * Bring in timestampable behaviour
     */
    use Traits\TimestampableTrait;

    /**
     * @var int
     * @Column(type="smallint", options={"unsigned":true,"default":0}, nullable=TRUE)
     */
    protected $position;

    /**
     * @var Category
     * @ManyToOne(targetEntity="Category", inversedBy="children")
     * @JoinColumn(onDelete="CASCADE")
     */
    protected $parent;

    /**
     * @var DoctrineCollection|Category[]
     * @OneToMany(targetEntity="Category", mappedBy="parent", cascade={"persist"})
     * @OrderBy({"position" = "ASC"})
     */
    protected $children;

    /**
     * @var DoctrineCollection|AbstractProgramme[]
     * @ManyToMany(targetEntity="\BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Programme\AbstractProgramme", mappedBy="categories")
     */
    protected $programmes;

    /**
     * @var DoctrineCollection|AbstractProgramme[]
     * @ManyToMany(targetEntity="\BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Programme\AbstractProgramme", mappedBy="effectiveCategories")
     */
    protected $effectiveProgrammes;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->programmes = new ArrayCollection();
        $this->effectiveProgrammes = new ArrayCollection();
    }

    /**
     * @PrePersist()
     * @PreUpdate()
     * @todo Implement
     */
    public function updatePosition(EventArgs $event)
    {
    }

    /**
     * Set position
     *
     * @param int $position
     *
     * @return Category
     */
    public function setPosition(int $position) : self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set parent
     *
     * @param Category $parent
     *
     * @return Category
     */
    public function setParent(Category $parent = null)
    {
        if ($this->parent !== $parent) {
            if ($this->parent) {
                $this->parent->removeChild($this);
            }
            $this->parent = $parent;
            if ($parent) {
                $parent->addChild($this);
            }
        }

        return $this;
    }

    /**
     * Get parent
     *
     * @return Category
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add child
     *
     * @param Category $child
     *
     * @return Category
     */
    public function addChild(Category $child) : self
    {
        if ($this->children->contains($child) === false) {
            $this->children[] = $child;
            $child->setParent($this);
        }

        return $this;
    }

    /**
     * Remove child
     *
     * @param Category $child
     *
     * @return Category
     */
    public function removeChild(Category $child) : self
    {
        if ($this->children->contains($child) === true) {
            $this->children->removeElement($child);
            $child->setParent(null);
        }

        return $this;
    }

    /**
     * Get children
     *
     * @return DoctrineCollection|Category[]
     */
    public function getChildren() : DoctrineCollection
    {
        return $this->children;
    }

    public function getAncestors($includeSelf = false)
    {
        $ancestors = [];
        if ($includeSelf) {
            $ancestors[] = $this;
        }
        $category = $this;
        while ($category = $category->getParent()) {
            $ancestors[] = $category;
        }

        return $ancestors;
    }

    /**
     * @param bool $includeSelf
     * @return Category[]
     */
    public function getDescendants($includeSelf = false)
    {
        $descendants = [];
        if ($includeSelf) {
            $descendants[] = $this;
        }
        foreach ($this->getChildren() as $child) {
            $descendants[] = $child;
            $descendants += $child->getDescendants();
        }

        return $descendants;
    }

    /**
     * @param string $separator
     * @return string
     */
    public function getPath($separator = ' > ')
    {
        $path = '';
        foreach (array_reverse($this->getAncestors()) as $parent) {
            $path .= $parent->getName() . $separator;
        }
        $path .= $this->getName();

        return $path;
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return $this->getPath();
    }

    /**
     * Add programme
     *
     * @param AbstractProgramme $programme
     *
     * @return $this
     */
    public function addProgramme(AbstractProgramme $programme) : self
    {
        if ($this->programmes->contains($programme) === false) {
            $this->programmes[] = $programme;
            $programme->addCategory($this);
        }

        return $this;
    }

    /**
     * Remove programme
     *
     * @param AbstractProgramme $programme
     *
     * @return $this
     */
    public function removeProgramme(AbstractProgramme $programme) : self
    {
        if ($this->programmes->contains($programme) === true) {
            $this->programmes->removeElement($programme);
            $programme->removeCategory($this);
        }

        return $this;
    }

    /**
     * Get programmes
     *
     * @return DoctrineCollection|AbstractProgramme[]
     */
    public function getProgrammes()
    {
        return $this->programmes;
    }

    /**
     * Add effective programme
     *
     * @param AbstractProgramme $programme
     *
     * @return $this
     */
    public function addEffectiveProgramme(AbstractProgramme $programme) : self
    {
        if ($this->effectiveProgrammes->contains($programme) === false) {
            $this->effectiveProgrammes[] = $programme;
        }

        return $this;
    }

    /**
     * Remove effective programme
     *
     * @param AbstractProgramme $programme
     *
     * @return $this
     */
    public function removeEffectiveProgramme(AbstractProgramme $programme) : self
    {
        if ($this->effectiveProgrammes->contains($programme) === true) {
            $this->effectiveProgrammes->removeElement($programme);
        }

        return $this;
    }
    /**
     * Get effective programmes
     *
     * @return DoctrineCollection|AbstractProgramme[]
     */
    public function getEffectiveProgrammes()
    {
        return $this->effectiveProgrammes;
    }
}
