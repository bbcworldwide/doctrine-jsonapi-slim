<?php

namespace BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Interfaces;

use BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Programme\Brand;
use Doctrine\Common\Collections\Collection as DoctrineCollection;

/**
 * Defines an interface for programmes which can be grouped by other programmes.
 */
interface GroupableInterface
{
    /**
     * Set position i.e. episode or series number.
     *
     * @param int $position
     *
     * @return $this
     */
    public function setPosition($position);

    /**
     * Get position i.e. episode or series number.
     *
     * @return int
     */
    public function getPosition();

    /**
     * Set position Public Service value.
     *
     * @param int $position
     *
     * @return $this
     */
    public function setPositionPs($positionPs);

    /**
     * Get position Public Service value.
     *
     * @return int
     */
    public function getPositionPs();

    /**
     * Is Public Service value of position overridden?
     *
     * @return bool
     */
    public function isPositionOverridden();

    /**
     * Set overridden status of position.
     *
     * @param bool $isPositionOverridden
     *
     * @return $this
     */
    public function setIsPositionOverridden(bool $isPositionOverridden);

    /**
     * Set primary parent i.e. the non-collection programme that this programme belongs to.
     *
     * @param Brand|null $primaryParent
     *
     * @return $this
     */
    public function setPrimaryParent($primaryParent);

    /**
     * Get primary parent i.e. the non-collection programme that this programme belongs to.
     *
     * @return Brand|null
     */
    public function getPrimaryParent();

    /**
     * Set Public Service value of primary parent.
     *
     * @param Brand|null $primaryParentPs
     *
     * @return $this
     */
    public function setPrimaryParentPs($primaryParentPs);

    /**
     * Get Public Service value of primary parent
     *
     * @return Brand|GroupInterface|null
     */
    public function getPrimaryParentPs();

    /**
     * Is Public Service value of primary parent overridden?
     *
     * @return bool
     */
    public function isPrimaryParentOverridden();

    /**
     * Set overridden status of primary parent.
     *
     * @param boolean $isPrimaryParentOverridden
     *
     * @return $this
     */
    public function setIsPrimaryParentOverridden(bool $isPrimaryParentOverridden);

    /**
     * Get parents i.e. primary parent + collections
     *
     * @return DoctrineCollection|GroupInterface[]
     */
    public function getParents();

    /**
     * Get ancestors i.e. recursive parents of parents.
     *
     * @param bool $includeSelf
     *
     * @return DoctrineCollection|GroupInterface[]
     */
    public function getAncestors($includeSelf = false);

    /**
     * Get primary ancestors i.e. recursive primary parents of primary parents
     *
     * @param bool $includeSelf
     *
     * @return DoctrineCollection|GroupInterface[]
     */
    public function getPrimaryAncestors($includeSelf = false);
}
