<?php
namespace BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Interfaces;

/**
 * Defines an interface for entities with a Content Reference ID
 */
interface CridInterface
{
    /**
     * Set crid
     *
     * @param string $crid
     *
     * @return $this
     */
    public function setCrid(string $crid = null);

    /**
     * Get crid
     *
     * @return string
     */
    public function getCrid();
}
