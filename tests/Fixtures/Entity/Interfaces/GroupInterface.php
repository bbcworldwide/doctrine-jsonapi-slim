<?php
namespace BBCWorldWide\JsonApi\Tests\Fixtures\Entity\Interfaces;

use Doctrine\Common\Collections\Collection as DoctrineCollection;

/**
 * Defines an interface for for programmes that can group other programmes.
 */
interface GroupInterface
{
    /**
     * Get stated items - i.e. the expected number of children in this group.
     *
     * @return int
     */
    public function getStatedItems();

    /**
     * Set stated items - i.e. the expected number of children in this group.
     *
     * @param int $statedItems
     * @return $this
     */
    public function setStatedItems($statedItems);

    /**
     * Get stated items from Public Service.
     *
     * @return int
     */
    public function getStatedItemsPs();

    /**
     * Set stated items value from Public Service.
     *
     * @param int $statedItems
     * @return $this
     */
    public function setStatedItemsPs($statedItemsPs);

    /**
     * Is Public Service value of stated items overridden?
     *
     * @return bool
     */
    public function isStatedItemsOverridden();

    /**
     * Set overridden status of stated items.
     *
     * @param bool $isStatedItemsOverridden
     *
     * @return $this
     */
    public function setIsStatedItemsOverridden(bool $isStatedItemsOverridden);

    /**
     * Get children.
     *
     * @return DoctrineCollection|GroupableInterface[]
     */
    public function getChildren();

    /**
     * Get descendants i.e. recursive children of children.
     *
     * @return DoctrineCollection|GroupableInterface[]
     */
    public function getDescendants($includeSelf = false);
}
