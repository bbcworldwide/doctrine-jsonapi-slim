<?php

namespace BBCWorldWide\JsonApi\Tests\Fixtures;

/**
 * Loads JSON payload files from disk.
 */
class JsonLoader
{
    /**
     * Load JSON file
     *
     * @param string $path  File path relative to json directory
     *
     * @return string       JSON payload
     */
    public function load($path)
    {
        $file = __DIR__ . '//json/'. $path;
        if (!file_exists($file)) {
            throw new \InvalidArgumentException("JSON fixture '$path' not found");
        }
        return file_get_contents($file);
    }
}
