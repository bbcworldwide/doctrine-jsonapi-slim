<?php

namespace BBCWorldWide\JsonApi\Tests\Functional;

use BBCWorldWide\JsonApi\App\SetupInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Set up class for our tests. Sets up a test schema with its entities.
 *
 * @author BBC Worldwide
 */
class TestAppSetup implements SetupInterface
{
    /**
     * Return an array of middlewares to add to the stack. They must be callables which return instances to middlewares.
     *
     * @return callable[]
     */
    public function getMiddlewareCallables(): array
    {
        return [
            new class
            {
                public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next)
                {
                    return $next($request, $response);
                }
            },
        ];
    }

    /**
     * Returns any extra config the app may want to define as an array.
     *
     * @return array
     */
    public function getSettings(): array
    {
        return include __DIR__ . '/../Fixtures/settings-test.php';
    }

    /**
     * Returns a list of extra dependency (or overrides) the app may want to add to the container.
     *
     * @return callable[]
     */
    public function getContainerDependencies(): array
    {
        return include __DIR__ . '/../../src/container.php';
    }

    /**
     * Returns a JSON API schema array - see examples.
     *
     * @return array
     */
    public function getJsonApiSchema(): array
    {
        return include __DIR__ . '/../Fixtures/schema.php';
    }

    /**
     * Return an array of paths (relative to the class implementing this interface) where doctrine entities can be
     * found.
     *
     * @return array
     */
    public function getEntityPaths(): array
    {
        return [__DIR__ . '/../Fixtures/Entity'];
    }

    /**
     * Return an array of callables that take a Psr\Container\ContainerInterface parameter which returns an
     * implementation of the interface Doctrine\Common\EventSubscriber.
     *
     * @return array
     */
    public function getEventSubscribers(): array
    {
        return [];
    }
}
