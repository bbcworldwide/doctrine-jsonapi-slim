<?php

namespace BBCWorldWide\JsonApi\Tests\Functional\ResponseCache;

use BBCWorldWide\JsonApi\ResponseCache\ExpiryChecker;
use BBCWorldWide\JsonApi\Tests\Functional\ApiTestCase;

class ExpiryCheckerTest extends ApiTestCase
{
    /**
     * @var ExpiryChecker
     */
    private $checker;

    public function setUp()
    {
        parent::setUp();
        $entityManager = $this->client->getApplication()->getContainer()->get('entityManager');
        $this->checker = new ExpiryChecker($entityManager);
    }

    /**
     * @test
     */
    public function nullByDefault()
    {
        $expiryDate = $this->checker->getExpiryDate();
        self::assertNull($expiryDate);
    }

    /**
     * @todo: more test scenarios, after removing catalogue-api specific code there's nothing here
     */

}
