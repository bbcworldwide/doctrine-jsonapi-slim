<?php
/**
 * @file Custom assertion functions.
 */

use Helmich\JsonAssert\Constraint\JsonValueMatchesMany;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\Constraint\LogicalAnd;

/**
 * @param array $constraints
 *
 * @return LogicalAnd
 */
function bodyMatchesJsonApi(array $constraints): LogicalAnd
{
    return Assert::logicalAnd(
        hasContentType('application/vnd.api+json'),
        bodyMatches(
            Assert::logicalAnd(
                Assert::isJson(),
                new JsonValueMatchesMany($constraints)
            )
        )
    );
}
