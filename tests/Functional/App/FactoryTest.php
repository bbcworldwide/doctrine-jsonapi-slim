<?php

namespace BBCWorldWide\JsonApi\Tests\Functional\App;

use BBCWorldWide\JsonApi\App\Application;
use BBCWorldWide\JsonApi\App\Factory;
use BBCWorldWide\JsonApi\Tests\Functional\TestAppSetup;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

/**
 * This factory will be the main method consumers will be instantiating us.
 *
 * This functional test ensures the factory works with the test setup config.
 *
 * @author BBC Worldwide
 */
class FactoryTest extends TestCase
{
    /**
     * @test
     */
    public function factoryReturnsApplication()
    {
        $appSetup = new TestAppSetup();
        $application = Factory::getInstance($appSetup);

        self::assertInstanceOf(Application::class, $application);
        self::assertInstanceOf(ContainerInterface::class, $application->getContainer());
    }

}
