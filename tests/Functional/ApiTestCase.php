<?php

namespace BBCWorldWide\JsonApi\Tests\Functional;

require_once __DIR__ . '/Functions.php';

use BBCWorldWide\JsonApi\Tests\Fixtures\DataLoader;
use BBCWorldWide\JsonApi\Tests\Fixtures\JsonLoader;
use Helmich\JsonAssert\JsonAssertions;
use Helmich\Psr7Assert\Psr7Assertions;
use Psr\Http\Message\ResponseInterface;

/**
 * Base class for API tests
 */
class ApiTestCase extends \PHPUnit\Framework\TestCase
{
    use Psr7Assertions;
    use JsonAssertions;

    /**
     * @var ApiClient
     */
    protected $client;

    /**
     * @var DataLoader
     */
    protected $dataFixtures;

    /**
     * @var JsonLoader
     */
    protected $jsonFixtures;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->client = new ApiClient();
        $container    = $this->client->getApplication()->getContainer();

        $this->dataFixtures = new DataLoader($container->get('entityManager'), $container->get('cacheFolder'));
        $this->jsonFixtures = new JsonLoader();
    }

    /**
     * Parse JSON from response body.
     *
     * @param ResponseInterface $response
     *
     * @return string
     */
    public function getResponseBody(ResponseInterface $response): string
    {
        $response->getBody()->rewind();

        return $response->getBody()->getContents();
    }

    /**
     * Parse JSON from response body.
     *
     * @param ResponseInterface $response
     *
     * @return array
     */
    public function getResponseJson(ResponseInterface $response): array
    {
        return json_decode($this->getResponseBody($response), true);
    }
}
