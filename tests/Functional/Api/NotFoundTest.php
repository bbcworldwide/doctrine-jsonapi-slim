<?php

namespace BBCWorldWide\JsonApi\Tests\Functional\Api;

use BBCWorldWide\JsonApi\Tests\Functional\ApiTestCase;

class NotFoundTest extends ApiTestCase
{
    /**
     * @test
     */
    public function unknownResourceTypeReturnsNotFoundResponse()
    {
        $uri = md5(time());

        $response = $this->client->get('/v1/' . $uri);

        self::assertResponseHasStatus($response, 404);
    }

    /**
     * @test
     */
    public function unknownRouteReturnsNotFoundResponse()
    {
        $uri = md5(time());

        $response = $this->client->get($uri);

        self::assertResponseHasStatus($response, 404);
    }
}
