<?php
namespace BBCWorldWide\JsonApi\Tests\Functional\Api;

use BBCWorldWide\JsonApi\Tests\Functional\ApiTestCase;
use Slim\Http\RequestBody;
use Slim\Http\Response;

class JsonApiHeadersTest extends ApiTestCase
{
    /**
     * @test
     */
    public function Accepts_headers_are_optional()
    {
        $request = $this->client->buildRequest([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => '/v1/programmes',
        ], new RequestBody());

        $response = $this->client->getApplication()->process($request, new Response());
        static::assertResponseHasStatus($response, 200);
    }

    /**
     * @test
     */
    public function Accepts_headers_must_include_JSON_API()
    {
        $request = $this->client->buildRequest([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => '/v1/programmes',
        ], new RequestBody());
        $request = $request->withAddedHeader('Accepts', 'application/json');

        $response = $this->client->getApplication()->process($request, new Response());
        static::assertResponseHasStatus($response, 406);

        $request = $request->withAddedHeader('Accepts', 'application/vnd.api+json');
        $response = $this->client->getApplication()->process($request, new Response());
        static::assertResponseHasStatus($response, 200);
    }

    /**
     * @test
     */
    public function Requests_with_body_must_have_Content_Type_of_JSON_API()
    {
        $this->dataFixtures->purge();

        $body = new RequestBody();
        $body->write($this->jsonFixtures->load('programmes/new-brand.json'));
        $request = $this->client->buildRequest([
            'REQUEST_METHOD' => 'POST',
            'REQUEST_URI'    => '/v1/programmes',
        ], $body);

        $response = $this->client->getApplication()->process($request, new Response());
        static::assertResponseHasStatus($response, 406);

        $request = $request->withAddedHeader('Content-Type', 'application/vnd.api+json');
        $response = $this->client->getApplication()->process($request, new Response());
        static::assertResponseHasStatus($response, 201);
    }
}
