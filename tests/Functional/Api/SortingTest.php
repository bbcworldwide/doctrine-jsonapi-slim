<?php
namespace BBCWorldWide\JsonApi\Tests\Functional\Api;

use BBCWorldWide\JsonApi\Tests\Functional\ApiTestCase;

class SortingTest extends ApiTestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->dataFixtures->load();
    }

    /**
     * @test
     */
    public function Can_sort_index()
    {
        $response = $this->client->get('/v1/categories?sort=createdAt');
        assertThat($response, logicalAnd(
            hasStatus(200),
            bodyMatchesJsonApi(['$.data[0].id' => 'hidden'])
        ));
    }

    /**
     * @test
     */
    public function Can_sort_relationship()
    {
        $response = $this->client->get('/v1/categories/drama/children?sort=createdAt');
        assertThat($response, logicalAnd(
            hasStatus(200),
            bodyMatchesJsonApi(['$.data[0].id' => 'drama-crime-thriller'])
        ));
    }

    /**
     * @test
     */
    public function Can_sort_in_descending_order()
    {
        $response = $this->client->get('/v1/categories/drama/children?sort=-position');
        assertThat($response, logicalAnd(
            hasStatus(200),
            bodyMatchesJsonApi(['$.data[0].id' => 'drama-doctor-who'])
        ));
    }

    /**
     * @test
     */
    public function Can_sort_on_multiple_fields()
    {
        $response = $this->client->get('/v1/categories?sort=position,createdAt');
        assertThat($response, logicalAnd(
            hasStatus(200),
            bodyMatchesJsonApi(['$.data[0].id' => 'factual-documentaries'])
        ));
    }

    /**
     * @test
     */
    public function Cant_sort_on_invalid_fields()
    {
        $response = $this->client->get('/v1/categories?sort=-madeUpField');
        assertThat($response, hasStatus(400));
    }

    /**
     * @test
     */
    public function Cant_sort_on_nonsortable_fields()
    {
        $response = $this->client->get('/v1/categories?sort=name');
        assertThat($response, hasStatus(400));
    }
}
