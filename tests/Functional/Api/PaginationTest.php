<?php
namespace BBCWorldWide\JsonApi\Tests\Functional\Api;

use BBCWorldWide\JsonApi\Tests\Functional\ApiTestCase;

class PaginationTest extends ApiTestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->dataFixtures->load();
    }

    /**
     * @test
     */
    public function Includes_page_metadata()
    {
        $response = $this->client->get('/v1/versions');
        assertThat($response, bodyMatchesJsonApi([
            '$.meta.results' => 100,
            '$.meta.pageSize' => 50,
            '$.meta.pageNumber' => 1,
            '$.meta.totalPages' => 2,
        ]));
    }

    /**
     * @test
     */
    public function Includes_page_navigation_links()
    {
        $response = $this->client->get('/v1/versions?page[size]=20&page[number]=3');
        assertThat($response, bodyMatchesJsonApi([
            '$.links.self' => '/v1/versions?page[size]=20&page[number]=3',
            '$.links.first' => '/v1/versions?page[size]=20&page[number]=1',
            '$.links.last' => '/v1/versions?page[size]=20&page[number]=5',
            '$.links.prev' => '/v1/versions?page[size]=20&page[number]=2',
            '$.links.next' => '/v1/versions?page[size]=20&page[number]=4',
        ]));

        $response = $this->client->get('/v1/versions?page[size]=20&page[number]=1');
        assertThat($response, bodyMatchesJsonApi(['$.links.prev' => isNull()]));

        $response = $this->client->get('/v1/versions?page[size]=20&page[number]=5');
        assertThat($response, bodyMatchesJsonApi(['$.links.next' => isNull()]));
    }

    /**
     * @test
     */
    public function Can_specify_page_size()
    {
        $response = $this->client->get('/v1/versions?page[size]=25');
        assertThat($response, bodyMatchesJsonApi(['$.meta.pageSize' => 25]));
    }

    /**
     * @test
     */
    public function Cant_exceed_maximum_page_size()
    {
        $response = $this->client->get('/v1/versions?page[size]=301');
        assertThat($response, hasStatus(400));
    }

    /**
     * @test
     */
    public function Page_size_must_be_numeric()
    {
        $response = $this->client->get('/v1/versions?page[size]=ponies');
        assertThat($response, hasStatus(400));
    }

    /**
     * @test
     */
    public function Can_specify_page_number()
    {
        $response = $this->client->get('/v1/versions?page[number]=2');
        assertThat($response, bodyMatchesJsonApi(['$.meta.pageNumber' => 2]));
    }

    /**
     * @test
     */
    public function Cant_specify_page_number_out_of_range()
    {
        $response = $this->client->get('/v1/versions?page[number]=3');
        assertThat($response, hasStatus(400));

        $response = $this->client->get('/v1/versions?page[number]=-1');
        assertThat($response, hasStatus(400));
    }


    /**
     * @test
     */
    public function Page_number_must_be_numeric()
    {
        $response = $this->client->get('/v1/versions?page[number]=ponies');
        assertThat($response, hasStatus(400));
    }
}
