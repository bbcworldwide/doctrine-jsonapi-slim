<?php
namespace BBCWorldWide\JsonApi\Tests\Functional\Api;

use BBCWorldWide\JsonApi\Tests\Functional\ApiTestCase;

class SparseFieldsetTest extends ApiTestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->dataFixtures->load();
    }

    /**
     * @test
     */
    public function Can_specify_attributes()
    {
        $response = $this->client->get('/v1/versions/version1?fields[versions]=cvCode,isSigned');
        $doc = $this->getResponseJson($response);

        $this->assertEquals(['cvCode', 'isSigned'], array_keys($doc['data']['attributes']));
        $this->assertArrayNotHasKey('relationships', $doc['data']);
    }

    /**
     * @test
     */
    public function Can_specify_relationships()
    {
        $response = $this->client->get('/v1/versions/version1?fields[versions]=programme');
        $doc = $this->getResponseJson($response);

        $this->assertEquals(['programme'], array_keys($doc['data']['relationships']));
    }

    /**
     * @test
     */
    public function Can_specify_fields_for_included_resources()
    {
        $response = $this->client->get('/v1/versions/version1?fields[programmes]=displayName&include=programme');
        $doc = $this->getResponseJson($response);

        $this->assertEquals(['displayName'], array_keys($doc['included'][0]['attributes']));
    }

    /**
     * @test
     */
    public function Cant_specify_invalid_fields()
    {
        $response = $this->client->get('/v1/versions/version1?fields[versions]=displayName,madeUpField');
        assertThat($response, hasStatus(400));
    }
}
