<?php

namespace BBCWorldWide\JsonApi\Tests\Functional\Api;

use BBCWorldWide\JsonApi\Tests\Functional\ApiTestCase;
use Helmich\JsonAssert\JsonAssertions;

/**
 * Tests generation of swagger.json on a certain api endpoint.
 *
 * @author BBC Worldwide
 */
class SwaggerTest extends ApiTestCase
{
    use JsonAssertions;

    const SCHEMA_LOCATION = __DIR__ . '/../../Fixtures/jsonapi-json-schema-2.0.json';

    /**
     * @test
     */
    public function swaggerJsonValidatesJsonSchema()
    {
        $schema = file_get_contents(self::SCHEMA_LOCATION);

        $response = $this->client->get('/v1/swagger.json');
        $response->getBody()->rewind();
        $swagger = $response->getBody()->getContents();

        self::assertResponseHasStatus($response, 200);
        self::assertRegExp('/^application\/json/', $response->getHeader('content-type')[0]);
        self::assertJsonDocumentMatchesSchema($swagger, $schema);
    }
}
