<?php

namespace BBCWorldWide\JsonApi\Tests\Functional\Api;

use BBCWorldWide\JsonApi\Tests\Functional\ApiTestCase;

class IncludeResourcesTest extends ApiTestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->dataFixtures->load();
    }

    /**
     * @test
     */
    public function Can_include_to_one_relationships()
    {
        $response = $this->client->get('/v1/versions/version1?include=programme');

        assertThat($response, logicalAnd(
            hasStatus(200),
            bodyMatchesJsonApi([
                '$.data.relationships.programme.data.id'   => logicalNot(isEmpty()),
                '$.data.relationships.programme.data.type' => 'episodes',
                '$.included[0].id'                         => logicalNot(isEmpty()),
                '$.included[0].type'                       => 'episodes',
                '$.included[0].attributes'                 => logicalNot(isEmpty()),
            ])
        ));
    }

    /**
     * @test
     */
    public function Can_include_to_many_relationships()
    {
        $response = $this->client->get('/v1/versions/version1?include=warnings');
        assertThat($response, logicalAnd(
            hasStatus(200),
            bodyMatchesJsonApi([
                "$.data.relationships.warnings.data" => logicalNot(isEmpty()),
                '$.included[0].id'                   => logicalNot(isEmpty()),
                '$.included[0].type'                 => 'warnings',
            ])
        ));
    }

    /**
     * @test
     */
    public function Can_include_nested_relationships()
    {
        $response = $this->client->get('/v1/versions/version1?include=programme.primaryParent');
        assertThat($response, logicalAnd(
            hasStatus(200),
            bodyMatchesJsonApi([
                '$.data.relationships.programme.data'                                => logicalNot(isEmpty()),
                "$.included[?(@.type=='episodes')]"                                  => logicalNot(isEmpty()),
                "$.included[?(@.type=='episodes')].relationships.primaryParent.data" => logicalNot(isEmpty()),
                "$.included[?(@.type=='brands')]"                                    => logicalNot(isEmpty()),
            ])
        ));
    }

    /**
     * @test
     */
    public function Cant_include_invalid_relationships()
    {
        $response = $this->client->get('/v1/programmes/b1?include=madeUpRelationship');
        assertThat($response, hasStatus(400));
    }

    /**
     * @test
     */
    public function Cant_include_blacklisted_relationships()
    {
        $response = $this->client->get('/v1/programmes/b1?include=descendants');
        assertThat($response, hasStatus(400));
    }

    /**
     * @test
     */
    public function Can_include_relationships_on_multiple_resources()
    {
        $response = $this->client->get('/v1/versions?include=programme');
        assertThat($response, logicalAnd(
            hasStatus(200),
            bodyMatchesJsonApi([
                '$.data[0].relationships.programme.data.id'   => logicalNot(isEmpty()),
                '$.data[0].relationships.programme.data.type' => 'episodes',
                '$.included'                                  => logicalNot(isEmpty()),
                '$.included[0].type'                          => 'episodes',
                '$.included[0].attributes'                    => logicalNot(isEmpty()),
            ])
        ));
    }
}
