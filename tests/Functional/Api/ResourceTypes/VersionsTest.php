<?php
namespace BBCWorldWide\JsonApi\Tests\Functional\Api\ResourceTypes\ResourceTypes;

use BBCWorldWide\JsonApi\Tests\Functional\ApiTestCase;

/**
 * Class VersionsTest
 * @package BBCWorldWide\JsonApi\Tests\Functional\Api\ResourceTypes\ResourceTypes
 *
 */
class VersionsTest extends ApiTestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->dataFixtures->load();
    }

    public function testIndex()
    {
        $response = $this->client->get('/v1/versions');

        assertThat($response, logicalAnd(
            hasStatus(200),
            bodyMatchesJsonApi([
                '$.data' => new \PHPUnit\Framework\Constraint\Count(50),
                '$.meta.results' => 100
            ])
        ));
    }

    public function testGetExistingVersion()
    {
        $response = $this->client->get('/v1/versions/version1');

        assertThat($response, logicalAnd(
            hasStatus(200),
            bodyMatchesJsonApi([
                '$.data.id' => 'version1',
                '$.data.type' => 'versions',
            ])
        ));
    }

    public function testUpdateVersion()
    {
        $payload = $this->jsonFixtures->load('programmes/update-version.json');
        $response = $this->client->patch('/v1/versions/version1', $payload);
        assertThat($response, logicalAnd(
            hasStatus(200),
            bodyMatchesJsonApi([
                '$.data.id' => 'version1',
                '$.data.type' => 'versions',
                '$.data.attributes.deliveryEnd' => null,
            ])
        ));
    }
}
