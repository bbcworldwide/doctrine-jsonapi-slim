<?php
namespace BBCWorldWide\JsonApi\Tests\Functional\Api;

use BBCWorldWide\JsonApi\Tests\Functional\ApiTestCase;

class CategoriesTest extends ApiTestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->dataFixtures->load();
    }

    public function testIndex()
    {
        $response = $this->client->get('/v1/categories');

        assertThat($response, logicalAnd(
            hasStatus(200),
            bodyMatchesJsonApi([
                '$.data' => new \PHPUnit\Framework\Constraint\Count(31)
            ])
        ));
    }

    public function testGet()
    {
        $response = $this->client->get('/v1/categories/drama');

        assertThat($response, logicalAnd(
            hasStatus(200),
            bodyMatchesJsonApi([
                '$.data.type' => 'categories',
                '$.data.id' => 'drama',
                '$.data.attributes.name' => 'Drama'
            ])
        ));
    }

    public function testPost()
    {
        $payload = $this->jsonFixtures->load('categories/new.json');
        $response = $this->client->post('/v1/categories', $payload);
        assertThat($response, hasStatus(204));

        $response = $this->client->get('/v1/categories/new');
        assertThat($response, hasStatus(200));
    }

    public function testDelete()
    {
        $response = $this->client->delete('/v1/categories/factual-religion');
        assertThat($response, hasStatus(204));

        $response = $this->client->get('/v1/categories/factual-religion');
        assertThat($response, hasStatus(404));
    }

    public function testPatch()
    {
        $payload = $this->jsonFixtures->load('categories/update.json');
        $response = $this->client->patch('/v1/categories/comedy', $payload);

        assertThat($response, logicalAnd(
            hasStatus(200),
            bodyMatchesJsonApi([
                '$.data.type' => 'categories',
                '$.data.id' => 'comedy',
                '$.data.attributes.name' => 'new name'
            ])
        ));
    }

    public function testGetProgrammesRelated()
    {
        $response = $this->client->get('/v1/categories/comedy/programmes');
        static::assertResponseHasStatus($response, 200);
    }

    public function testGetProgrammesRelationship()
    {
        $response = $this->client->get('/v1/categories/comedy/relationships/programmes');
        static::assertResponseHasStatus($response, 200);
    }
}
