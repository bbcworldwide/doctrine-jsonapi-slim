<?php

namespace BBCWorldWide\JsonApi\Tests\Functional\Api\ResourceTypes;

use BBCWorldWide\JsonApi\Tests\Functional\ApiTestCase;

class ProgrammesTest extends ApiTestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->dataFixtures->load();
    }

    /**
     * @test
     */
    public function indexAllProgrammes()
    {
        $response = $this->client->get('/v1/programmes');
        assertThat($response, logicalAnd(
            hasStatus(200),
            bodyMatchesJsonApi([
                '$.data' => new \PHPUnit\Framework\Constraint\Count(23),
            ])
        ));
    }

    /**
     * @test
     */
    public function indexBrands()
    {
        $response = $this->client->get('/v1/brands');
        assertThat($response, logicalAnd(
            hasStatus(200),
            bodyMatchesJsonApi([
                '$.data'        => new \PHPUnit\Framework\Constraint\Count(3),
                '$.data.*.type' => 'brands',
            ])
        ));
    }

    /**
     * @test
     */
    public function getExistingBrand()
    {
        $response = $this->client->get('/v1/brands/b1');

        assertThat($response, logicalAnd(
            hasStatus(200),
            bodyMatchesJsonApi([
                '$.data.id'                     => 'b1',
                '$.data.type'                   => 'brands',
                '$.data.attributes.displayName' => logicalNot(isEmpty()),
            ])
        ));
    }

    /**
     * @test
     */
    public function createNewBrand()
    {
        $response = $this->client->post('/v1/brands', $this->jsonFixtures->load('programmes/new-brand.json'));
        assertThat($response, hasStatus(204));
    }

    /**
     * @test
     */
    public function deleteBrand()
    {
        $response = $this->client->delete('/v1/brands/b2');
        assertThat($response, hasStatus(204));

        $response = $this->client->get('/v1/brands/b2');
        assertThat($response, hasStatus(404));
    }

    /**
     * @test
     */
    public function updateBrand()
    {
        $payload  = $this->jsonFixtures->load('programmes/update.json');
        $response = $this->client->patch('/v1/brands/b1', $payload);

        assertThat($response, logicalAnd(
            hasStatus(200),
            bodyMatchesJsonApi([
                '$.data.id'                     => 'b1',
                '$.data.type'                   => 'brands',
                '$.data.attributes.displayName' => 'New name',
            ])
        ));
    }
}
