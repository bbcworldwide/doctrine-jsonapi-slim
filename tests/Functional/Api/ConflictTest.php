<?php
namespace BBCWorldWide\JsonApi\Tests\Functional\Api;

use BBCWorldWide\JsonApi\Tests\Functional\ApiTestCase;

class ConflictTest extends ApiTestCase
{
    /**
     * @test
     */
    public function Create_using_existing_ID_results_in_conflict_status()
    {
        $this->dataFixtures->purge();

        $payload = $this->jsonFixtures->load('programmes/new-brand.json');
        $this->client->post('/v1/programmes', $payload);

        // Post same payload again:
        $response = $this->client->post('/v1/programmes', $payload);
        static::assertResponseHasStatus($response, 409);
    }
}
