<?php

namespace BBCWorldWide\JsonApi\Tests\Functional\Api;

use BBCWorldWide\JsonApi\Tests\Functional\ApiTestCase;

class RelationshipTest extends ApiTestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->dataFixtures->load();
    }

    /**
     * @test
     */
    public function addToManyRelationship()
    {
        $payload  = ['data' => [['id' => 'b1s2e1', 'type' => 'episodes']]];
        $response = $this->client->post('/v1/categories/comedy/relationships/programmes', $payload);
        static::assertResponseHasStatus($response, 201);

        $response = $this->client->get('/v1/categories/comedy/relationships/programmes');
        $json     = $this->getResponseJson($response);

        self::assertEquals('b1s2e1', end($json['data'])['id']);
    }

    /**
     * @test
     */
    public function removeRelationshipWithNoPayloadFails()
    {
        $response = $this->client->delete('/v1/categories/comedy/relationships/programmes');
        static::assertResponseHasStatus($response, 400);
    }

    /**
     * @test
     */
    public function removeToManyRelationship()
    {
        $id        = 'b1s1e4';
        $startJson = $this->getResponseJson($this->client->get('/v1/categories/comedy/relationships/programmes'));
        $payload   = ['data' => [['id' => $id, 'type' => 'episodes']]];
        $response  = $this->client->delete('/v1/categories/comedy/relationships/programmes', $payload);
        static::assertResponseHasStatus($response, 204);

        $response = $this->client->get('/v1/categories/comedy/relationships/programmes');
        static::assertResponseHasStatus($response, 200);
        $endJson = $this->getResponseJson($response);

        self::assertCount(count($startJson['data']) - 1, $endJson['data']);

        foreach ($endJson['data'] as $data) {
            self::assertNotEquals($id, $data['id']);
        }
    }

    /**
     * @test
     */
    public function updateToManyRelationship()
    {
        $payload  = ['data' => [['id' => 'b1s2e1', 'type' => 'episodes']]];
        $response = $this->client->patch('/v1/categories/comedy/relationships/programmes', $payload);
        static::assertResponseHasStatus($response, 204);

        $response = $this->client->get('/v1/categories/comedy/relationships/programmes');
        $json     = $this->getResponseJson($response);
        self::assertEquals('b1s2e1', $json['data'][0]['id']);
    }

    /**
     * @test
     */
    public function setToOneRelationship()
    {
        $payload  = ['data' => ['id' => 'drama', 'type' => 'categories']];
        $response = $this->client->patch('/v1/categories/comedy/relationships/parent', $payload);
        static::assertResponseHasStatus($response, 204);

        $response = $this->client->get('/v1/categories/comedy/relationships/parent');
        $json     = $this->getResponseJson($response);

        self::assertEquals('drama', $json['data']['id']);
    }

    /**
     * @test
     */
    public function clearToOneRelationship()
    {
        $payload  = ['data' => null];
        $response = $this->client->patch('/v1/categories/comedy/relationships/parent', $payload);
        static::assertResponseHasStatus($response, 204);

        $response = $this->client->get('/v1/categories/comedy/relationships/parent');
        $json     = $this->getResponseJson($response);
        self::assertEmpty($json['data']);
    }
}
