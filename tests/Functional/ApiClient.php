<?php

namespace BBCWorldWide\JsonApi\Tests\Functional;

use BBCWorldWide\JsonApi\App\Application;
use BBCWorldWide\JsonApi\App\Factory;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Environment;
use Slim\Http\Headers;
use Slim\Http\Request;
use Slim\Http\RequestBody;
use Slim\Http\Response;
use Slim\Http\Uri;

/**
 * Slim application Mock to make API requests.
 */
class ApiClient
{
    /**
     * @var Application
     */
    protected $application;

    /**
     * Returns a new instance to the app.
     *
     * @return Application
     */
    public function getApplication(): Application
    {
        if ($this->application === null) {
            $this->application = Factory::getInstance(new TestAppSetup());
        }

        return $this->application;
    }

    /**
     * Make request to API
     *
     * @param string            $method HTTP Method
     * @param string            $path   URL path
     * @param string|array|null $data   Request body data
     *
     * @return ResponseInterface  Response message
     */
    public function request($method, $path, $data = null): ResponseInterface
    {
        $envSettings = [
            'REQUEST_METHOD' => $method,
            'REQUEST_URI'    => $path,
        ];

        if (is_array($data)) {
            $data = json_encode($data);
        }

        $body = new RequestBody();
        if (!empty($data)) {
            $body->write($data);
            $envSettings['CONTENT_TYPE'] = 'application/vnd.api+json';
        }

        $request  = $this->buildRequest($envSettings, $body);
        $response = $this->getApplication()->process($request, new Response());

        $response->getBody()->rewind();

        return $response;
    }

    /**
     * Build request object from environment settings.
     *
     * @param array       $envSettings
     * @param RequestBody $body
     *
     * @return Request
     */
    public function buildRequest(array $envSettings, RequestBody $body): Request
    {
        $env          = Environment::mock($envSettings);
        $method       = $env->get('REQUEST_METHOD');
        $uri          = Uri::createFromEnvironment($env);
        $headers      = Headers::createFromEnvironment($env);
        $cookies      = [];
        $serverParams = $env->all();

        $request = new Request($method, $uri, $headers, $cookies, $serverParams, $body);

        $request->getBody()->rewind();

        return $request;
    }

    /**
     * Make HEAD request
     *
     * @param string $path URL path
     *
     * @return ResponseInterface  Response message
     */
    public function head(string $path): ResponseInterface
    {
        return $this->request('HEAD', $path);
    }

    /**
     * Make GET request
     *
     * @param string $path URL path
     *
     * @return ResponseInterface  Response message
     */
    public function get(string $path): ResponseInterface
    {
        return $this->request('GET', $path);
    }

    /**
     * Make POST request
     *
     * @param string       $path URL path
     * @param string|array $body Request body data
     *
     * @return ResponseInterface    Response message
     */
    public function post(string $path, $body): ResponseInterface
    {
        return $this->request('POST', $path, $body);
    }

    /**
     * Make PATCH request
     *
     * @param string       $path URL path
     * @param string|array $body Request body data
     *
     * @return ResponseInterface    Response message
     */
    public function patch(string $path, $body): ResponseInterface
    {
        return $this->request('PATCH', $path, $body);
    }

    /**
     * Make PUT request
     *
     * @param string       $path URL path
     * @param string|array $body Request body data
     *
     * @return ResponseInterface    Response message
     */
    public function put(string $path, $body): ResponseInterface
    {
        return $this->request('PUT', $path, $body);
    }

    /**
     * Make DELETE request
     *
     * @param string       $path URL path
     * @param string|array $body Request body data
     *
     * @return ResponseInterface    Response message
     */
    public function delete(string $path, $body = ''): ResponseInterface
    {
        return $this->request('DELETE', $path, $body);
    }
}
