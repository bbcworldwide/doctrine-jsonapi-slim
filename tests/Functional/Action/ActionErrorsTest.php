<?php

namespace BBCWorldWide\JsonApi\Tests\Functional\Action;

use BBCWorldWide\JsonApi\Tests\Functional\ApiTestCase;
use Symfony\Component\VarDumper\VarDumper;

/**
 * Miscellaneous error conditions on actions not entirely covered by other tests.
 *
 * All these specific examples assume the test schema.
 *
 * @author BBC Worldwide
 */
class ActionErrorsTest extends ApiTestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->dataFixtures->load();
    }

    public function tearDown()
    {
        parent::tearDown();

        $this->dataFixtures->purge();
    }

    /**
     * @test
     */
    public function handlesUnknownRelationship()
    {
        $response = $this->client->get('/v1/categories/store/relationships/foobar');
        self::assertResponseHasStatus($response, 404);
    }

    /**
     * @test
     */
    public function handlesKnownRelationship()
    {
        $response = $this->client->get('/v1/categories/store/relationships/children');
        self::assertResponseHasStatus($response, 200);
    }

    /**
     * The schema on fixtures has that relationship down as read only.
     *
     * @test
     */
    public function doesntAllowWritesOnReadOnlyRelationships()
    {
        $payload  = ['data' => [['id' => 'foobar', 'type' => 'categories']]];
        $response = $this->client->post('/v1/categories/store/relationships/children', $payload);
        static::assertResponseHasStatus($response, 403);
    }

    /**
     * @test
     */
    public function patchDoesntWorkWithTypeMismatch()
    {
        $payload  = ['data' => ['id' => 'foobar', 'type' => 'fafa']];
        $response = $this->client->patch('/v1/categories/store', $payload);
        static::assertResponseHasStatus($response, 400);
    }

    /**
     * @test
     */
    public function postDoesntWorkWithTypeMismatch()
    {
        $payload  = ['data' => ['id' => 'foobar', 'type' => 'fafa']];
        $response = $this->client->post('/v1/categories', $payload);
        static::assertResponseHasStatus($response, 400);
    }
}
