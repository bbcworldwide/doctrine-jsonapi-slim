![BBC WorldWide](resources/bbc-ww.png)

# BBC WorldWide - Doctrine Slim JSON API Skeleton

## What it is

This library is an implementation of a [JSONAPI](http://jsonapi.org) compliant service written for Slim Framework 3 and Doctrine. 
It provides with a quick way of spawning new apps by merely specifying a JSONAPI schema, and a bunch of matching 
Doctrine ORM entities.

This, and the [the example app skeleton](https://bitbucket.org/bbcworldwide/doctrine-jsonapi-slim-skeleton), is a
refactoring of the now defunct [BBC Store Catalogue API](https://bitbucket.org/bbcworldwide/bbcstore-catalogue-api) 
written as part of the Amsterdam project. Essentially a like-for like, composerised refactoring of that project into
two minus all the Store specific code.

It is as such very opinionated:
  * Requires a relational database, in fact, anything Doctrine DBAL supports out of the box should work. There might 
  be however a handful of optimised MySQL-specific queries.
  * Requires Sqlite, for functional tests.
  * Requires Redis for the built in response cache.
  * Requires PHP 7.1+
  
## Features

  * **Automatic routes:** once you're fully set up, it's a matter of adding extra ORM entities as well as touching up the
  [JSONAPI schema (example)](src/schema.php).
  * **Response caching:** if enabled in config, every GET request is cached into Redis and served via an 
  [early middleware](src/ResponseCache/ResponseCacheMiddleware.php) back to the client. A system of cache tags based 
  on entity names and IDs take care of invalidation via a doctrine [event subscriber](src/ResponseCache/ResponseCacheSubscriber.php)
  which is also loaded in by default.
  * **Filtering**: [JSONAPI](http://jsonapi.org/format/#fetching-filtering) compliant filtered queries are also possible.
  * **Serialization**: automagically serialize and userialize to/from JSON to/from doctrine entities.
  * **Input validation**: based on schema. Note: this is rather basic and will need propping up.

## How it works
### The main application

We provide with a [BBCWorldWide\JsonApi\App\Application](src/App/Application.php) class, which must be run from the 
front controller. It requires an instance to Slim\App, configured as needed, and a PSR-3 logger.

This class is a thin layer around Slim that would allow us in the future to change to a different framework, for as 
long as it uses the same format of middleware and the dependency injection container implements 
Psr\Container\ContainerInterface.

This library comes furnished with a convenience [BBCWorldWide\JsonApi\App\Factory](src/App/Factory.php) to instantiate 
the whole thing automagically based on the provided default configuration. You can of course build your own 
implementation from all the pieces provided here - the factory class above is also provided as a full-featured example 
on how to do so. See [the example app skeleton](https://bitbucket.org/bbcworldwide/doctrine-jsonapi-slim-skeleton) 
for an app implementation using this factory.

### Configuration

  * [src/settings.php](src/settings.php) contains any infrastructure-related settings, such as database credentials, 
  logger configuration, application dev mode and the like. These are extracted from the environment:
    * APP_ENVIRONMENT: dev / prod. Dev turns on a number of debugging features.
    * APP_NAME: used in places like the logger.
    * APP_CACHE_FOLDER: where the app stores cached things such as Doctrine proxies.
    * APP_BASE_URI: the expected app base URL. Protocol, hostname and port only (eg `http://127.0.0.1:40000`)
    * APP_REDIS_HOST: hostname for the redis server.
    * APP_CACHE_RESPONSES: whether to cache full API responses.
    * APP_CACHE_DRIVER: redis / array (per request, in-memory).
    * APP_DATABASE_MASTER_HOST: hostname to the database.
    * APP_DATABASE_SLAVE_HOSTS: list of comma-separated hostnames to database slaves.
    * APP_DATABASE_PORT: database port
    * APP_DATABASE_NAME
    * APP_DATABASE_USER
    * APP_DATABASE_PASSWORD=password
    * APP_DATABASE_DRIVER: any literal driver supported by doctrine DBAL.
    * APP_DATABASE_VERSION: required to stop Doctrine from checking database version on startup.
  * [src/container.php](src/container.php) defines the default services (like the logger or the entity manager) based 
  on config contained above.
  * [src/middleware-builder](src/middleware-builder.php) defines the default middlewares added on to the app. They
  follow the [Slim Framework middleware convention](https://www.slimframework.com/docs/concepts/middleware.html).
  By default, a request logger, response caching as well as JSONAPI magic.
  * [src/schema.php](src/schema.php) this is the JSONAPI schema. This is an array that describes the JSONAPI schema
  while tying it up to our Doctrine entities. It describes each individual attribute for entities, as well as whether
  they're sortable or read-only. It also describes relationships between different resources, whether they're multiple
  or read-only as well. Plenty of examples on the [corresponding test fixture](tests/Fixtures/schema-compiled.php).
  * [src/routes.php](src/routes.php) contains the default, abstract routes for resources, resource relations, methods
  etc. Rarely should you ever add anything to routes.
  
## How to use
### Installation

To install via composer, you'll need to add a `vcs` repository. Not git, not github, but VCS, to leverage composer's 
ability to fetch the zip file from a tagged release. Like so:


```json
    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/bbcworldwide/doctrine-jsonapi-slim.git"
        }
    ],
    "require": [
        "bbcworldwide/doctrine-jsonapi-slim": "^1.0@dev"
    ]
```

This repository is public, so it will work without any special permissions.

### Usage

The best way is always an example:

  * [The skeleton app:](https://bitbucket.org/bbcworldwide/doctrine-jsonapi-slim-skeleton) 
  uses the built in Factory and its own overridding config to function. This should be the preferred starting point
  for any apps using this library, but also serve as an example if you need something more customised. Its 
  [README.md](https://bitbucket.org/bbcworldwide/doctrine-jsonapi-slim-skeleton) contains instructions on how to
  actually get up and running.
  * [Functional test fixtures](tests/Fixtures) contain a trimmed down schema and ORM entity set taken from 
  [BBC Store Catalogue API](https://bitbucket.org/bbcworldwide/bbcstore-catalogue-api), which this library derives from.
  The functional test set up also uses the Factory with its own implementation of the set up class loading these.

## Contribute

Usual AMS / Meerkat development practices apply. Any new feature MUST come with unit (mandatory) and functional 
(optional, where appropriate) tests.

### Versioning

Since this is a composer package, we need to follow [Semver](http://semver.org) and actually tag releases that way.
Make sure you fully understand Semver before you go around tagging stuff as we absolutely need to ensure no breakage of
downstream apps when creating new minor versions. BC is imperative. Please read through [Semver](http://semver.org), 
but in a nutshell:

  * Major version increase when we're breaking backwards compatibility
  * Minor version increase when we're adding new features without breaking backwards compatibility
  * Patch level when we're fixing up a given release without adding any features

Example: tag a new release as 1.1.3:

```bash
git fetch origin
git checkout master
git pull
git tag 1.1.3 -m"Witty commento on why this release is awesome"
git push --tags
```

Any downstream apps requiring us as "^1" or "^1.0" or "^1.1" or "1.1.*" should pick up the new version after they do
a `composer update`.
